module.exports = {
	plugins: {
		'postcss-cssnext': {
			browsers: ['last 2 version', 'Safari 7', 'ie 10']
		},
		'css-declaration-sorter': {
			order: 'concentric-css'
		},
		'css-mqpacker': {},
		'postcss-modules': {
			'generateScopedName': function(name, filename, css) {
			  const i           = css.indexOf(`.${ name }`);
			  const lineNumber  = css.substr(0, i).split(/[\r\n]/).length;
			  const hash        = stringHash(css).toString(36).substr(0, 5);
			  return `${ name }_${ hash }_${ lineNumber }`;
			},
		},
		'postcss-camel-case':{}
	}
}