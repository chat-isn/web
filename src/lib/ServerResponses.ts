import RequestTypes from './RequestTypes'

enum ServerEvents {
	RECEIVED_MESSAGE = 'receivedmessage',
	RECEIVED_GAME_MESSAGE = 'receivedarg',
	ERROR = 'error',
	SUCCESS = 'success'
}
export { ServerEvents }

export interface IBaseResponse {
	type: RequestTypes | ServerEvents
	id: number
}

export interface ILoginResponse extends IBaseResponse {
	type: RequestTypes.LOGIN
	userId: number
	username: string
}

export interface IRegisterResponse extends IBaseResponse {
	type: RequestTypes.REGISTER
	userId: number
	username: string
	error?: number
}

export interface ICreateChatResponse extends IBaseResponse {
	type: RequestTypes.CREATE_CHAT
	chatid: number
}

export interface IListChatResponse extends IBaseResponse {
	type: RequestTypes.LIST_CHAT
	chatlist: string // chatId separated by -
}

export interface IFetchUsersResponse extends IBaseResponse {
	type: RequestTypes.FETCH_USERS
	usernameList: string[]
}

export interface IFetchUsersInChatResponse extends IBaseResponse {
	type: RequestTypes.FETCH_USERS_IN_CHAT
	chatlist: { [chatId: number]: string } // userids separated by '-'
}

export interface IReceivedMessage extends IBaseResponse {
	type: ServerEvents.RECEIVED_MESSAGE
	chatid: number
	message: string
	userid: number
	time?: number
}

export interface IReceivedGameMessage extends IBaseResponse {
	type: ServerEvents.RECEIVED_GAME_MESSAGE
	chatid: number
	message: string
	userid: number
	userlist: string
}

export interface IErrorMessage extends IBaseResponse {
	type: ServerEvents.ERROR
	errorid: number
}

export interface IDeleteChatResponse extends IBaseResponse {
	type: RequestTypes.DELETE_CHAT
	chatid: number
}

export interface IChangeMailResponse extends IBaseResponse {
	type: RequestTypes.CHANGE_MAIL | ServerEvents.ERROR | ServerEvents.SUCCESS
}

export interface IChangePasswordResponse extends IBaseResponse {
	type: RequestTypes.CHANGE_PASSWORD | ServerEvents.ERROR | ServerEvents.SUCCESS
}

export interface IFetchMessagesResponse extends IBaseResponse {
	type: RequestTypes.FETCH_MESSAGES
	chatid: number
	maxmessages?: number
	messageList?: { [userId: string]: ([string, string])[]}
}
