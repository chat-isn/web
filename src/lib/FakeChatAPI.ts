import Connection from './Connection'
import ChatAPI, {IDMChatPreviewPartial, ISelf, UserId} from './ChatAPI'

export default class FakeChatAPI extends ChatAPI {
	authedUser: ISelf = {
		id: 1, name: 'Maeeen', email: 'kek@kek.kek'
	}

	constructor(protected connection: Connection) {
		super(connection)
	}

	async createDM(userIds: UserId[], ignoreExistantChat = false) {
		console.log('(dont remove or eslint gonna be annoying)', userIds, ignoreExistantChat)
		await new Promise(resolve => setTimeout(resolve, 1500))
		return null
	}

	async fetchDMMessages(id: number) {
		console.log('(do not remove or eslint will be annoying) id...', id)
		await new Promise(resolve => setTimeout(resolve, 1500))
		return [{
			sender: { id: 1, name: 'Maeeen' },
			time: (new Date().getTime()) - 20 * 60 * 1000,
			content: id + 'Bonjour!!' + Math.random()
		}, {
			sender: { id: 2, name: 'Naozumi' },
			time: (new Date().getTime()) - 21 * 60 * 1000,
			content: 'Bonsoir **md_test**\n__souligné__'
		}]
	}

	sendMessage(source: string, str: string) {
		console.log('Message', str, 'has to be sent from', source)
	}

	protected async fetchDMs() {
		await new Promise(resolve => setTimeout(resolve, 150))

		const formatDMName = (dm: IDMChatPreviewPartial) => dm.users
			.filter(user => user.id !== this.authedUser.id)
			.map(user => user.name)
			.join(', ')

		const DMs: IDMChatPreviewPartial[] = [{
			id: 1, users: [{id: 1, name: 'Maeeen'}, {id: 2, name: 'Naozumi'}], lastMessage: 'hello'
		}, {
			id: 2, users: [{id: 1, name: 'Maeeen'}, {id: 3, name: 'Nathan'}], lastMessage: 'bon slt'
		}, {
			id: 3, users: [{id: 4, name: 'Lyroma'}, {id: 1, name: 'Maeeen'}], lastMessage: 'cc lyroma'
		}]

		return DMs.map(dm => ({...dm, name: formatDMName(dm)}))
	}
}
