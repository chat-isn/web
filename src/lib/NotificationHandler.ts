import {EventEmitter} from 'events'

// import MESSAGE_ICON from '../img/icons-png/baseline_chat_bubble_black_48dp.png'
import MESSAGE_ICON from '../img/icons-png/baseline_chat_bubble_black_48dp.png'

import appInstance from '../AppInstance'
import log from '../util/log'
import getHistory from '../components/util/getHistory'
import i18n from '../i18n/i18n'
import buildSimpleMessageModal from '../components/modal/simple-message-modal/buildSimpleMessageModal'
import {IMessage} from './ChatAPI'
import userPreferences from './UserPreferences'
import RichMessage from './RichMessage'


interface INotificationHandlerEvents {
	on(event: 'notification', listener: (title: string, body: string, iconPath: string, url: string) => any): this
}

/**
 * @singleton
 */
export class NotificationHandler extends EventEmitter implements INotificationHandlerEvents {
	private static instance = new NotificationHandler()
	private Notification = window.Notification

	constructor() {
		super()

		if (this.supported()) {
			const userPref = userPreferences.registerPreference({
				id: 'notification',
				default: 'disabled',
				localStorage: true,
				friendlyName: () => i18n.get('app.notifications.name'),
				choices: () => ({
					enabled: i18n.get('app.notifications.enabled'),
					disabled: i18n.get('app.notifications.disabled')
				}),
				onChange: id => {
					if (id === 'enabled')
						this.requestPermission().then(enabled => {
							if (!enabled) {
								const pref = userPreferences.getPreferenceObject('notification')
								if (pref)
									pref.value = id = 'disabled'
								appInstance.showModal(buildSimpleMessageModal(i18n.get('app.notifications.blocked')))
							}
						})
					DESKTOP_NOTIFICATIONS_ENABLED = id === 'enabled'
				}
			})

			let DESKTOP_NOTIFICATIONS_ENABLED = userPreferences.get( userPref ) === 'enabled'

			let IN_APP_NOTIFICATIONS_ENABLED = userPreferences.get( userPreferences.registerPreference({
				id: 'notification-in-app',
				default: 'enabled',
				localStorage: true,
				friendlyName: () => i18n.get('app.in-notifications.name'),
				choices: () => ({
					enabled: i18n.get('app.in-notifications.enabled'),
					disabled: i18n.get('app.in-notifications.disabled')
				}),
				onChange: id => {
					IN_APP_NOTIFICATIONS_ENABLED = id === 'enabled'
				}
			}) ) === 'enabled'

			appInstance.auth.on('connected', () => {
				if (DESKTOP_NOTIFICATIONS_ENABLED)
					this.requestPermission().then(res => {
						if (!res)
							userPref.value = 'disabled'
						log('Notification', res ? 'Enabled' : 'Disabled')
					})
			})

			appInstance.chatAPI.on('message', (chatid: number, message: IMessage) => {
				const notificationTitle = message.sender.name
				let notificationBody = message.content

				if (notificationBody instanceof RichMessage)
					notificationBody = i18n.get('app.chat.notification.click-to-see')

				if (DESKTOP_NOTIFICATIONS_ENABLED && this.isPermissionGranted() && !document.hasFocus()) {
					const msgNotification = new Notification(notificationTitle, {
						body: notificationBody,
						icon: MESSAGE_ICON
					})
					msgNotification.addEventListener('click', () => {
						const history = getHistory()
						if (history)
							history.push(`/app/chat/${chatid}`)
						window.focus()
					})
				} else if (IN_APP_NOTIFICATIONS_ENABLED) {
					const history = getHistory()
					if (!history)
						return

					if (history.location.pathname === `/app/chat/${chatid}`)
						return

					window.focus()
					this.emit('notification', notificationTitle, notificationBody, MESSAGE_ICON, `/app/chat/${chatid}`)
				}
			})
		}
	}

	static init() {
		return NotificationHandler.instance
	}

	supported() {
		return !!this.Notification
	}

	isPermissionGranted() {
		return this.Notification.permission === 'granted'
	}

	async requestPermission() {
		if (this.isPermissionGranted())
			return true

		if (this.Notification.permission === 'denied')
			return false

		return (await this.Notification.requestPermission()) === 'granted'
	}
}

export default NotificationHandler.init()
