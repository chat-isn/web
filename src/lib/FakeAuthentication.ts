import log from '../util/log'
import { email as emailTest } from '../util/tests'
import Authentication, { IAuthFields, IRegisterFields, RegisterRules, ERegisterResponse } from './Authentication'
import { checkRules } from './FieldRules'

export default class FakeAuthentication extends Authentication {
	isAuthed = false

	async auth(fields: IAuthFields) {
		log('FakeAuth', `Authed with ${fields.mail}:${fields.password}`)
		await new Promise(resolve => setTimeout(resolve, 1500))

		this.emit('connected')
		return true
	}

	async register(fields: IRegisterFields): Promise<ERegisterResponse> {
		if (!checkRules({
			username: fields.username,
			password: fields.password,
			mail: fields.mail
		}, RegisterRules))
			return ERegisterResponse.RULES_NOT_MET

		log('FakeAuth', `Creating account ${fields.mail}:${fields.username}:${fields.password}`)
		await new Promise(resolve => setTimeout(resolve, 1500))
		this.emit('connected')
		return ERegisterResponse.OK
	}

	async reset(mail: string): Promise<boolean> {
		if (!emailTest(mail))
			return false

		log('FakeAuth', `Resetting password for ${mail}`)

		await new Promise(resolve => setTimeout(resolve, 1500))
		return true
	}
}
