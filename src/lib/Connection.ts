import { EventEmitter } from 'events'
import WebSocketMessage from './WebSocketMessage'
import {IBaseResponse, ServerEvents} from './ServerResponses'
import RequestTypes, {IServerResponsesMap} from './RequestTypes'

enum ConnectionState {
	NOTHING,
	CONNECTING,
	EXCHANGING_KEYS,
	ESTABLISHED,
	ERROR
}
export { ConnectionState }

interface IConnection {
	on(event: 'error', listener: (error: Error, connection: Connection) => any): this
	on(event: 'chat-message' | 'game-chat-message' | 'create-chat-event',
	   listener: (data: object, connection: Connection) => any): this
	on(event: 'retry', listener: (connection: Connection) => any): this
	on(event: 'connecting' | 'hello-received' | 'connected', listener: () => any): this
}

export default class Connection extends EventEmitter implements IConnection {
	connectionState: ConnectionState = ConnectionState.NOTHING
	connection: WebSocket | null = null
	connectionTries = 1
	connectionMax = 3

	private cancelled = false

	constructor(public address: string) {
		super()

		this.on('hello-received', () => {
			this.connectionState = ConnectionState.ESTABLISHED
			this.emit('connected')
		})
	}

	reset() {
		this.connectionTries = 1
	}

	connect() {
		// connecting to the ws
		try {
			this.cancelled = false
			this.connection = new WebSocket(this.address)
			this.connectionState = ConnectionState.CONNECTING
			this.emit('connecting')

			this.connection.addEventListener('open', () => {
				if (this.cancelled) return
				this.sendHello()
			})

			this.connection.addEventListener('error', e => {
				if (this.connectionTries < this.connectionMax) {
					this.connectionTries++
					this.emit('retry', e)
					return this.connect()
				}

				if (this.connectionState === ConnectionState.ESTABLISHED)
					this.emit('unexpected-error', e)

				this.emit('error', e)
				this.connectionState = ConnectionState.ERROR
			})

			// the connection has been properly closed
			this.connection.addEventListener('close', e => {
				if (this.cancelled) return

				console.error(e, e.wasClean, this.connectionState)

				if (this.connectionState === ConnectionState.ESTABLISHED)
					if (!e.wasClean || !this.cancelled) this.emit('unexpected-close', e)

				if (this.connectionState !== ConnectionState.CONNECTING) {
					this.emit('close', e)
					this.connectionState = ConnectionState.NOTHING
				}
			})

			this.connection.addEventListener('message', e => this.handleMessage(e))
		} catch (e) {
			this.emit('error', e)
		}
	}

	send<T extends IBaseResponse>(message: WebSocketMessage<T, IServerResponsesMap[T['type']]>): Promise<T> | null {
		if (this.connection) {
			this.connection.send(message.toJSON())
			return new Promise(resolve => {
				// maybe: insert timeout ?

				// once for memory leaks
				this.once(`message-${message.id}`, data => resolve(data))
			})
		}
		return null
	}

	close() {
		this.cancel()
		this.connectionState = ConnectionState.NOTHING
		if (this.connection)
			this.connection.close()
		this.connection = null
	}

	private async sendHello() {
		await this.send( new WebSocketMessage(RequestTypes.HELLO, {}) )
	}

	private handleMessage(event: MessageEvent) {
		try {
			const data = JSON.parse(event.data)

			if (this.connectionState === ConnectionState.ESTABLISHED)
				if (data.type === ServerEvents.RECEIVED_MESSAGE)
					this.emit('chat-message', data)
				else if (data.type === ServerEvents.RECEIVED_GAME_MESSAGE)
					this.emit('game-chat-message', data)
				else {
					if (data.type === RequestTypes.CREATE_CHAT)
						this.emit('create-chat-event', data)
					else if (data.type === RequestTypes.DELETE_CHAT)
						this.emit('delete-chat-event', data)
					this.emit(`message-${data.id}`, data)
				}
			else if (this.connectionState === ConnectionState.CONNECTING) {
				if (data.type === 'hello')
					this.emit('hello-received')
				else
					this.emit('error', new Error('No hello received at first.'))
				return
			}
		} catch (e) {
			this.emit('error', e)
		}
	}

	private cancel() {
		this.cancelled = true
	}
}
