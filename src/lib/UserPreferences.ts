import {ISearchResult} from '../components/top-info-and-search-bar/SearchResults'
import normalize from '../util/normalize'


interface IUserPreferences {
	[key: string]: IUserPreference
}

/**
 * Set userPreferences by settings the .value
 * Get the value surely
 */
export interface IUserPreference<T = any> {
	id: string
	value?: T
	default: T
	localStorage: boolean

	// For the user friendly preferences!
	friendlyName?: string | (() => string)
	choices?: () => { [value: string]: string }
	onChange?: (value: T) => any
	enableSearchFromChoice?: boolean
	/**
	 * The value_ should not be used externally
	 */
	_value?: T
}

export class UserPreferences {
	static instance: UserPreferences = new UserPreferences()

	preferences: IUserPreferences = {}

	static init(): UserPreferences {
		return UserPreferences.instance
	}

	static isPreferencePublic(preference: IUserPreference) {
		return typeof preference.choices === 'function' &&
			['string', 'function'].includes(typeof preference.friendlyName) &&
			typeof preference.onChange === 'function'
	}

	registerPreference(preference: IUserPreference): IUserPreference {
		if ( this.exists(preference) )
			return preference

		this.preferences[ preference.id ] = preference

		if (!preference.value)
			preference.value = this.get(preference)
		// watch !
		preference._value = preference.value

		Object.defineProperty(preference, 'value', {
			get: function get() { return this._value },
			set: function set(newValue) {
				if (this.localStorage) // updates in localStorage
					localStorage.setItem( this.id, newValue )
				this._value = newValue

				if (preference.onChange)
					preference.onChange(newValue)
			}
		})

		return preference
	}

	exists(preference: IUserPreference): boolean {
		return this.existsById(preference.id)
	}

	existsById(id: string): boolean {
		return Object.keys( this.preferences ).includes( id )
	}

	getPreferenceObject(id: string): IUserPreference | undefined {
		if (!this.existsById(id))
			return

		return this.preferences[id]
	}

	get<T>(preference: IUserPreference<T>): T {
		let value

		if (preference.localStorage)
			value = localStorage.getItem( preference.id )
		else if (preference.value)
			value = preference.value

		if (!value)
			value = preference.default

		return value as T
	}

	getById(id: string): any {
		if (!this.existsById(id))
			return

		return this.get( this.preferences[id] )
	}

	search(query: string): ISearchResult[] {
		query = normalize(query.toLowerCase())
		return Object.values(this.preferences)
			.filter(pref => UserPreferences.isPreferencePublic(pref))
			.filter(pref => {
				if (!pref.friendlyName || !pref.choices) // useless
					return

				const friendlyName = typeof pref.friendlyName === 'string' ? pref.friendlyName : pref.friendlyName()

				if (normalize(friendlyName.toLowerCase()).includes(query) || // is the query in the friendlyName
					pref.id.includes(query)) // is the query in the id
					return true

				if (pref.enableSearchFromChoice) {
					const choices = pref.choices()
					// TODO_: add maybe search by code-value ? (+ existing friendlyName)
					if (Object.values(choices)
							.find((choiceStr: string) => normalize(choiceStr).toLowerCase().includes(query)))
						return true
				}
				return false
			}).map(pref => ({
				type: 'settings',
				friendlyName: typeof pref.friendlyName === 'string' ? pref.friendlyName : (
					typeof pref.friendlyName === 'function' ? pref.friendlyName() : 'null'
				),
				path: `app/settings/${pref.id}`
			}))
	}
}

export default UserPreferences.init()
