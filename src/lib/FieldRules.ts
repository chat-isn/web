export interface IFieldRule {
	notEmpty?: boolean
	min?: number
	max?: number
	custom?: ((val: string) => true | string)[]
}

export type IRuleSet<T> = {
	[x in (keyof T)]: IFieldRule
}

enum IFieldRuleResult {
	OK,
	EMPTY,
	TOO_SHORT,
	TOO_LONG
}
export { IFieldRuleResult }
type IFieldRuleResultCustom = string

export function checkRule(val: string, rule: IFieldRule): IFieldRuleResult
	| IFieldRuleResultCustom {
	if (!rule)
		return IFieldRuleResult.OK

	if (!val)
		val = ''

	if ( (typeof rule.notEmpty === 'undefined' || rule.notEmpty) && val === '' )
		return IFieldRuleResult.EMPTY

	if ( (rule.min && val.length < rule.min) )
		return IFieldRuleResult.TOO_SHORT
	if ( (rule.max && val.length > rule.max) )
		return IFieldRuleResult.TOO_LONG

	if ( rule.custom ) {
		const customRuleResult = rule.custom.reduce((prev: boolean | string, fn) => {
			if (prev)
				return fn(val)
			return prev
		}, true)

		if (typeof customRuleResult === 'string')
			return customRuleResult
	}

	return IFieldRuleResult.OK
}

export function checkRules(vals:  { [x: string]: string },
						   rules: { [x: string]: IFieldRule }) {

	return !Object.keys(vals)
				 .map( key => checkRule(vals[key], rules[key]) === IFieldRuleResult.OK)
				 .includes(false)
}

/**
 * Returns false if no errors
 * T is a field object (such as IAuthFields)
 */
export function generateFieldCheck<T = any>(
	formData: T,
	ruleSet: IRuleSet<T>,
	fieldKey: keyof T,
	tooLong: string,
	tooShort: string,
	empty: string): string | false {
	const checkRuleResult = checkRule(formData[fieldKey] as unknown as string, ruleSet[fieldKey])

	if (checkRuleResult === IFieldRuleResult.OK)
		return false

	if (checkRuleResult === IFieldRuleResult.TOO_LONG)
		return tooLong

	if (checkRuleResult === IFieldRuleResult.TOO_SHORT)
		return tooShort

	if (checkRuleResult === IFieldRuleResult.EMPTY)
		return empty

	if (typeof checkRuleResult === 'string')
		return checkRuleResult

	return false
}
