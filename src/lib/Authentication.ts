import { EventEmitter } from 'events'
import { sha256 } from 'js-sha256'
import log from '../util/log'
import { AppInstance } from '../AppInstance'
import { email as emailTest } from '../util/tests'
import i18n from '../i18n/i18n'
import { checkRules, IRuleSet } from './FieldRules'
import WebSocketMessage from './WebSocketMessage'
import { ILoginResponse, IRegisterResponse } from './ServerResponses'
import RequestTypes from './RequestTypes'
import userPreferences from './UserPreferences'

export interface IAuthFields {
	mail: string
	password: string
}

export interface IRegisterFields {
	username: string
	password: string
	mail: string
}

const AuthRules: IRuleSet<IAuthFields> = {
	mail: { min: 5, max: 64, custom: [ val => emailTest(val) ? true : i18n.get('home.register.not-email') ] },
	password: { min: 7 }
}
export { AuthRules }

const RegisterRules: IRuleSet<IRegisterFields> = {
	username: { min: 4, max: 16 },
	password: { min: 7 },
	mail: {
		min: 4,
		max: 64,
		custom: [ val => emailTest(val) ? true : i18n.get('home.register.not-email') ]
	}
}
export { RegisterRules }

enum ERegisterResponse { OK, RULES_NOT_MET, USERNAME_USED, EMAIL_USED, UNKNOWN }
export { ERegisterResponse }


export default class Authentication extends EventEmitter {
	isAuthed = false

	autoConnectPref = userPreferences.registerPreference({
		id: 'credentials',
		localStorage: true,
		default: null
	})

	constructor(protected appInstance: AppInstance) {
		super()

		this.on('connected', () => this.isAuthed = true)
	}

	canAutoConnect() {
		try {
			if (this.autoConnectPref.value) {
				const autoConnectData = JSON.parse(this.autoConnectPref.value)
				if (Array.isArray(autoConnectData) && autoConnectData.length === 2)
					return autoConnectData.every(t => typeof t === 'string')
			}
		} catch (e) {
			console.error(e)
		}
		return false
	}

	async autoConnect() {
		// let's assume .canAutoConnect() has been called first
		const autoConnectData = JSON.parse(this.autoConnectPref.value)
		log('Authentication', 'Auto-connecting...')
		return await this.auth({
			mail: autoConnectData[0], password: autoConnectData[1]
		}, true)
	}

	async auth(fields: IAuthFields, hasBeenHashed = false) {
		const password = hasBeenHashed ? fields.password : sha256(fields.password)

		fields.mail = fields.mail.toLowerCase()

		const { connection, chatAPI } = this.appInstance,
			response = await connection.send<ILoginResponse>(
				new WebSocketMessage(RequestTypes.LOGIN, {
					email: fields.mail, password
				})
			) as ILoginResponse
		this.autoConnectPref.value = JSON.stringify([fields.mail, password])

		if (response.userId) {
			chatAPI.authedUser = { id: response.userId, name: response.username, email: fields.mail }
			this.emit('connected')
			return this.isAuthed = true
		}
		this.autoConnectPref.value = null
		return false
	}

	async register(fields: IRegisterFields): Promise<ERegisterResponse> {
		if (!checkRules({
			username: fields.username,
			password: fields.password,
			mail: fields.mail
		}, RegisterRules))
			return ERegisterResponse.RULES_NOT_MET

		fields.mail = fields.mail.toLowerCase()

		const { connection, chatAPI } = this.appInstance,
			response = await connection.send<IRegisterResponse>(
				new WebSocketMessage(RequestTypes.REGISTER, {
					username: fields.username,
					email: fields.mail,
					password: sha256(fields.password)
				})
			) as IRegisterResponse

		if (!response.error)
			if (response.userId && response.username) {
				chatAPI.authedUser = { id: response.userId, name: response.username, email: fields.mail }
				this.isAuthed = true
				this.emit('connected')
				return ERegisterResponse.OK
			} else
				response.error = -1

		switch (response.error) { // made by the switch statement gang
			case 114:
				return ERegisterResponse.EMAIL_USED
			case 115:
				return ERegisterResponse.USERNAME_USED
			case -1:
			default:
				return ERegisterResponse.UNKNOWN
		}
	}

	/**
	 * For security purposes, NEVER the server
	 * will return whether the mail has been found or not
	 * in the database. So the answer is always kinda true
	 */
	async reset(mail: string): Promise<boolean> {
		if (!emailTest(mail))
			return false

		await new Promise(resolve => setTimeout(resolve, 1500))
		return true
	}

	disconnect() {
		this.isAuthed = false
		this.autoConnectPref.value = null
	}
}
