import { IEmptyRequestFields } from './RequestTypes'
import {IBaseResponse} from './ServerResponses'

export default class WebSocketMessage<S extends IBaseResponse, T extends IEmptyRequestFields> {
	static ID = 0

	id = WebSocketMessage.ID

	constructor(public type: S['type'], public additionalFields: T) {
		WebSocketMessage.ID++
	}

	toJSON() {
		return JSON.stringify({
			id: this.id,
			type: this.type,
			...this.additionalFields
		})
	}
}
