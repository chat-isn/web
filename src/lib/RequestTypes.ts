import {ServerEvents} from './ServerResponses'

enum RequestTypes {
	HELLO = 'hello',
	LOGIN = 'login',
	REGISTER = 'register',
	LIST_CHAT = 'listconv',
	CREATE_CHAT = 'createconv',
	FETCH_USERS = 'fetchusers',
	FETCH_USERS_IN_CHAT = 'fetchchatid',
	DELETE_CHAT = 'deletechat',
	CHANGE_PASSWORD = 'changepassword',
	CHANGE_MAIL = 'changemail',

	CREATE_MESSAGE = 'message',
	FETCH_MESSAGES = 'fetchmessage',
	CREATE_GAME_MESSAGE = 'arg'
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IEmptyRequestFields { }

export interface ILoginRequestFields extends IEmptyRequestFields {
	email: string
	password: string
}

export interface IRegisterRequestFields extends IEmptyRequestFields {
	username: string
	email: string
	password: string
}

export interface IListChatRequestFields extends IEmptyRequestFields {
	userid: number
}

export interface ICreateConvRequestFields extends IEmptyRequestFields {
	user: string
}

export interface IFetchUsersFields extends IEmptyRequestFields {
	users: string
}

export interface IFetchUsersInChatFields extends IEmptyRequestFields {
	chatlist: string
}

export interface ICreateMessageFields extends IEmptyRequestFields {
	user: string
	chatid: string
	message: string
}

export interface ICreateGameMessageFields extends ICreateMessageFields {
	userlist: string
}


export interface IDeleteChatFields extends IEmptyRequestFields {
	userid: number
	chatid: number
}

export interface IChangePasswordFields extends IEmptyRequestFields {
	userid: string
	password: string
	newpassword: string
}

export interface IChangeMailFields extends IEmptyRequestFields {
	userid: string
	mail: string
	newmail: string
}

export interface IFetchMessagesFields extends IEmptyRequestFields {
	chatid: string
	/* from-to */
	messages: string
}

export interface IServerResponsesMap {
	[RequestTypes.HELLO]: IEmptyRequestFields
	[RequestTypes.LOGIN]: ILoginRequestFields
	[RequestTypes.REGISTER]: IRegisterRequestFields
	[RequestTypes.LIST_CHAT]: IListChatRequestFields
	[RequestTypes.CREATE_CHAT]: ICreateConvRequestFields
	[RequestTypes.FETCH_USERS]: IFetchUsersFields
	[RequestTypes.FETCH_USERS_IN_CHAT]: IFetchUsersInChatFields
	[RequestTypes.DELETE_CHAT]: IDeleteChatFields
	[RequestTypes.CHANGE_PASSWORD]: IChangePasswordFields
	[RequestTypes.CHANGE_MAIL]: IChangeMailFields
	[RequestTypes.FETCH_MESSAGES]: IFetchMessagesFields

	// BE CAREFUL, NO WS MESSAGE IS SENT AFTER CREATE_MESSAGE
	// THE BROADCAST IS THE CONFIRMATION OF MESSAGE RECEPTION
	[RequestTypes.CREATE_MESSAGE]: ICreateMessageFields
	[RequestTypes.CREATE_GAME_MESSAGE]: ICreateGameMessageFields

	// Server Events, this is just for typings, never gonna be used in fact.
	[ServerEvents.RECEIVED_MESSAGE]: IEmptyRequestFields
	[ServerEvents.RECEIVED_GAME_MESSAGE]: IEmptyRequestFields
	[ServerEvents.ERROR]: IEmptyRequestFields
	[ServerEvents.SUCCESS]: IEmptyRequestFields
}

export default RequestTypes
