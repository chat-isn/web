import { EventEmitter } from 'events'
import log from '../util/log'
import userPreferences, {IUserPreference} from './UserPreferences'

/* eslint-disable camelcase */
/* eslint-disable @typescript-eslint/camelcase */

const SPOTIFY_API_URL = 'https://api.spotify.com/v1/'
const SCOPES = 'user-read-private user-read-playback-state user-modify-playback-state user-read-currently-playing'
const CLIENT_ID = '0d0081c6d5cc4e9ca90b1841f503e2d9'

interface ISpotifyIntegrationEvents {
	on(event: 'player-data-update', listener: () => any): this
}

export class SpotifyIntegration extends EventEmitter implements ISpotifyIntegrationEvents {
	private static instance: SpotifyIntegration = new SpotifyIntegration()

	playerData: null | SpotifyApi.CurrentlyPlayingObject = null
	private pref: IUserPreference = userPreferences.registerPreference({
			id: 'spotify-refresh-token',
			default: 'null',
			localStorage: true
		})
	private code: string | null = null
	private token: string | null = null
	private interval: NodeJS.Timeout | null = null

	static getInstance() {
		return this.instance
	}

	hasSpotifyAccess() {
		return this.token !== null
	}

	hasRefreshToken() {
		return this.pref.value !== 'null'
	}

	disconnect() {
		this.pref.value = 'null'
		this.code = null
		this.token = null
		this.clearInterval()
	}

	clearInterval() {
		if (this.interval) {
			clearInterval(this.interval)
			this.interval = null
		}
	}

	findCode() {
		const codeRegex = /\?code=([A-z\d-_]+)/gi,
			test = codeRegex.exec(location.search)

		if (!test || test.length <= 1) {
			if (this.pref.value !== 'null')
				this.requestToken(true).then()

			return
		}

		this.code = test[1]
		log('Spotify Integration', 'Code fetched!')
		history.replaceState(null, document.title,
			location.href.replace(codeRegex, '#/app/settings/'))
		this.requestToken(false).then()
	}

	redirectToLogin() {
		if (!this.hasSpotifyAccess())
			window.location.href = 'https://accounts.spotify.com/authorize?response_type=code' +
				'&client_id=' + CLIENT_ID + '&scope=' +
				encodeURIComponent(SCOPES) + '&redirect_uri=' +
				encodeURIComponent(window.location.origin)
	}

	async play(trackUri: null | string | string[], ctx?: string) {
		if (!this.hasSpotifyAccess())
			return false

		if (trackUri === null)
			trackUri = []
		if (!Array.isArray(trackUri))
			trackUri = [trackUri]

		const [status] = await this.callSpotifyApi('me/player/play', 'PUT', {
			uris: trackUri.length === 0 ? null : trackUri,
			context_uri: trackUri.length === 0 ? ctx : null
		})

		return status <= 300
	}

	async getUserData() {
		if (!this.hasSpotifyAccess()) return null

		const [status, data] = await this.callSpotifyApi('me', 'GET')
		if (status !== 200)
			return null

		return data
	}

	private async requestToken(refreshing: boolean) {
		if (this.hasSpotifyAccess()) return

		const data = refreshing ? {
			grant_type: 'refresh_token',
			refresh_token: this.pref.value
		} : {
			grant_type: 'authorization_code',
			code: this.code,
			redirect_uri: window.location.origin
		}

		await this.callSpotifyApi('token', 'POST', data, SPOTIFY_WEB_SERVICE_SERVER).then(([status, response]) => {
			if (status !== 200) {
				log('Spotify Integration', 'Can\'t fetch token. Disabled.')
				this.token = null
				this.code = null
				return this.pref.value = 'null'
			}

			log('Spotify Integration', `Session ${ refreshing ? 'refreshed' : 'authed' }!`)
			if (response.refresh_token)
				this.pref.value = response.refresh_token
			this.token = response.access_token

			this.interval = setInterval(() => {
				this.callSpotifyApi('me/player')
					.then(([playerReqStatus, d]) => {
						if (playerReqStatus === 401)
							return this.requestToken(true)
						this.playerData = d as unknown as SpotifyApi.CurrentlyPlayingObject
						this.emit('player-data-update')
					})
			}, 1500)
		})
	}

	private async callSpotifyApi(path: string, type: 'GET' | 'POST' | 'PUT' = 'GET', data?: null | any,
	                             apiUrl?: string, secondTry = false):
		Promise<[number, any]> {
		const token = this.token

		return new Promise(resolve => {
			const xhr = new XMLHttpRequest()
			xhr.open(type, `${apiUrl ? apiUrl : SPOTIFY_API_URL}${path}`, true)
			xhr.setRequestHeader('Authorization', `Bearer ${token}`)

			if (type !== 'GET')
				xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

			xhr.addEventListener('readystatechange', () => {
				if (xhr.readyState === 4)
					if (xhr.status === 401) {
						this.token = null
						if (!secondTry)
							this.requestToken(true).then(() =>
								this.callSpotifyApi(path, type, data, apiUrl, true).then(d => resolve(d))
							)
						else
							resolve([0, {}])
					} else
						try {
							resolve([xhr.status, JSON.parse(xhr.responseText)])
						} catch (e) {
							resolve([xhr.status, {}])
						}
			})

			xhr.send(type === 'GET' || !data ? null : JSON.stringify(data))
		})
	}
}

export default SpotifyIntegration.getInstance()
