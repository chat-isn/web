import crypto from 'crypto'

/**
 * Used to encrypt all communication
 * Should not be confused with the crypto module
 */
export default class Crypto {
	private dh = crypto.getDiffieHellman( 'modp14' )
	private aesKey: Buffer | null = null
	private aesIv: Buffer | null = null

	generateKeys() {
		this.dh.generateKeys()
	}

	getPublicKey() {
		return this.dh.getPublicKey()
	}

	computeSecret(buffer: Buffer) {
		// let's take the 256 first bits ! (32 first chars of the buffer BECAUSE
		// 32 * 8 = 256 !!)
		this.aesKey = this.dh.computeSecret( buffer ).slice(0, 32)
		this.aesIv = this.sha256( this.aesKey ).slice(0, 16)
	}

	crypt(str: string) {
		if (!this.aesKey || !this.aesIv)
			return

		const cipher = crypto.createCipheriv(
			'aes-256-cbc',
			this.aesKey,
			this.aesIv
		)

		return cipher.update(str, 'utf8', 'hex') +
			cipher.final('hex')
	}

	decrypt(str: string) {
		if (!this.aesKey || !this.aesIv)
			return

		const decipher = crypto.createDecipheriv(
			'aes-256-cbc',
			this.aesKey,
			this.aesIv
		)

		return decipher.update(str, 'hex', 'utf8') +
			decipher.final('utf8')
	}

	private sha256(str: Buffer) {
		return crypto.createHash('sha256').update(str).digest()
	}
}
