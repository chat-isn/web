import i18n from '../i18n/i18n'

export default class RelativeDate {
	private readonly date: Date

	constructor(date: Date | number) {
		this.date = new Date()
		if (date instanceof Date)
			this.date = date
		if (typeof date === 'number')
			this.date = new Date(date)
	}

	getSettings() {
		return {
			days: ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'].map(d => i18n.get(`date.${d}`)),
			timeRange: i18n.get('time.range'),
			timeFormat: i18n.get('time.format'),
			dateFormat: i18n.get('date.format')
		}
	}

	formatDay() {
		const { dateFormat } = this.getSettings(),
			{ date } = this
		return dateFormat
			.replace('dd', date.getDate().toString(10).padStart(2, '0'))
			.replace('mm', date.getMonth().toString(10).padStart(2, '0'))
			.replace('yyyy', date.getFullYear().toString(10).padStart(2, '0'))
	}

	formatHour() {
		const { timeFormat, timeRange } = this.getSettings(),
			{ date } = this
		if (timeRange === '24h')
			return timeFormat
				.replace('HH', date.getHours().toString(10).padStart(2, '0'))
				.replace('MM', date.getMinutes().toString(10).padStart(2, '0'))
		else {
			let h = date.getHours()
			const m = date.getMinutes(),
				  tt = h >= 12 ? 'PM' : 'AM'
			h = h % 12
			h = h === 0 ? 12 : h // midnight
			return timeFormat
				.replace('HH', h.toString(10).padStart(2, '0'))
				.replace('MM', m.toString(10).padStart(2, '0'))
				.replace('TT', tt)
		}
	}

	expressRelativeTime() {
		const settings = this.getSettings()

		const topNow = this.topUpDate(new Date())
		const top = this.topUpDate(new Date(this.date))

		const daysDifference = (topNow.getTime() - top.getTime()) / (24 * 60 * 60 * 1000)

		if (daysDifference === 0) // today
			 return i18n.format('date.relative-time', {
			 	 day: i18n.get('date.today'),
				 hour: this.formatHour()
			 })
		else if (daysDifference === 1) // yesterday
			return i18n.format('date.relative-time', {
				day: i18n.get('date.yesterday'),
				hour: this.formatHour()
			})
		else if (daysDifference < 7) // last week
			return i18n.format('date.relative-time', {
				day: i18n.format('date.last-format', {
					day: settings.days[ top.getDay() ]
				}),
				hour: this.formatHour()
			})
		else return i18n.format('date.relative-time', {
				day: this.formatDay(),
				hour: this.formatHour()
			})
	}

	private topUpDate(date: Date) {
		date.setHours(0)
		date.setMinutes(0)
		date.setSeconds(0)
		date.setMilliseconds(0)
		return date
	}
}
