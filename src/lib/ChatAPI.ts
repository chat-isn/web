import {EventEmitter} from 'events'
import {sha256} from 'js-sha256'
import log from '../util/log'
import applyNewSelfMessage from '../util/apply-new-self-message'
import Connection from './Connection'
import WebSocketMessage from './WebSocketMessage'
import {
	IBaseResponse,
	IChangeMailResponse,
	IChangePasswordResponse,
	ICreateChatResponse,
	IDeleteChatResponse,
	IErrorMessage, IFetchMessagesResponse,
	IFetchUsersInChatResponse,
	IFetchUsersResponse,
	IListChatResponse,
	IReceivedGameMessage,
	IReceivedMessage,
	ServerEvents
} from './ServerResponses'
import RequestTypes, {IChangeMailFields, IChangePasswordFields,
	ICreateGameMessageFields, ICreateMessageFields} from './RequestTypes'
import {checkRule, IFieldRuleResult} from './FieldRules'
import {AuthRules} from './Authentication'
import RichMessage from './RichMessage'

export type UserId = number
export interface IUser {
	id: UserId
	name: string
}
export interface ISelf extends IUser {
	email: string
}

export type ChatId = number
export interface IDMChatPreviewPartial {
	id: ChatId
	users: IUser[]
	lastMessage?: string
}

export interface IDMChatPreview extends IDMChatPreviewPartial {
	name: string
}

export interface IMessage {
	sender: IUser
	time: number
	content: string | RichMessage
	sending?: boolean | 'fail'
}
export interface ISendingMessage extends IMessage {
	sending: true
}
export interface IFailedMessage extends IMessage {
	sending: 'fail'
}

interface IChatAPIEvents {
	on(event: 'message', listener: (chatid: number, message: IMessage) => any): this
	on(event: 'new-self-message', listener: (chatid: number, message: ISendingMessage) => any): this
	on(event: 'error-self-message', listener: (chatid: number, message: IFailedMessage) => any): this
	on(event: 'new-dm', listener: (dm: IDMChatPreview) => any): this
	on(event: 'del-dm', listener: (chatid: number) => any): this
	on(event: 'dms-fetched', listener: () => any): this
}

export default class ChatAPI extends EventEmitter implements IChatAPIEvents {
	authedUser: ISelf = {
		id: 1, name: 'Maeeen', email: 'kek@kek.kek'
	}

	private DMs: IDMChatPreview[] | null = null
	private DMsContent: { [chatId: number]: IMessage[] } = {}
	private usersCache: { [id: number]: IUser } = {}

	constructor(protected connection: Connection) {
		super()

		this.connection.on('chat-message', (message: IReceivedMessage) => {
			(async () => {

				const sender = (await this.getUsers([message.userid]))[message.userid]
				const msg: IMessage = {
					sender,
					time: message.time ? message.time : Date.now(),
					content: message.message
				}
				msg.content = RichMessage.parse(msg, message.message)

				this.emit('message', message.chatid, msg)
				this.emit(`message-${message.chatid}`, msg)

				if (msg.sender.id !== this.authedUser.id)
					if (this.DMsContent[message.chatid])
						this.DMsContent[message.chatid].unshift(msg)
					else
						this.DMsContent[message.chatid] = [msg]
			})()
		})

		this.connection.on('game-chat-message', (message: IReceivedGameMessage)  => {
			if (message.userid === this.authedUser.id)
				return

			this.emit(`game-message-${message.chatid}`, message.message)
		})

		this.connection.on('delete-chat-event', (message: IDeleteChatResponse) => {
			if (this.DMs)
				this.DMs = this.DMs.filter(v => v.id !== message.chatid)
			this.emit('del-dm', message.chatid)
		})

		this.connection.on('create-chat-event', (message: ICreateChatResponse) => {
			(async () => {
				const dm: IDMChatPreview = {
					id: message.chatid,
					users: Object.values(await this.getUsers(
						Object.values((await this.fetchUserIdsInDM([message.chatid]))[message.chatid])
					)),
					name: ''
				}
				dm.name = this.formatDMName(dm)
				if (Array.isArray(this.DMs) && !this.DMs.find(d => d.id === dm.id))
					this.DMs.push(dm)
				else
					this.DMs = [ dm ]
				this.emit('new-dm', dm)
			})()
		})
	}

	clearCache() {
		this.DMs = null
		this.usersCache = {}
		this.DMsContent = {}
	}

	async changeMail(newMail: string) {
		if (checkRule(newMail, AuthRules.mail) !== IFieldRuleResult.OK)
			return false

		const response: IChangeMailResponse | null =
			await this.connection.send<IChangeMailResponse>(
				new WebSocketMessage<IChangeMailResponse, IChangeMailFields>(RequestTypes.CHANGE_MAIL, {
					userid: this.authedUser.id.toString(),
					mail: this.authedUser.email,
					newmail: newMail
				})
			)

		if (!response)
			return false

		if (response.type === ServerEvents.ERROR)
			return false

		this.authedUser.email = newMail
		return true
	}

	async changePassword(oldPassword: string, newPassword: string) {
		if (checkRule(newPassword, AuthRules.password) !== IFieldRuleResult.OK)
			return false

		oldPassword = sha256(oldPassword)
		newPassword = sha256(newPassword)

		const response: IChangePasswordResponse | null =
			await this.connection.send<IChangePasswordResponse>(
				new WebSocketMessage<IChangePasswordResponse, IChangePasswordFields>(RequestTypes.CHANGE_PASSWORD, {
					userid: this.authedUser.id.toString(),
					password: oldPassword,
					newpassword: newPassword
				})
			)

		if (!response)
			return false

		if (response.type === ServerEvents.ERROR)
			return (response as IErrorMessage).errorid

		return newPassword
	}

	async getDM(chatId: number): Promise<IDMChatPreview | null> {
		if (!this.DMs)
			this.DMs = await this.getDMs()

		return this.getDMSync(chatId) as IDMChatPreview
	}

	getDMSync(chatId: number) {
		if (!this.DMs)
			return

		const search = this.DMs.filter(dm => dm.id === chatId)

		if (search.length > 0)
			return search[0]
		return null
	}

	async getDMs(): Promise<IDMChatPreview[]> {
		if (this.DMs)
			return this.DMs

		this.DMs = await this.fetchDMs()
		this.emit('dms-fetched')
		return this.DMs
	}

	async getDMMessages(chatId: number, from = 0, length = 50): Promise<IMessage[] | false> {
		// check if dm exists
		const dms = await this.getDMs()
		let fromModified = from

		if (!dms.map(dm => dm.id).includes(chatId))
			return false

		if (this.DMsContent.hasOwnProperty(chatId)) {
			const cache = this.DMsContent[chatId]
			const cacheResult = cache.slice(from, from + length)
			// we have all msg in cache
			if (cacheResult.length === length)
				return cacheResult
			// fetch the remaining ones
			fromModified += cacheResult.length
		} else
			this.DMsContent[chatId] = []

		const result = await this.fetchDMMessages(chatId, fromModified, from + length)

		this.DMsContent[chatId].splice
				.apply(this.DMsContent[chatId],
					[fromModified, 0, ...result])

		return this.DMsContent[chatId].slice(from, from + length).map(msg => {
			if (typeof msg.content === 'string')
				msg.content = RichMessage.parse(msg, msg.content)

			return msg
		})
	}

	async checkForExistantDM(userIds: UserId[]) {
		const existingDMs: IDMChatPreview[] = await this.getDMs()

		existingDMs.filter(dm => dm.users.map(u => u.id).every(id => userIds.includes(id)))

		if (existingDMs.length > 0)
			return existingDMs[0].id
		return null
	}

	async createDM(userIds: UserId[], ignoreExistantChat = false) {
		if (!userIds.includes(this.authedUser.id))
			userIds.push(this.authedUser.id)

		if (userIds.length < 2)
			return null

		if (!ignoreExistantChat) {
			const existantDM = await this.checkForExistantDM(userIds)
			if (existantDM)
				return existantDM
		}

		const newDM: ICreateChatResponse | null = await this.connection.send<ICreateChatResponse>(
			new WebSocketMessage(RequestTypes.CREATE_CHAT, {
				user: userIds.join('-')
			})
		)

		if (newDM) {
			const dm: IDMChatPreview = {
				id: newDM.chatid,
				users: Object.values(await this.getUsers(userIds)),
				name: ''
			}
			dm.name = this.formatDMName(dm)
			if (Array.isArray(this.DMs))
				this.DMs.push(dm)
			else
				this.DMs = [ dm ]

			return newDM.chatid
		}

		log('ChatAPI', 'No response from server, CREATE_CONV')
		return null

	}

	async getUsers(usersId: UserId[]): Promise<{ [id: number]: IUser }> {
		// filter those we have in cache and those we do not
		const usersToFetch: UserId[] = []
		const fetchedUsers: { [id: number]: IUser } = {}
		usersId.forEach(userId => {
			if (userId === this.authedUser.id)
				fetchedUsers[userId] = this.authedUser
			else if (this.usersCache.hasOwnProperty(userId))
				fetchedUsers[userId] = this.usersCache[userId]
			else
				usersToFetch.push(userId)
		})
		const fetchResult = await this.fetchUsers(usersToFetch)

		this.usersCache = {...this.usersCache, ...fetchResult}

		return {...fetchedUsers, ...fetchResult}
	}

	async fetchDMMessages(chatid: number, from = 0, length = 50): Promise<(IMessage)[]> {
		const dms = await this.getDMs()

		if (!dms.map(dm => dm.id).includes(chatid))
			return []

		from = Math.max(from, 0)

		if (length <= 0)
			return []

		const newDM: IFetchMessagesResponse | null = await this.connection.send<IFetchMessagesResponse>(
			new WebSocketMessage(RequestTypes.FETCH_MESSAGES, {
				chatid: chatid.toString(10),
				messages: `${from}-${from + length}`
			})
		)

		if (!newDM)
			return []

		if (newDM.maxmessages)
			return await this.fetchDMMessages(chatid, from, newDM.maxmessages - from)

		const { messageList: ml } = newDM
		if (ml) {
			const tempMessageArray: IMessage[] = []
			const userIds = Object.keys(ml).map(id => parseInt(id, 10))
			const users = await this.getUsers(userIds)

			userIds.forEach(senderId => {
				if (ml[senderId])
					ml[senderId].forEach(serializedMessage => {
						const msg: IMessage = {
							sender: users[senderId],
							time: parseInt(serializedMessage[0], 10),
							content: serializedMessage[1]
						}
						msg.content = RichMessage.parse(msg, msg.content as string)

						tempMessageArray.push(msg)
					})
			})

			return tempMessageArray.sort((a, b) => a.time - b.time).reverse()
		}

		return []
	}

	async deleteChat(chatid: number) {
		const dms = await this.getDMs()

		if (!dms.map(dm => dm.id).includes(chatid))
			return true

		const response: IDeleteChatResponse | null = await this.connection.send<IDeleteChatResponse>(
			new WebSocketMessage(RequestTypes.DELETE_CHAT, {
				userid: this.authedUser.id,
				chatid
			})
		)

		if (response)
			return true
		return false
	}

	async sendRichMessageToChat(richMessage: RichMessage, chatid: number): Promise<boolean | null> {
		return this.sendMessageToChat(richMessage, chatid)
	}

	async sendMessageToChat(message: string | RichMessage, chatid: number, game = false): Promise<boolean | null> {
		const customWSMessage = new WebSocketMessage<IBaseResponse, ICreateGameMessageFields | ICreateMessageFields>(
			game ? RequestTypes.CREATE_GAME_MESSAGE : RequestTypes.CREATE_MESSAGE, {
				user: this.authedUser.id.toString(),
				chatid: chatid.toString(),
				message: message.toString().trim(),
				userlist: game ? (await this.getDMs())
					.filter(dm => dm.id === chatid)[0].users
					.map(u => u.id).join('-') : ''
			})

		const msg: IMessage = {
			sender: this.authedUser,
			time: Date.now(),
			content: typeof message === 'string' ? message.trim() : message,
			sending: true
		}

		if (message instanceof RichMessage)
			message.message = msg

		if (!game) {
			this.emit('new-self-message', chatid, msg)
			if (this.DMsContent[chatid])
				this.DMsContent[chatid].unshift(msg)
			else
				this.DMsContent[chatid] = [msg]
		}

		const EVENT_NAME = game ? 'game-chat-message' : 'chat-message'

		if (!this.connection.connection)
			return null

		this.connection.connection.send(customWSMessage.toJSON())
		return new Promise(resolve => {

			const removeListeners = () => {
				this.connection.removeListener(EVENT_NAME, listener)
				this.connection.removeListener(`message-${customWSMessage.id}`, errorListener)
			}

			const listener = (messageData: IReceivedMessage) => {
				if (messageData.message === message &&
					messageData.userid === this.authedUser.id &&
					messageData.chatid === chatid) {
					removeListeners()
					msg.sending = false

					if (!game)
						this.DMsContent[chatid] = applyNewSelfMessage(msg, this.DMsContent[chatid])
					resolve(true)
				}
			}
			const errorListener = (messageData: IErrorMessage) => {
				if (messageData.type === ServerEvents.ERROR) {
					log('ChatAPI', `Can't send ${game ? 'game ' : ''}message bc error ${messageData.errorid}`)
					removeListeners()
					if (!game) {
						msg.sending = 'fail'
						this.DMsContent[chatid] = applyNewSelfMessage(msg, this.DMsContent[chatid])
						this.emit('error-self-message', chatid, msg)
					}
					resolve(false)
				}
			}

			this.connection.on(EVENT_NAME, listener)
			this.connection.on(`message-${customWSMessage.id}`, errorListener)
		})
	}

	protected async fetchDMs(): Promise<IDMChatPreview[]> {

		const chatReponses: IListChatResponse | null =
			await this.connection.send<IListChatResponse>(
				new WebSocketMessage(RequestTypes.LIST_CHAT, { userid: this.authedUser.id })
			)

		if (!chatReponses) {
			log('ChatAPI', 'No response from server, LIST_CHAT')
			return []
		}

		if (!chatReponses.chatlist || chatReponses.chatlist === '')
			return []

		const chats = chatReponses.chatlist.split('-')
		const finalChats: { id: number; users: IUser[] }[] = []

		const chatsUsers = await this.fetchUserIdsInDM(chats)

		const usersToFetch = Object.values(chatsUsers).reduce((acc, val) => acc.concat(val), []) // Array#flat
			.filter((userId, i, arr) => arr.indexOf(userId) === i) // remove duplicates
		await this.getUsers(usersToFetch) // putting them in cache to fetch them later

		for (const chatId in chatsUsers)
			if (chatsUsers.hasOwnProperty(chatId)) {
				const chatIdN = parseInt(chatId, 10)
				finalChats.push({
					id: chatIdN,
					users: Object.values(await this.getUsers(chatsUsers[chatIdN]))
				})
			}

		return finalChats.map(dm => ({...dm, name: this.formatDMName(dm)}))
	}

	protected async fetchUserIdsInDM(chatIds: (number | string)[]): Promise<{ [chatId: number]: UserId[] }> {
		if (chatIds.length === 0)
			return []

		const userIds: IFetchUsersInChatResponse | null =
			await this.connection.send<IFetchUsersInChatResponse>(
				new WebSocketMessage(RequestTypes.FETCH_USERS_IN_CHAT, {
					chatlist: chatIds.join('-')
				})
			)

		if (!userIds) {
			log('ChatAPI', 'No response from server, FETCH_USERS_IN_CHAT')
			return []
		}

		const chatsUsers = userIds.chatlist
		const finalObj: { [chatId: number]: UserId[] } = {}

		for (const chat in chatsUsers)
			if (chatsUsers.hasOwnProperty(chat))
				finalObj[chat] = chatsUsers[chat].split('-').map(a => parseInt(a, 10))

		return finalObj
	}

	protected async fetchUsers(userIds: UserId[]) {
		if (userIds.length === 0)
			return []

		const users: IFetchUsersResponse | null =
			await this.connection.send<IFetchUsersResponse>(
				new WebSocketMessage(RequestTypes.FETCH_USERS, {
					users: userIds.join('-')
				})
			)

		if (!users) {
			log('ChatAPI', 'No response from server, FETCH_USERS')
			return []
		}

		const fetchedUsers: IUser[] = users.usernameList.map((name, i) => ({
			id: userIds[i],
			name
		}))

		const returnValue: { [id: number]: IUser } = {}
		fetchedUsers.map(user => returnValue[user.id] = user)
		return returnValue
	}

	private formatDMName = (dm: IDMChatPreviewPartial) => {
		if (dm.users.length === 1)
			return dm.users[0].name
		return dm.users
			.filter(user => user.id !== this.authedUser.id)
			.map(user => user.name)
			.join(', ')
	}
}
