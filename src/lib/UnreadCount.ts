import { EventEmitter } from 'events'
import { History } from 'history'
import appInstance from '../AppInstance'
import getHistory from '../components/util/getHistory'

interface IUnreadCount {
	on(event: 'count-added' | 'count-updated', listener: (chatId: number, count: number) => any): this
}

/**
 * @singleton
 */
export class UnreadCount extends EventEmitter implements IUnreadCount {
	private static instance: UnreadCount = new UnreadCount()
	unreadCounts: { [chatId: number]: number } = {}

	constructor() {
		super()

		appInstance.chatAPI.on('message', (chatid: number) => {
			const history = getHistory()

			if (!history)
				return

			if (history.location.pathname === `/app/chat/${chatid}`)
				return


			this.setCount(chatid, this.getCount(chatid) + 1)
		})

		appInstance.chatAPI.on('del-dm', (chatid: string) => {
			this.setCount(parseInt(chatid, 10), 0)
		})
	}

	static init() {
		return this.instance
	}

	setCount(chatId: number, count: number) {
		const oldCount = this.getCount(chatId)

		this.unreadCounts[chatId] = count

		if (oldCount < count)
			this.emit('count-added', chatId, count)

		this.emit('count-updated', chatId, count)
	}

	getCount(chatId: number) {
		if (!this.unreadCounts.hasOwnProperty(chatId))
			return 0
		return this.unreadCounts[chatId]
	}

	onPageChanged(history: History) {
		const { pathname } = history.location
		if (pathname.startsWith('/app/chat/')) {
			const chatId = parseInt(pathname.substring('/app/chat/'.length), 10)
			if (!isNaN(chatId))
				return this.clearBadge(chatId)
		}
	}

	private clearBadge(chatId: number) {
		if (!this.unreadCounts.hasOwnProperty(chatId))
			return

		this.setCount(chatId, 0)
	}
}

export default UnreadCount.init()
