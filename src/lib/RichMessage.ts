import {ComponentClass} from 'react'
import ScoreNotify from '../components/rich-messages/ScoreNotify'
import GameNotify from '../components/rich-messages/GameNotify'
import SpotifyListen from '../components/rich-messages/SpotifyListen'
import {IMessage} from './ChatAPI'

enum RichMessageTypes {
	SCORE_NOTIFY,
	GAME_NOTIFY,
	SPOTIFY_LISTEN
}
export { RichMessageTypes }

const RichMessageComponentMap: { [x in RichMessageTypes]: ComponentClass<{
	rm: RichMessage
	compact: boolean
}> } = {
	[RichMessageTypes.SCORE_NOTIFY]: ScoreNotify,
	[RichMessageTypes.GAME_NOTIFY]: GameNotify,
	[RichMessageTypes.SPOTIFY_LISTEN]: SpotifyListen
}

export default class RichMessage {
	constructor(public message: IMessage | null, public type: RichMessageTypes, public data: any) {

	}

	static parse(originalMessage: IMessage, msg: string) {
		try {
			const data = JSON.parse(msg)

			if (Object.keys(RichMessageTypes).includes(data.type.toString()))
				return new RichMessage(originalMessage, data.type, data.data)
		} catch (e) {
			return msg
		}
		return msg
	}

	getComponent() {
		return RichMessageComponentMap[this.type]
	}

	toJSON() {
		return JSON.stringify({
			type: this.type,
			data: this.data
		})
	}

	toString() {
		return this.toJSON()
	}
}
