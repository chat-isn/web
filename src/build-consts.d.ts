declare const VERSION: string
declare const COMMITHASH: string
declare const BRANCH: string
declare const DEV: boolean
declare const WEBSOCKET_SERVER: string
declare const SPOTIFY_WEB_SERVICE_SERVER: string

declare module '*.svg'
declare module '*.png'
