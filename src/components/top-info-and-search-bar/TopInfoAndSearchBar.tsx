import classnames from 'classnames'
import React, { ReactNode } from 'react'
import {RouteComponentProps, withRouter} from 'react-router'

import SEARCH_ICON from '../../img/icons/search-black-36dp.svg'
import DELETE_ICON from '../../img/icons/delete_forever-black-36dp.svg'
import HAMBURGER_ICON from '../../img/icons/menu-black-36dp.svg'
import CHAT_ICON from '../../img/icons/chat_bubble_outline-black-36dp.svg'

import BeautifulInput from '../beautiful-input/BeautifulInput'
import i18n from '../../i18n/i18n'
import appInstance from '../../AppInstance'
import deleteChatConfirmation from '../../util/delete-chat-confirmation'
import * as ChatListComponent from '../chat-list/ChatList'
import SearchResults from './SearchResults'
import * as style from './top-info-and-search-bar.less'


interface IChatSearchState {
	content: string | false
}

export class TopInfoAndSearchBar extends React.Component<RouteComponentProps, IChatSearchState> {
	private input = React.createRef<BeautifulInput>()
	private div = React.createRef<HTMLDivElement>()
	private searchResultDiv = React.createRef<HTMLDivElement>()

	constructor(props: RouteComponentProps) {
		super(props)
		this.state = { content: false }
	}

	componentDidMount() {
		window.addEventListener('keydown', this.shortcutHandler)
	}

	componentWillUnmount() {
		window.removeEventListener('keydown', this.shortcutHandler)
	}

	render(): ReactNode {
		const isChat =  /[/]app[/]chat[/]?/g.exec(this.props.location.pathname)
		const possibleChatId = /[/]app[/]chat[/](\d+)/g.exec(this.props.location.pathname)

		return <div className={ classnames(style.container, {
				[style.isInDm]: !!possibleChatId,
				[style.showHamburger]: !!isChat }) }>
			<div className={ style.hamburgerIcon } onClick={ () => {
				const { ChatList } = ChatListComponent,
					instance = ChatList.getInstance()
				if (instance)
					instance.setState({
						mobileOpenPx: instance.state.mobileOpenPx <= 0 ? ChatListComponent.CHAT_LIST_WIDTH : 0
					})
				return false
			} } onTouchStart={ e => e.stopPropagation() } onTouchEnd={ e => e.stopPropagation() }>
				{ /* e.stopPropagation() prevents the SideBar from closing itself like it wants to */ }
				<HAMBURGER_ICON />
			</div>
			<div className={ style.chatSearch } ref={ this.div }>
				<SEARCH_ICON />
				<BeautifulInput placeholder={ i18n.get('app.search') }
								className={ style.input } ref={ this.input }
								autoComplete='off'
								onFocus={ () => { this.refreshInputValue() } }
								onKeyUp={ () => { this.refreshInputValue() } }
								onBlur={ () => { this.onBlur() } } />

				<div className={ style.searchResultContainer } ref={ this.searchResultDiv }
					style={ { display: this.state.content ? 'block' : 'none' } }>
					<SearchResults search={ this.state.content } />
				</div>
			</div>
			<div className={ style.mobileDmInfo }>
				{ possibleChatId ? this.renderMobileDMInfo( parseInt(possibleChatId[1], 10) ) : null }
			</div>
		</div>
	}

	private renderMobileDMInfo(chatId: number) {
		const dm = appInstance.chatAPI.getDMSync(chatId)

		if (!dm) {
			appInstance.chatAPI.once('dms-fetched', () => this.forceUpdate())
			return null
		}
		return <>
			<CHAT_ICON />
				<p>{ dm.name }</p>
				<div className={ style.deleteIcon } onClick={ () => deleteChatConfirmation(dm) }>
					<DELETE_ICON />
				</div>
			</>
	}

	private onBlur() {
		this.setState({ content: false })
	}

	private shortcutHandler = ((e: KeyboardEvent) => {
		// Raccourcis: CTRL+P
		if (e.ctrlKey && e.key === 'p')
			if (this.input.current && this.input.current.inputRef.current) {
				this.input.current.inputRef.current.focus()
				e.preventDefault()
			}
	})

	private refreshInputValue() {
		if (!this.input.current)
			return

		const value = this.input.current.getValue()
		this.setState({ content: value ? value : '' })
	}
}

export default withRouter(TopInfoAndSearchBar)
