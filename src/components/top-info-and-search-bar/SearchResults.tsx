import React, { ReactNode } from 'react'
import {RouteComponentProps, withRouter} from 'react-router'

import CHAT_ICON from '../../img/icons/chat_bubble_outline-black-36dp.svg'
import SETTINGS_ICON from '../../img/icons/settings-black-36dp.svg'

import makeCancelable, {ICancellablePromise} from '../../util/make-cancelable'
import LoadingSpinner from '../loading-spinner/LoadingSpinner'
import userPreferences from '../../lib/UserPreferences'
import appInstance from '../../AppInstance'
import i18n from '../../i18n/i18n'
import {searchInExtraSettings} from '../../routes/settings-page/ExtraSettings'

import * as style from './top-info-and-search-bar.less'


interface IChatSearchProps {
	search: string | false
}

export interface ISearchResult {
	type: 'chat' | 'settings'
	friendlyName: string
	path: string
}

interface IChatSearchState {
	search: string | false
	results: ISearchResult[] | null
}

export class SearchResults extends React.Component<IChatSearchProps &
	RouteComponentProps<{}>, IChatSearchState> {
	requests: ICancellablePromise<ISearchResult[]>[] = []
	donePromises = 0

	constructor(props: IChatSearchProps & RouteComponentProps<{}>) {
		super(props)
		this.state = { results: null, search: false }
	}

	private static getIcon(result: ISearchResult) {
		switch (result.type) {
			case 'chat':
				return <CHAT_ICON />
			case 'settings':
				return <SETTINGS_ICON />
		}
	}

	shouldComponentUpdate(nextProps: Readonly<IChatSearchProps>,
	                      nextState: Readonly<IChatSearchState>): boolean {
		if (nextProps.search && nextProps.search !== nextState.search)
			this.search(nextProps.search)
		return true
	}

	render(): ReactNode {
		return <div className={ style.searchResult }>
			{ this.state.results === null ?
				<LoadingSpinner className={ style.loading } /> :
				( <>
					{ this.state.results.map((result: ISearchResult, i: number) =>
						// not onClick because https://stackoverflow.com/a/28963938
						<div key={i} className={style.result} onMouseDown={ () => this.redirect(`/${result.path}`) }>
							{ SearchResults.getIcon(result) }
							<span>{ result.friendlyName }</span>
						</div>
					)}
					{ this.donePromises === this.requests.length && this.state.results.length === 0 ?
						<p>{ i18n.get('app.search.no-results') }</p> : null }
					{ this.donePromises < this.requests.length ? <LoadingSpinner className={ style.loading } /> : null }
					</>
				)
			}
		</div>
	}

	private redirect(to: string) {
		this.props.history.push(to)
	}

	private search(searchQuery: string) {
		if (!searchQuery)
			return

		this.requests.forEach(req => req.cancel())
		this.requests = []
		this.donePromises = 0

		this.setState({
			search: searchQuery,
			results: [
				...userPreferences.search( searchQuery ),
				...searchInExtraSettings( searchQuery )
				]
		})

		this.requests [ this.requests.push( makeCancelable((async () =>
			(await appInstance.chatAPI.getDMs()).filter(dm => {
				if (dm.id.toString(10) === searchQuery)
					return true
				if (dm.users.filter(user => user.id !== appInstance.chatAPI.authedUser.id)
						.find(user => user.id.toString() === searchQuery
					|| user.name.toLowerCase().includes(searchQuery.toLowerCase())))
					return true
			}).map(foundDM => ({
				type: 'chat',
				friendlyName: foundDM.name,
				path: `app/chat/${foundDM.id}`
			}) as {
				type: 'chat'
				friendlyName: string
				path: string
			})
		)())) - 1].then(results => {
			this.donePromises++
			this.setState({
				results: [...this.state.results || [], ...results] // append the result
			} )
		})
	}
}

export default withRouter(SearchResults)
