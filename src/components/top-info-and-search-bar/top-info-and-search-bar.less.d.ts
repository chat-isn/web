/* eslint-disable */
declare namespace TopInfoAndSearchBarLessNamespace {
  export interface ITopInfoAndSearchBarLess {
    "chat-search": string;
    chatSearch: string;
    container: string;
    "delete-icon": string;
    deleteIcon: string;
    gradient: string;
    "hamburger-icon": string;
    hamburgerIcon: string;
    input: string;
    "is-in-dm": string;
    isInDm: string;
    loading: string;
    "mobile-dm-info": string;
    mobileDmInfo: string;
    result: string;
    "search-result": string;
    "search-result-container": string;
    searchResult: string;
    searchResultContainer: string;
    "show-hamburger": string;
    showHamburger: string;
  }
}

declare const TopInfoAndSearchBarLessModule: TopInfoAndSearchBarLessNamespace.ITopInfoAndSearchBarLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: TopInfoAndSearchBarLessNamespace.ITopInfoAndSearchBarLess;
};

export = TopInfoAndSearchBarLessModule;
