import React from 'react'
import {CSSTransition, TransitionGroup} from 'react-transition-group'
import {RouteComponentProps, withRouter} from 'react-router'
import notificationHandler from '../../lib/NotificationHandler'
import i18n from '../../i18n/i18n'
import ToolTip from '../tooltip/ToolTip'

import CLOSE_ICON from '../../img/icons/close-black-36dp.svg'

import * as style from './notifications.less'


export class Notifications extends React.Component<RouteComponentProps, {
	notifications: {
		title: string
		body: string
		icon: string
		url: string }[]
}> {

	constructor(props: RouteComponentProps) {
		super(props)
		this.state = { notifications: [] }
	}

	componentDidMount() {
		notificationHandler.on('notification', this.notificationListener)
	}

	componentWillUnmount() {
		notificationHandler.removeListener('notification', this.notificationListener)
	}

	render() {
		return <div className={ style.notificationContainer }>
			<TransitionGroup>
				{ this.state.notifications.map((notification, i) =>
					<CSSTransition key={i} timeout={500} classNames={{
						enter: style.notificationEnter,
						enterActive: style.notificationEnterActive,
						enterDone: style.notificationEnterActive,
						exit: style.notificationExit,
						exitActive: style.notificationExitActive
					}}>
						<div className={style.notification} onClick={ e => {
							if (e.currentTarget !== e.target)
								return
							this.props.history.push(notification.url)
							this.setState({ notifications: this.state.notifications.filter((el, i_) =>
									i_ !== i && el.url !== notification.url) })
						}}>
							<img src={ notification.icon } alt={'icon'} />
							<div className={style.notificationText}>
								<p>{ notification.title }</p>
								<p>{ notification.body }</p>
							</div>
							<div className={style.close} onClick={() => {
								this.setState({ notifications: this.state.notifications.filter((el, i_) => i_ !== i) })
							}}>
								<ToolTip wrapIt={true} text={ i18n.get('generic.close') }>
									<CLOSE_ICON />
								</ToolTip>
							</div>
						</div>
					</CSSTransition>
				) }
			</TransitionGroup>
		</div>
	}

	private notificationListener = (title: string, body: string, icon: string, url: string) => {
		this.setState({ notifications: [ ...this.state.notifications, {
			title, body, icon, url
		} ]})
		setTimeout(() => {
			const arr = Array.from(this.state.notifications)
			arr.shift()
			this.setState({ notifications: arr })
		}, 2500)
	}
}

export default withRouter(Notifications)
