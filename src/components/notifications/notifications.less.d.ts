/* eslint-disable */
declare namespace NotificationsLessNamespace {
  export interface INotificationsLess {
    close: string;
    notification: string;
    "notification-container": string;
    "notification-enter": string;
    "notification-enter-active": string;
    "notification-exit": string;
    "notification-exit-active": string;
    "notification-text": string;
    notificationContainer: string;
    notificationEnter: string;
    notificationEnterActive: string;
    notificationExit: string;
    notificationExitActive: string;
    notificationText: string;
  }
}

declare const NotificationsLessModule: NotificationsLessNamespace.INotificationsLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: NotificationsLessNamespace.INotificationsLess;
};

export = NotificationsLessModule;
