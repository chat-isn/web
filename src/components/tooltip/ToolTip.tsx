import classnames from 'classnames'
import React from 'react'
import * as style from './tooltip.less'

interface IProps {
	text: string
	children: JSX.Element[] | JSX.Element
	showMobile?: boolean
	/**
	 * Use it if your child doesn't return an HTMLElement
	 */
	wrapIt?: boolean
	delayMs?: number
}

interface IState {
	show: boolean
	x: number
	y: number
}

export default class ToolTip extends React.Component<IProps, IState> {
	private ref = React.createRef<HTMLDivElement>()
	private toolTipRef = React.createRef<HTMLDivElement>()
	private hover = false
	private delayTimeout: NodeJS.Timeout | null = null

	// private width =

	constructor(props: IProps) {
		super(props)
		this.state = { show: false, x: 0, y: 0 }
	}

	componentDidMount(): void {
		const el = this.ref.current
		if (!el)
			return

		el.addEventListener('touchstart', this.onMouseEnter, { passive: true })
		el.addEventListener('mouseenter', this.onMouseEnter)
		el.addEventListener('mousemove', this.onMouseMove)
		el.addEventListener('mouseleave', this.onMouseLeave)
	}

	componentWillUnmount() {
		const el = this.ref.current
		if (!el)
			return

		el.removeEventListener('touchstart', this.onMouseEnter)
		el.removeEventListener('mouseenter', this.onMouseEnter)
		el.removeEventListener('mousemove', this.onMouseMove)
		el.removeEventListener('mouseleave', this.onMouseLeave)

		if (this.delayTimeout)
			clearTimeout(this.delayTimeout)
	}

	render() {
		if (this.props.wrapIt)
			return <div ref={this.ref}>
				{ this.props.children }
				{ this.state.show ? this.renderToolTip() : null }
			</div>

		return React.Children.map( this.props.children, child =>
			React.cloneElement(child as React.ReactElement,
				{ ref: this.ref }, (child as React.ReactElement).props.children,
				this.state.show ? this.renderToolTip() : null)
		)
	}

	get timeout() {
		return this.props.delayMs
	}

	private calculatePosition() {
		const el = this.toolTipRef.current

		if (!el)
			return

		const boundingRect = el.getBoundingClientRect()

		// overflowing to the right
		const widthDiff = window.innerWidth - (boundingRect.width + boundingRect.left)
		if (widthDiff < 0)
			this.setState({
				x: this.state.x + widthDiff
			})

		// overflowing to the bottom
		const heightDiff = window.innerHeight - (boundingRect.height + boundingRect.top)
		if (heightDiff < 0)
			this.setState({
				y: this.state.y + heightDiff - 20
			})
	}

	private delayedShow = () => {
		if (this.delayTimeout) {
			clearTimeout(this.delayTimeout)
			this.delayTimeout = null
		}

		if (this.hover)
			this.setState({ show: true }, () => this.calculatePosition())
	}

	private onMouseEnter = () => {
		this.hover = true
		if (!this.timeout)
			this.delayedShow()
		else
			if (!this.delayTimeout)
				this.delayTimeout = setTimeout(() => this.delayedShow(), this.timeout)
	}

	private onMouseLeave = () => {
		this.hover = false
		this.setState({ show: false })
	}

	private onMouseMove = (e: MouseEvent) => {
		this.setState({ x: e.clientX, y: e.clientY + 20 }, () => this.calculatePosition())
	}

	private renderToolTip() {
		return <div className={ classnames(style.tooltip, {
			[style.stillShowTooltip]: this.props.showMobile
		}) } style={{
			left: `${this.state.x}px`,
			top: `${this.state.y}px`
		}} ref={this.toolTipRef}>{ this.props.text }</div>
	}
}
