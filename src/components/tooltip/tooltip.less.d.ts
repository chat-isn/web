/* eslint-disable */
declare namespace TooltipLessNamespace {
  export interface ITooltipLess {
    "appearing-tooltip": string;
    appearingTooltip: string;
    gradient: string;
    "still-show-tooltip": string;
    stillShowTooltip: string;
    tooltip: string;
  }
}

declare const TooltipLessModule: TooltipLessNamespace.ITooltipLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: TooltipLessNamespace.ITooltipLess;
};

export = TooltipLessModule;
