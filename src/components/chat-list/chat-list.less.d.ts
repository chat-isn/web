/* eslint-disable */
declare namespace ChatListLessNamespace {
  export interface IChatListLess {
    "black-fade": string;
    blackFade: string;
    "chat-icon": string;
    "chat-list": string;
    chatIcon: string;
    chatList: string;
    container: string;
    content: string;
    "delete-icon": string;
    deleteIcon: string;
    gradient: string;
    selected: string;
    shimmer: string;
    skeleton: string;
    "skeleton-loading": string;
    skeletonLoading: string;
    "surprising-delete-icon": string;
    surprisingDeleteIcon: string;
    "transition-enabled": string;
    transitionEnabled: string;
  }
}

declare const ChatListLessModule: ChatListLessNamespace.IChatListLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: ChatListLessNamespace.IChatListLess;
};

export = ChatListLessModule;
