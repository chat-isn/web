import React, { ReactNode } from 'react'
import classnames from 'classnames'
import { RouteComponentProps, withRouter } from 'react-router'
import {NavLink} from 'react-router-dom'

import NEW_CHAT_ICON from '../../img/icons/add_circle_outline-black-36dp.svg'
import CHAT_ICON from '../../img/icons/chat_bubble_outline-black-36dp.svg'
import DELETE_ICON from '../../img/icons/delete_forever-black-36dp.svg'

import makeCancelable, { ICancellablePromise } from '../../util/make-cancelable'
import {IDMChatPreview} from '../../lib/ChatAPI'
import i18n from '../../i18n/i18n'
import appInstance from '../../AppInstance'
import ToolTip from '../tooltip/ToolTip'
import UnreadCount from '../../lib/UnreadCount'
import Badge from '../badge/Badge'
import deleteChatConfirmation from '../../util/delete-chat-confirmation'
import * as style from './chat-list.less'

const CHAT_LIST_WIDTH = 270
const DELTA_Y_MAX = 50

interface IChatListProps {
	onDMFetched(): any
}

interface IChatListState {
	content: IDMChatPreview[] | false
	mobileOpenPx: number
	mobileTransition: boolean
}

export class ChatList extends React.Component<RouteComponentProps<{
	chatId: string
}> & IChatListProps, IChatListState> {
	private static instance: ChatList | null = null

	private request: ICancellablePromise<IDMChatPreview[]> | null = null
	private touchCoordinates: {
		x: number
		y: number
		initialY: number
		time: number
		speed: number
	} | null = null

	constructor(props: RouteComponentProps<{ chatId: string }> & IChatListProps) {
		super(props)
		this.state = {
			content : false,
			mobileOpenPx: 0,
			mobileTransition: true
		}
	}

	static getInstance() {
		return this.instance
	}

	componentDidMount(): void {
		ChatList.instance = this

		this.request = makeCancelable( appInstance.chatAPI.getDMs() )
		this.request.then(content => {
			this.setState({ content })
			this.props.onDMFetched()
		})
		appInstance.chatAPI.on('new-dm', this.newDMHandler)
		appInstance.chatAPI.on('del-dm', this.delDMHandler)

		window.addEventListener('touchstart', this.touchStartHandler)
		window.addEventListener('touchmove', this.touchMoveHandler)
		window.addEventListener('touchend', this.touchEndHandler)
	}

	componentWillUnmount(): void {
		if (this.request)
			this.request.cancel()
		appInstance.chatAPI.removeListener('new-dm', this.newDMHandler)
		appInstance.chatAPI.removeListener('del-dm', this.delDMHandler)

		window.removeEventListener('touchstart', this.touchStartHandler)
		window.removeEventListener('touchmove', this.touchMoveHandler)
		window.removeEventListener('touchend', this.touchEndHandler)
	}

	render(): ReactNode {
		let content: (IDMChatPreview | null)[] = this.state.content === false ?
				new Array(8).fill(null) : this.state.content

		content = [...content].reverse()

		return <div className={ style.container }>
			<div className={ classnames(style.chatList, {
				[style.skeleton]: this.state.content === false,
				[style.transitionEnabled]: this.state.mobileTransition
			})} style={{
				left: `${this.state.mobileOpenPx - CHAT_LIST_WIDTH }px`
			}} >
				<div className={ classnames({
						[style.selected]: this.props.match.params.chatId === 'new' })
					}>
					<NavLink to='/app/contacts'>
						<NEW_CHAT_ICON />
						<div>
							<p>{ i18n.get('app.chat.create-dm') }</p>
						</div>
					</NavLink>
				</div>
				{ content.map((dmchat: null | IDMChatPreview, i: number) =>
					<div key={i} className={ classnames({
							[style.selected]: dmchat ? this.isItThisChat(dmchat.id) : false })
					}>
						<NavLink to={`/app/chat/${dmchat ? dmchat.id : ''}`}>
							<div className={ style.surprisingDeleteIcon }
							     onClick={ e => {
							            deleteChatConfirmation(dmchat)
								        e.stopPropagation()
							     } }>
								<Badge
										onMount={ self => {
											const listener = () => {
												self.forceUpdate()
											}

											UnreadCount.on('count-added', listener)
											return listener
										} }

										onUnmount={ (self, listener) => {
											if (listener)
												UnreadCount.removeListener('count-added', listener as any)
										} }
										count={ () => dmchat ? UnreadCount.getCount(dmchat.id) : 0 }>
									<ToolTip text={ i18n.get('app.chat.delete') } wrapIt={true} delayMs={1250}>

										<div className={ style.chatIcon }>
											<CHAT_ICON />
										</div>
										<div className={ style.deleteIcon }>
											<DELETE_ICON />
										</div>
									</ToolTip>
								</Badge>
							</div>


							<div className={ style.content }>
								<p>{ dmchat ? dmchat.name : 'lol' }</p>
								{ dmchat && dmchat.lastMessage ? <p>{ dmchat.lastMessage }</p> : null }
							</div>
						</NavLink>
					</div>
				)}
			</div>
		</div>
	}

	private touchStartHandler = (ev: TouchEvent): void => {
		const touches = ev.touches
		if (touches.length > 1)
			return

		const touch = touches[0]

		this.touchCoordinates = {
			x: touch.clientX,
			y: touch.clientY,
			initialY: touch.clientY,
			time: Date.now(),
			speed: 0
		}
		this.setState({ mobileTransition: false })
	}

	private touchMoveHandler = (ev: TouchEvent): void => {
		const touches = ev.touches
		if (touches.length > 1 || !this.touchCoordinates)
			return

		const touch = touches[0],
			deltaX = touch.clientX - this.touchCoordinates.x,
			now = Date.now()

		if (Math.abs(touch.clientY - this.touchCoordinates.initialY) > DELTA_Y_MAX)
			this.setState({
				mobileOpenPx: 0
			})
		else {
			if (deltaX > 0)
				this.setState({
					mobileOpenPx: Math.min(this.state.mobileOpenPx + deltaX, CHAT_LIST_WIDTH)
				})
			if (deltaX < 0)
				this.setState({mobileOpenPx: Math.max(this.state.mobileOpenPx + deltaX, 0)})
		}

		this.touchCoordinates = {
			x: touch.clientX,
			y: touch.clientY,
			initialY: this.touchCoordinates.initialY,
			time: now,
			speed: deltaX / (now - this.touchCoordinates.time)
		}
	}

	private touchEndHandler = () => {
		if (this.touchCoordinates)
			this.setState({
				mobileOpenPx: this.touchCoordinates.speed > 0 &&
					Math.abs(this.touchCoordinates.y - this.touchCoordinates.initialY) < DELTA_Y_MAX
					? CHAT_LIST_WIDTH : 0,
				mobileTransition: true
			})

		this.touchCoordinates = null
	}

	private isItThisChat(chatId: number) {
		try {
			return parseInt(this.props.match.params.chatId, 10) === chatId
		} catch (e) {
			return false
		}
	}

	private newDMHandler = (dm: IDMChatPreview) => {
		if (Array.isArray(this.state.content) && this.state.content.find(dd => dd.id === dm.id))
			this.forceUpdate()
		else
			this.setState({content: [...(this.state.content === false ? [] : this.state.content), dm]})
	}

	private delDMHandler = (chatid: number) => {
		if (Array.isArray(this.state.content))
			this.setState({content: this.state.content.filter(dm => dm.id !== chatid)})
	}
}

export default withRouter(ChatList)
export { CHAT_LIST_WIDTH }
