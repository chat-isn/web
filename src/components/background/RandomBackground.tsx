import React, { ReactNode } from 'react'
import userPreferences from '../../lib/UserPreferences'
import AnimatedGradient from './AnimatedGradient'
import ImageBackground from './ImageBackground'
import * as style from './random-bckg.less'

/**
 * Liste des fonds disponibles
 * On a utilisé une fonction afin que le texte
 * soit rafraichi pour l'i18n à l'update
 */
const BACKGROUNDS = [
	(): ReactNode => <AnimatedGradient />,
	(): ReactNode => <ImageBackground
		blur={'2px'}
		title={'an alley like a background of anime'}
		src={ require('../../img/random-backgrounds/eraplatonico_8916614485.jpg').default }
		imageUrl = { 'https://www.flickr.com/photos/83823034@N06/8916614485' }
		author={ '@eraplatonico' }
		authorPlatform={ 'Flickr' }
	/>,
	(): ReactNode => <ImageBackground
		src={ require('../../img/random-backgrounds/chebynkin_tokyo.jpg').default }
		author={ 'Arseniy Chebynkin' }
		authorPlatform={ 'artstation' }
	/>,
	(): ReactNode => <ImageBackground
		src={ require('../../img/random-backgrounds/bliss.jpg').default }
		title={ 'Bliss (1996)' }
		author={ 'Charles O\'Rear' }
		authorPlatform={ 'Corbis' }
	/>,
	(): ReactNode => <ImageBackground
		src={ require('../../img/random-backgrounds/joeyjazz_under_the_strange_horizon.jpg').default }
		title={ 'Under the strange horizon' }
		author={ 'JoeyJazz' }
		authorPlatform={ 'deviantart' }
	/>,
	(): ReactNode => <ImageBackground
		title={ 'Савёловская Metro Station' }
		src={ require('../../img/random-backgrounds/videofrommoscow_savelovskaya.jpg').default }
		author={ 'VideoFromMoscow' }
		authorPlatform={ 'YouTube' }
	/>,
	(): ReactNode => <ImageBackground
		src={ require('../../img/random-backgrounds/chasingartwork_my_heart_laid_bare.jpg').default }
		title={ 'My heart laid bare' }
		author={ 'ChasingNetwork' }
		authorPlatform={ 'deviantart' }
	/>,
	(): ReactNode => <ImageBackground
		src={ require('../../img/random-backgrounds/unknown_osaka.jpg').default }
		title={ 'Osaka' }
		noSource={ 'u/HellsJuggernaut, r/wallpaper'}
	/>,
	(): ReactNode => <ImageBackground
		src={ require('../../img/random-backgrounds/lockwould_banner.jpg').default }
		author={ 'lockwould' }
		authorPlatform={ 'YouTube' }
	/>,
	(): ReactNode => <ImageBackground
		src={ require('../../img/random-backgrounds/catzz_atarashi-ni-tsu-no.jpg').default }
		title={ '新しい日の' }
		author={ 'catzz' }
		authorPlatform={ 'pixiv' }
	/>,
	(): ReactNode => <ImageBackground
		src={ require('../../img/random-backgrounds/Golden-Gate-Bridge-San-Francisco.jpg').default }
		title={ 'The Golden Bridge' }
		blur={'2px'}
		author={ 'Robert Glusic' }
		authorPlatform={ 'Getty Images' }
	/>
]

export { BACKGROUNDS }

const randomBackgroundId = Math.floor(Math.random() * BACKGROUNDS.length)

/**
 * Représente le composant chargé de mettre un
 * fond aléatoire parmis @see{BACKGROUNDS}
 */
export default class RandomBackground extends React.Component {
	render(): React.ReactNode {
		const pref = userPreferences.getById('background')

		let id = pref === 'random' ? randomBackgroundId : parseInt(pref, 10)

		if (id > BACKGROUNDS.length)
			id = randomBackgroundId

		return <div className={style.background}>
			{ BACKGROUNDS[ id ]() }
			<div className={style.backgroundDarken}/>
		</div>
	}
}
