/* eslint-disable */
declare namespace AnimatedGradientLessNamespace {
  export interface IAnimatedGradientLess {
    background: string;
    gradient: string;
  }
}

declare const AnimatedGradientLessModule: AnimatedGradientLessNamespace.IAnimatedGradientLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: AnimatedGradientLessNamespace.IAnimatedGradientLess;
};

export = AnimatedGradientLessModule;
