/* eslint-disable */
declare namespace ImageBackgroundLessNamespace {
  export interface IImageBackgroundLess {
    background: string;
    shadow: string;
  }
}

declare const ImageBackgroundLessModule: ImageBackgroundLessNamespace.IImageBackgroundLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: ImageBackgroundLessNamespace.IImageBackgroundLess;
};

export = ImageBackgroundLessModule;
