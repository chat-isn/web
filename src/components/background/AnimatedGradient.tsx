import React from 'react'
import * as style from './animated-gradient.less'

export default class AnimatedGradient extends React.Component {

	render(): React.ReactNode {
		return <div className={style.background}></div>
	}
}
