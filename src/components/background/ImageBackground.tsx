import React, { ReactNode } from 'react'
import i18n from '../../i18n/i18n'
import * as style from './image-background.less'

interface IImageBackgroundProps {
	blur?: string
	src: string
	imageUrl?: string
	author?: string
	authorPlatform?: string
	noSource?: string
	title?: string
}

export default class ImageBackground extends React.Component<IImageBackgroundProps> {
	render(): ReactNode {
		return <div className={ style.background }>
			<img src={ this.props.src } style={ { filter: `blur(${this.props.blur})` } } alt={'background...'}/>
			<div>
				<div className={ style.shadow } />
				{ this.props.title ? <p><i>{this.props.title}</i></p> : '' }
				{ this.props.author && this.props.authorPlatform ? <p>
					{ i18n.format('home.background.author', {
						author: this.props.author,
						platform: this.props.authorPlatform
					}) }</p> : '' }
				{ this.props.noSource ? <p>
					{ i18n.format('home.background.no-source', {
						found: this.props.noSource
					}) }</p> : '' }
			</div>
		</div>
	}
}
