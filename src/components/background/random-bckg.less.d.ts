/* eslint-disable */
declare namespace RandomBckgLessNamespace {
  export interface IRandomBckgLess {
    background: string;
    "background-darken": string;
    backgroundDarken: string;
  }
}

declare const RandomBckgLessModule: RandomBckgLessNamespace.IRandomBckgLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: RandomBckgLessNamespace.IRandomBckgLess;
};

export = RandomBckgLessModule;
