import React, { ChangeEvent } from 'react'

export interface IFormData	{ [key: string]: string | boolean }

export default class SavingStateForm<P = {}, S = {}, SS = {}>
	extends React.Component<P, S, SS> {

	protected _hdlInput = this.handleInputChange.bind(this)
	protected formData: IFormData = {}

	handleInputChange(e: ChangeEvent<HTMLInputElement>) {
		this.formData[ e.target.name ] = e.target.checked || e.target.value
	}
}
