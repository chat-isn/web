import React from 'react'
import {EmojiType, SheetData} from '../../util/emoji-data'

import './emoji.less'

interface IProps {
	emoji: EmojiType
}

const isFlag = (emoji: EmojiType) => emoji.short_name.includes('flag-') ||
	/[\uD83C][\uDDE6-\uDDFF][\uD83C][\uDDE6-\uDDFF]/.test(emoji.char)

export default class Emoji extends React.Component<IProps> {
	static toHtml(emoji: EmojiType) {
		const { char } = emoji
		const { sheetSizeX, sheetSizeY, multiplyX, multiplyY  } = SheetData
		const charPlaceholder = isFlag( emoji ) ? char : char.substring(0, 3)

		return `<span class='emoji-container emoji-font-set'>${char}<span class='emoji emoji-font-set'
			style='background-position: ${emoji.sheet_x * multiplyX }% ${emoji.sheet_y * multiplyY}%;
			       background-size: ${sheetSizeX}% ${sheetSizeY}%'>
			${ charPlaceholder }</span></span>`
	}

	render() {
		const { emoji } = this.props
		const { sheetSizeX, sheetSizeY, multiplyX, multiplyY  } = SheetData
		const {char} = emoji
		const charPlaceholder = isFlag(emoji) ? char : char.substring(0, 3)

		return <span className={'emoji-container emoji-font-set'}>{char}
			<span className={'emoji emoji-font-set'} style={{
				backgroundPosition: `${emoji.sheet_x * multiplyX}% ${emoji.sheet_y * multiplyY}%`,
				backgroundSize: `${sheetSizeX}% ${sheetSizeY}%`
			}}>{ charPlaceholder }</span>
		</span>
	}
}
