/* eslint-disable */
declare namespace EmojiPickerLessNamespace {
  export interface IEmojiPickerLess {
    card: string;
    "card-exit": string;
    cardExit: string;
    container: string;
    "emojis-container": string;
    emojisContainer: string;
    gradient: string;
  }
}

declare const EmojiPickerLessModule: EmojiPickerLessNamespace.IEmojiPickerLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: EmojiPickerLessNamespace.IEmojiPickerLess;
};

export = EmojiPickerLessModule;
