/* eslint-disable */
declare namespace EmojiLessNamespace {
  export interface IEmojiLess {
    file: string;
    mappings: string;
    names: string;
    sources: string;
    sourcesContent: string;
    version: string;
  }
}

declare const EmojiLessModule: EmojiLessNamespace.IEmojiLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: EmojiLessNamespace.IEmojiLess;
};

export = EmojiLessModule;
