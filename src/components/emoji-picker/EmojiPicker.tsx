import React from 'react'
import i18n from '../../i18n/i18n'
import BeautifulInput from '../beautiful-input/BeautifulInput'
import Button from '../button/Button'
import emojiData, { EmojisSorted, EmojiType } from '../../util/emoji-data'
import Emoji from './Emoji'
import * as style from './emoji-picker.less'

// use calculations ?
// since it's embedded with emoji-picker.less, nope ! ^^
const EMOJIS_PER_LINE = 6
const EMOJI_HEIGHT = 26
const LINES_PER_HEIGHT = 8


interface IProps {
	onClose: (emoji?: string) => any
	show: boolean
}

interface IState {
	search: string
	emojisToRender: EmojiType[]
	topMargin: number
}

// TODO: skin variations ?
export default class EmojiPicker extends React.Component<IProps, IState> {
	// Removing skin variations from emoji list
	private emojis = EmojisSorted.map(emoji => {
		let str = (emojiData.replaceEmojiToStr(emoji))

		if (str.includes('skin-tone'))
			return null

		str = str.substring(1, str.length - 1)

		const mdr = emojiData.searchEmoji(str, 1)
		if (mdr.length === 1)
			return mdr[0]
		return null
	}).filter(e => e !== null).filter((v, i, a) => a.indexOf(v) === i) as EmojiType[]


	constructor(props: IProps) {
		super(props)
		this.state = {
			search: '',
			topMargin: 0,
			emojisToRender: this.emojis.slice(0, LINES_PER_HEIGHT * EMOJIS_PER_LINE)
		}
	}

	search() {
		const { search } = this.state
		return this.emojis
			.filter(emoji => search === '' ? false : emoji.short_name.startsWith(search))
			.map((emoji, i) => emoji ? this.renderEmojiButton(emoji, i) : null)
	}

	render() {
		// used to calculate height
		let emojiLength = this.emojis.length
		// round up so we fill a whole line
		emojiLength += EMOJIS_PER_LINE - (emojiLength % EMOJIS_PER_LINE)

		const isNotSearching = this.state.search === '',
			results = this.search(),

			height = emojiLength * EMOJI_HEIGHT / EMOJIS_PER_LINE + 30

		return <div className={ style.container } style={ { display: this.props.show ? 'block' : 'none' } }>
			<div className={ style.cardExit } onClick={ () => this.props.onClose() } />

			<div className={ style.card }>
				<BeautifulInput placeholder={ i18n.get('app.chat.search-emojis') }
				                autoComplete={'off'} spellCheck='false'
								onInput={ e => this.handleInput(e) }/>
				<div className={ style.emojisContainer } onScroll={ e => this.scrollEvent(e) }>
					{ /* search */ }
					<div style={{ display: isNotSearching ? 'none' : 'block' }}>
						{ results.length > 0 ? results : <p>{ i18n.get('app.chat.search-emojis.no-results') }</p> }
					</div>

					{ /* normal list */ }
					<div style={{ display: isNotSearching ? 'block' : 'none',
						height: isNotSearching ? `${ height }px` : 'auto'
					}}>
						<div style={{ position: 'relative', top: `${this.state.topMargin}px` }}>
							{ this.state.emojisToRender.map((emoji, i) =>
								this.renderEmojiButton(emoji, i)) }
						</div>
					</div>
				</div>
			</div>
		</div>
	}

	private refreshScroll(div: HTMLDivElement) {
		const scroll = div.scrollTop
		const initialLine = Math.floor(scroll / EMOJI_HEIGHT)
		const firstIndex = initialLine * EMOJIS_PER_LINE

		this.setState({
			emojisToRender: this.emojis.slice(firstIndex,
				firstIndex + EMOJIS_PER_LINE * LINES_PER_HEIGHT),
			topMargin: initialLine * EMOJI_HEIGHT
		})

	}

	private scrollEvent(e: React.UIEvent<HTMLDivElement>) {
		this.refreshScroll(e.currentTarget)
	}

	private renderEmojiButton(emoji: EmojiType, key?: number) {
		return 	<Button key={key} style={{
			display: 'inline-block'
		}} onClick={ () => {
			this.props.onClose(emoji.char)
		} } >
			<Emoji emoji={emoji} />
		</Button>
	}

	private handleInput(e: React.FormEvent<HTMLInputElement>) {
		this.setState({ search: e.currentTarget.value })
	}
}
