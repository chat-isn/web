import React, { ReactNode } from 'react'
import classNames from 'classnames'
import omit from '../../util/omit'
import * as style from './beautiful-input.less'

export type InputError = false | string

interface IProps extends React.InputHTMLAttributes<HTMLInputElement> {
	placeholder: string
	error?: InputError
	checkError?(val: string): InputError
	/**
	 * OnEnter is deprecated, prefer <form> with submit event
	 * @deprecated
	 */
	onEnter?(): any
}

interface IState {
	focused: boolean
	error: InputError
	/**
	 * Correspond à si le placeholder doit rester en haut
	 * quand par ex. il y a une valeur
	 */
	stuckLabel: boolean
}

export default class BeautifulInput extends	React.Component<IProps, IState> {
	/**
	 * Compteur afin de générer des attributs `id` uniques
	 */
	protected static id = 0
	inputRef = React.createRef<HTMLInputElement>()
	id = BeautifulInput.id++

	constructor(props: IProps) {
		super(props)

		this.state = {
			error: this.props.error ? this.props.error : false,
			focused: false,
			stuckLabel: typeof this.props.value !== 'undefined' ||
				typeof this.props.defaultValue !== 'undefined'
		}
	}

	shouldComponentUpdate(nextProps: IProps, nextState: IState) {
		if (typeof nextProps.error !== 'undefined')
			nextState.error = nextProps.error

		if (!nextState.error && nextProps.checkError)
			if (this.inputRef.current && this.inputRef.current.value.length > 0)
				nextState.error = nextProps.checkError( this.inputRef.current.value )

		return true
	}

	render(): ReactNode {
		const inputId = this.props.id ? this.props.id : `input-id-${this.id}`

		return <div className={ classNames( style.input, this.props.className, {
			[style.focused]: this.state.focused,
			[style.value]: this.state.stuckLabel,
			[style.error]: typeof this.state.error === 'string' // si on a un message d'erreur
		} ) }>
				<label htmlFor={ inputId }>{ this.props.placeholder }</label>
				{ /* omit permet d'enlever de passer les propriétés que l'on utilise déjà à l'input */ }
				<input ref={this.inputRef} { ...omit(this.props, [ 'id', 'className',
												'placeholder', 'children', 'checkError', 'error',
					'onEnter' ]) } id={ inputId }
					onFocus={ this.focus.bind(this) } // lorsqu'on focus le champ
					onBlur={ this.blur.bind(this) }
					onKeyUp={ this.keyUp.bind(this) } />
				{ /* message d'erreur */ }
				<p>{ this.state.error }</p>
			</div>
	}

	/**
	 * Retourne le contenu du champ
	 */
	getValue() {
		if (this.inputRef.current)
			return this.inputRef.current.value
	}

	/**
	 * Gère lorsqu'on focus le champ
	 */
	protected focus(e: React.FocusEvent<HTMLInputElement>): void {
		this.setState({ focused: true })
		// forwarding props
		if (this.props.onFocus)
			this.props.onFocus(e)
	}

	/**
	 * Gère lorsqu'on quitte le focus du champ
	 */
	protected blur(e: React.FocusEvent<HTMLInputElement>): void {
		this.setState({ focused: false })
		// forwarding props
		if (this.props.onBlur)
			this.props.onBlur(e)
	}

	/**
	 * Gère lorsque quelque chose est entré
	 */
	protected keyUp(e: React.KeyboardEvent<HTMLInputElement>): void {
		// forwarding props
		if (this.props.onKeyUp)
			this.props.onKeyUp(e)

		if (this.props.onEnter && e.keyCode === 13)
			return this.props.onEnter()
		// S'il y a du contenu, on force le label à rester en haut
		const value = e.currentTarget.value

		let error: InputError = false

		if (this.props.error)
			error = this.props.error

		if (!error)
			if (this.props.checkError)
				error = this.props.checkError(value)

		this.setState({
			stuckLabel: value.length > 0,
			error
		})
	}
}
