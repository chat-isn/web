/* eslint-disable */
declare namespace BeautifulInputLessNamespace {
  export interface IBeautifulInputLess {
    error: string;
    focused: string;
    input: string;
    value: string;
  }
}

declare const BeautifulInputLessModule: BeautifulInputLessNamespace.IBeautifulInputLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: BeautifulInputLessNamespace.IBeautifulInputLess;
};

export = BeautifulInputLessModule;
