import classnames from 'classnames'
import React, { ReactNode } from 'react'
import omit from '../../util/omit'
import * as style from './button.less'

interface ILoginButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
	type?: 'normal' | 'icon'
	children: ReactNode
	className?: string
	onClick?(): any
}

export default class Button
	extends React.Component<ILoginButtonProps> {
	render(): ReactNode {
		return <button { ...omit(this.props, [ 'type', 'children' ]) }
		               className={ classnames(this.props.className, style.button, {
			               [style.iconButton]: this.props.type === 'icon'
		               }) }>
			{ this.props.children }
			</button>
	}
}
