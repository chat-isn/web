/* eslint-disable */
declare namespace ButtonLessNamespace {
  export interface IButtonLess {
    button: string;
    "icon-button": string;
    iconButton: string;
  }
}

declare const ButtonLessModule: ButtonLessNamespace.IButtonLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: ButtonLessNamespace.IButtonLess;
};

export = ButtonLessModule;
