import { EmojiData } from 'emoji-data-ts'
import React, { ReactNode } from 'react'
import classnames from 'classnames'
import omit from '../../util/omit'
import renderMarkdown from '../../util/render-md'
import sanitize from '../../util/sanitize'
import * as style from './markdown-input.less'

interface IProps extends React.TextareaHTMLAttributes<HTMLTextAreaElement> {
	placeholder: string
	/**
	 * OnEnter is deprecated, prefer <form> with submit event
	 * @deprecated
	 */
	onEnter?(): any
}

export default class MarkdownInput extends React.Component<IProps> {
	ref = React.createRef<HTMLTextAreaElement>()
	renderRef = React.createRef<HTMLDivElement>()
	private emojiData = new EmojiData()

	constructor(props: IProps) {
		super(props)
	}

	render(): ReactNode {
		let shouldHideText = false

		if (this.ref.current)
			shouldHideText = this.ref.current.value.length > 0

		return <div className={ classnames(this.props.className, style.container) }>
			<textarea ref={ this.ref } { ...omit(this.props, [ 'className' ]) }
			         onInput={ () => this.handleInput() } className={ classnames('emoji-font-set', {
			         	[style.hideText]: shouldHideText
			         }) } />
			<div ref={ this.renderRef } className={ classnames(style.render, 'emoji-font-set') }
			     dangerouslySetInnerHTML={ this.renderMarkdown() } />
		</div>
	}

	private handleInput() {
		this.forceUpdate()

		if (this.ref && this.ref.current) {
			const el = this.ref.current
			const outerHeight = parseInt(window.getComputedStyle(el).height, 10)
			const diff = outerHeight - el.clientHeight
			el.style.height = '0px'

			el.style.height = Math.max(32, el.scrollHeight + diff) + 'px'

			el.value = el.value.replace(/:([\w\d-_]+):/g, (s, g) => {
				const foundEmojis = this.emojiData.searchEmoji(g.replace(/_/gi, '-'), 2)
				if (foundEmojis.length > 0)
					return foundEmojis[0].char
				return `:${g}:`
			})
		}
	}

	private renderMarkdown() {
		let __html
		if (this.ref && this.ref.current)
			__html = renderMarkdown(sanitize( this.ref.current.value ))
		else
			__html = ''

		return { __html }
	}
}
