/* eslint-disable */
declare namespace MarkdownInputLessNamespace {
  export interface IMarkdownInputLess {
    container: string;
    "hide-text": string;
    hideText: string;
    render: string;
  }
}

declare const MarkdownInputLessModule: MarkdownInputLessNamespace.IMarkdownInputLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: MarkdownInputLessNamespace.IMarkdownInputLess;
};

export = MarkdownInputLessModule;
