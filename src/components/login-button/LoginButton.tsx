import classnames from 'classnames'
import React, { ReactNode } from 'react'
import * as style from './login-button.less'

interface ILoginButtonProps {
	className?: string
	children: ReactNode
	onClick?: () => any
}

export default class LoginButton
	extends React.Component<ILoginButtonProps> {
	render(): ReactNode {
		return <button className={ classnames(style.loginButton, this.props.className) }
		               onClick={ this.props.onClick }>{ this.props.children }</button>
	}
}
