/* eslint-disable */
declare namespace LoginButtonLessNamespace {
  export interface ILoginButtonLess {
    "login-button": string;
    loginButton: string;
  }
}

declare const LoginButtonLessModule: LoginButtonLessNamespace.ILoginButtonLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: LoginButtonLessNamespace.ILoginButtonLess;
};

export = LoginButtonLessModule;
