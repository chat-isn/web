import React from 'react'
import i18n from '../../../i18n/i18n'
import BeautifulInput, {InputError} from '../../beautiful-input/BeautifulInput'
import LoginButton from '../../login-button/LoginButton'
import {email as emailTest} from '../../../util/tests'
import {generateFieldCheck} from '../../../lib/FieldRules'
import buildSimpleMessageModal from '../../modal/simple-message-modal/buildSimpleMessageModal'
import {ERegisterResponse, IRegisterFields, RegisterRules} from '../../../lib/Authentication'
import appInstance from '../../../AppInstance'
import SavingStateForm from '../../form/SavingStateForm'
import LoginCard from './LoginCard'
import * as style from './login-card.less'


export default class RegisterPart extends SavingStateForm<{
	parent: LoginCard
}, {
	errors: {
		username: InputError
		mail: InputError
		password: InputError
		confirmPassword: InputError
	}
}> {
	constructor(props: { parent: LoginCard }) {
		super(props)
		this.state = { errors: { username: false, mail: false, password: false, confirmPassword: false }}
	}

	render() {
		const h = this._hdlInput

		return <div>
			<div className={ style.paddedOne }>
				<h1>{ i18n.get('home.register') }</h1>

				<form onSubmit={ (e): void => {
					e.preventDefault()
					this.register()
				}}>
					<BeautifulInput onChange={h} name='username' placeholder={ i18n.get('home.username') }
						autoComplete='username' error={ this.state.errors.username } spellCheck='false' required />
					<BeautifulInput onChange={h} name='mail' placeholder={ i18n.get('home.mail') } checkError={ val =>
						emailTest( val ) ? false : i18n.get('home.register.not-email')
					} autoComplete='email' type='email' error={ this.state.errors.mail } required />
					<BeautifulInput onChange={h} name='password' placeholder={ i18n.get('home.password') }
						autoComplete='new-password' type='password' error={ this.state.errors.password } required />
					<BeautifulInput onChange={h} name='confirmPassword'
						autoComplete='new-password' placeholder={ i18n.get('home.confirm-password') }
						type='password'
						error= { this.state.errors.confirmPassword }
						checkError={ val =>
							val === this.formData.password ? false : i18n.get('home.register.not-same-pw')
						} required />
					<input type='submit' hidden /> { /* makes the magic happen (enables Enter submitting) */ }
				</form>

			</div>

			<div className={ style.buttonContainer }>
				<LoginButton onClick={ (): void => this.props.parent.goToTabIndex(1) }>
					{ i18n.get('home.signin') }
				</LoginButton>
				<LoginButton onClick={ (): void => this.register() }>
					{ i18n.get('home.register') }
				</LoginButton>
			</div>
		</div>
	}

	private register() {
		if (this.props.parent.state.loading)
			return

		const generateFieldCheckBounded = (
			fieldKey: keyof IRegisterFields,
			tooLong: string,
			tooShort: string,
			empty: string
		) => generateFieldCheck<IRegisterFields>(
				this.formData as unknown as IRegisterFields,
				RegisterRules,
				fieldKey, tooLong, tooShort, empty)

		// fields checking
		const username = generateFieldCheckBounded(
			'username',
			i18n.get('home.register.username-too-long'),
			i18n.get('home.register.username-too-short'),
			i18n.get('home.register.field-should-not-empty') )


		const password = generateFieldCheckBounded(
			'password',
			i18n.get('home.register.pw-too-long'),
			i18n.get('home.register.pw-too-short'),
			i18n.get('home.register.field-should-not-empty') )

		const mail = generateFieldCheckBounded(
			'mail',
			i18n.get('home.register.mail-too-long'),
			i18n.get('home.register.mail-too-short'),
			i18n.get('home.register.field-should-not-empty') )

		const confirmPassword = this.formData.password === this.formData.confirmPassword ?
			false : i18n.get('home.register.not-same-pw')

		this.setState({ errors: { username, password, mail, confirmPassword }})

		// if any check has returned an error
		if (![username, password, mail, confirmPassword].every(el => el === false))
			return

		this.props.parent.setState({ loading: true })
		appInstance.auth.register(this.formData as any as IRegisterFields).then(result => {
			this.props.parent.setState({ loading: false })

			if (result === ERegisterResponse.EMAIL_USED)
				return this.setState({ errors: {...this.state.errors, mail: i18n.get('home.register.email-used') }})
			if (result === ERegisterResponse.USERNAME_USED)
				return this.setState({ errors:
					{...this.state.errors, username: i18n.get('home.register.username-used') }
				})
			if (result === ERegisterResponse.RULES_NOT_MET)
				// this never has to happen but if someone manipulates the code...
				// let's make the thing harder
				return
			if (result === ERegisterResponse.UNKNOWN)
				return appInstance.showModal(buildSimpleMessageModal('wtf?'))
			// the auth object will emit 'connected' which means redirection & logged in!
			if (result === ERegisterResponse.OK)
				return
		})
	}
}
