import React from 'react'
import i18n from '../../../i18n/i18n'
import BeautifulInput from '../../beautiful-input/BeautifulInput'
import LoginButton from '../../login-button/LoginButton'
import { email as emailTest } from '../../../util/tests'
import appInstance from '../../../AppInstance'
import LoadingSpinner from '../../loading-spinner/LoadingSpinner'
import SavingStateForm from '../../form/SavingStateForm'
import * as style from './login-card.less'
import LoginCard from './LoginCard'


export default class RecoverPwPart extends SavingStateForm<{
	parent: LoginCard
}, {
	isRecovering: boolean
	result: boolean | null
}> {
	constructor(props: { parent: LoginCard }) {
		super(props)
		this.state = { isRecovering: false, result: null }
	}

	render() {
		return <div>
			<div className={ style.paddedOne }>
				<h1>{ i18n.get('home.recover-pw') }</h1>
				<form onSubmit={ (e): void => {
					e.preventDefault()
					this.recover()
				} }>
					<BeautifulInput name='mail'
						onChange={ this._hdlInput } placeholder={ i18n.get('home.mail') }
						checkError={ val =>	emailTest( val ) ? false : i18n.get('home.recover.not-email') }
					type='email' />
					<input type='submit' hidden /> { /* makes the magic happen (enables Enter submitting) */ }
				</form>
			</div>

			<div className={ style.buttonContainer }>
				<LoginButton color={'#898c94'} background={'#e7e9eb'}
					onClick={ (): void => this.props.parent.goToTabIndex(1) }>
					{ i18n.get('home.recover-pw.back') }
				</LoginButton>
				<LoginButton color={'#fff'} background={'#2d3c54'}
					onClick={ (): void => this.recover() }>
					{ i18n.get('home.recover-pw.do') }
				</LoginButton>
			</div>

			{ this.state.isRecovering ? <LoadingSpinner style={
				{ width: '40px', height: '40px'}
			} /> : null }

			<p className={ style.text } style={ { height: '40px' }}>
				{ this.state.result === true ? i18n.get('home.recover-pw.okay') :
					this.state.result === false ? i18n.get('home.recover-pw.error') : null }
			</p>

		</div>
	}

	private recover() {
		if (this.state.isRecovering)
			return

		const mail = this.formData.mail as string

		if (!emailTest( mail ))
			return

		this.setState({ isRecovering: true })

		if (this.formData.mail)
			appInstance.auth.reset( mail ).then(result => {
				this.setState({ isRecovering: false, result })
			})
	}
}
