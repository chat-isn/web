/* eslint-disable */
declare namespace LoginCardLessNamespace {
  export interface ILoginCardLess {
    buttonContainer: string;
    gradient: string;
    hidingLogin: string;
    "loading-container": string;
    "loading-container-show": string;
    "loading-login-card": string;
    loadingContainer: string;
    loadingContainerShow: string;
    loadingLoginCard: string;
    "login-card": string;
    "login-card-container": string;
    "login-containers": string;
    loginCard: string;
    loginCardContainer: string;
    loginContainers: string;
    "padded-one": string;
    paddedOne: string;
    text: string;
  }
}

declare const LoginCardLessModule: LoginCardLessNamespace.ILoginCardLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: LoginCardLessNamespace.ILoginCardLess;
};

export = LoginCardLessModule;
