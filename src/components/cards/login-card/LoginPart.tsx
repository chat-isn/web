import React from 'react'
import i18n from '../../../i18n/i18n'
import BeautifulInput, { InputError } from '../../beautiful-input/BeautifulInput'
import LoginButton from '../../login-button/LoginButton'
import Button from '../../button/Button'
import SavingStateForm from '../../form/SavingStateForm'
import { generateFieldCheck } from '../../../lib/FieldRules'
import { AuthRules, IAuthFields } from '../../../lib/Authentication'
import appInstance from '../../../AppInstance'
import LoginCard from './LoginCard'
import * as style from './login-card.less'

export default class LoginPart extends SavingStateForm<{
	parent: LoginCard
}, {
	errors: {
		mail: InputError
		password: InputError
	}
}> {
	constructor(props: { parent: LoginCard }) {
		super(props)
		this.state = { errors: { mail: false, password: false }}
	}

	render() {
		return <div>
			<div className={ style.paddedOne }>
				<h1>{ i18n.get('home.login') }</h1>

				<form onSubmit={ (e): void => {
					e.preventDefault()
					this.auth()
				}}>
					<BeautifulInput error={ this.state.errors.mail } name='mail' onChange={ this._hdlInput }
						autoComplete='email' placeholder={ i18n.get('home.mail') } spellCheck='false' />
					<BeautifulInput error={ this.state.errors.password } name='password' onChange={ this._hdlInput }
						type='password' placeholder={ i18n.get('home.password') } autoComplete='current-password' />
					<input type='submit' hidden /> { /* makes the magic happen (enables Enter submitting) */ }
				</form>

				<Button onClick={ () => this.props.parent.goToTabIndex(2) }>
					{ i18n.get('home.password-forgotten') }
				</Button>

			</div>

			<div className={ style.buttonContainer }>
				<LoginButton
					onClick={ (): void => this.props.parent.goToTabIndex(0) }>
					{ i18n.get('home.register') }
				</LoginButton>
				<LoginButton
					onClick={ (): void => this.auth() }>
					{ i18n.get('home.signin') }
				</LoginButton>
			</div>
		</div>
	}

	private auth() {
		if (this.props.parent.state.loading)
			return

		const generateFieldCheckBounded = (
			fieldKey: keyof IAuthFields,
			tooLong: string,
			tooShort: string,
			empty: string
		) => generateFieldCheck<IAuthFields>(
				this.formData as unknown as IAuthFields,
				AuthRules,
				fieldKey, tooLong, tooShort, empty)


		const mail = generateFieldCheckBounded(
			'mail',
			i18n.get('home.register.mail-too-long'),
			i18n.get('home.register.mail-too-short'),
			i18n.get('home.register.field-should-not-empty') )


		const password = generateFieldCheckBounded(
			'password',
			i18n.get('home.register.pw-too-long'),
			i18n.get('home.register.pw-too-short'),
			i18n.get('home.register.field-should-not-empty') )

		this.setState({ errors: { mail, password }})
		// if any check has returned an error
		if (![mail, password].every(el => el === false))
			return

		this.props.parent.setState({ loading: true })

		const {auth} = appInstance

		auth.auth({
			mail: this.formData.mail as string,
			password: this.formData.password as string
		}).then(response => {
			if (response) // connected!!
				return
			// else
			// we can do those setStates because theses containers ARE NOT
			// unmounted after starting to connect!!
			this.props.parent.setState({ loading: false })
			this.setState({
				errors: {
					mail: '',
					password: ''
				}
			})
		})
	}
}
