import classnames from 'classnames'
import React, { ReactNode } from 'react'
import LoadingSpinner from '../../loading-spinner/LoadingSpinner'
import * as style from './login-card.less'
import RegisterPart from './RegisterPart'
import LoginPart from './LoginPart'
import RecoverPwPart from './RecoverPwPart'

// Login card based on https://static.collectui.com/shots/2292415/daily-ui-001-day-001-sign-up-large

export interface ILoginCardState {
	pageId: number
	loading: boolean
}

/**
 * Représente la "carte" de connexion
 */
export default class LoginCard extends React.Component<{}, ILoginCardState> {
	private loginContainer = React.createRef<HTMLDivElement>()
	private tabs: HTMLDivElement[] = []

	private TAB_WIDTH = 400

	private handleResize_ = this.handleResize.bind(this)

	constructor(props: {}) {
		super(props)
		this.state = { pageId: 2, loading: false }
	}

	handleResize() {
		this.goToTabIndex( this.state.pageId )
	}

	componentDidMount(): void {
		this.goToTabIndex(1) // connection tab

		// this is used to refresh TAB_WIDTH
		window.addEventListener('resize', this.handleResize_ )
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.handleResize_)
		delete this.handleResize_
	}

	goToTabIndex(i: number): void {
		if (!this.loginContainer.current)
			return

		const loginContainer = this.loginContainer.current

		// if (this.tabs.length === 0)
		// Weird behavior of React, fixed like that... (even tho unoptimized)
		this.tabs = Array.from(loginContainer.children).filter(child => child.matches('div')) as HTMLDivElement[]

		const computedDim = this.tabs[i].getBoundingClientRect(),
			{ height, width } = computedDim

		this.TAB_WIDTH = width;

		(loginContainer.parentNode as HTMLDivElement).style.height = `${height + 50}px`
		this.setState({ pageId: i })
	}

	render(): ReactNode {
		return <div className={ style.loginCardContainer }>
			<div className={ classnames(style.loginCard, {
				[style.loadingLoginCard]: this.state.loading
			}) }>
				<div className={ classnames(style.loginContainers, {
					[style.hidingLogin]: this.state.loading
				}) }
					ref={ this.loginContainer }
					style={{left: `${-this.state.pageId * this.TAB_WIDTH}px`}}>

					<RegisterPart parent={ this } />
					<LoginPart parent={ this } />
					<RecoverPwPart parent={ this } />

				</div>
				<div className={ classnames(style.loadingContainer, {
					[style.loadingContainerShow]: this.state.loading
				}) }>
					<LoadingSpinner />
				</div>

			</div>
		</div>
	}
}
