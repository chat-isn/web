/* eslint-disable */
declare namespace GameSelectorLessNamespace {
  export interface IGameSelectorLess {
    card: string;
    "card-exit": string;
    cardExit: string;
    container: string;
    gradient: string;
  }
}

declare const GameSelectorLessModule: GameSelectorLessNamespace.IGameSelectorLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: GameSelectorLessNamespace.IGameSelectorLess;
};

export = GameSelectorLessModule;
