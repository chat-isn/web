import React from 'react'
import appInstance from '../../AppInstance'

import spotifyIntegration from '../../lib/SpotifyIntegration'
import i18n from '../../i18n/i18n'
import GAME_ICON from '../../img/icons/games-black-36dp.svg'
import SPOTIFY_ICON from '../../img/icons/spotify-logo-without-text-36dp.svg'
import RichMessage, {RichMessageTypes} from '../../lib/RichMessage'
import * as style from './game-selector.less'

const GAMES: { friendlyName: string; gameName: string }[] = [{
	friendlyName: 'Paint',
	gameName: 'paint'
}, {
	friendlyName: 'Piano',
	gameName: 'piano'
}]

export { GAMES }

interface IProps {
	chatId: number
	show: boolean
	onClose(): any
}

export default class GameSelector extends React.Component<IProps> {
	constructor(props: IProps) {
		super(props)
	}

	componentDidMount() {
		spotifyIntegration.on('player-data-update', this.handlePlayerDataUpdate)
	}

	componentWillUnmount() {
		spotifyIntegration.removeListener('player-data-update', this.handlePlayerDataUpdate)
	}

	render() {
		const { playerData } = spotifyIntegration

		return <div className={ style.container } style={ { display: this.props.show ? 'block' : 'none' } }>
			<div className={ style.cardExit } onClick={ () => this.props.onClose() } />

			<div className={ style.card }>
				<div>
					{ GAMES.map((game, i) =>
						<div key={i} onClick={ () => {
							appInstance.openGameChat( this.props.chatId, game.gameName )
							this.props.onClose()
						} }>
							<GAME_ICON />
							<p>{ i18n.format('app.chat.game', {
								name: game.friendlyName
							}) }</p>
						</div>) }
					{ /* TODO: Spotify listening */ }
					{ !playerData ? null : (
						playerData.item ?
							<div onClick={ () => {
								appInstance.chatAPI.sendRichMessageToChat(new RichMessage(null,
									RichMessageTypes.SPOTIFY_LISTEN, {
										contentType: 'song',
										uri: playerData.item ? playerData.item.uri : '',
										name: playerData.item ? playerData.item.name : '',
										cover: playerData.item ? playerData.item.album.images[0].url : '',
										albumName: playerData.item ? playerData.item.album.name : '',
										albumUri: playerData.item ? playerData.item.album.uri : '',
										href: playerData.item ? playerData.item.external_urls.spotify : '',
										artists: playerData.item ? playerData.item.artists.map(d => d.name).join(', ')
											: ''
									}), this.props.chatId).then()
								this.props.onClose()
							} }>
								<SPOTIFY_ICON />
								<p>{ i18n.format('app.chat.spotify.share-current-track', {
									song: playerData.item.name,
									author: playerData.item.artists.map(artist => artist.name).join(', ')
								}) }</p>
							</div> : null
					)}
				</div>
			</div>
		</div>
	}

	private handlePlayerDataUpdate = () => this.forceUpdate()
}
