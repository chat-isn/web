/* eslint-disable */
declare namespace BadgeLessNamespace {
  export interface IBadgeLess {
    badge: string;
  }
}

declare const BadgeLessModule: BadgeLessNamespace.IBadgeLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: BadgeLessNamespace.IBadgeLess;
};

export = BadgeLessModule;
