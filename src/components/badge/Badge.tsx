import React, {ReactNode} from 'react'
import classnames from 'classnames'

import * as style from './badge.less'

interface IProps<T> {
	children: ReactNode
	className?: string
	onMount?(self: Badge<T>): T
	onUnmount?(self: Badge<T>, arg: T | null): any
	count(): number
}

export default class Badge<T> extends React.Component<IProps<T>> {
	private saved: T | null = null

	constructor(props: IProps<T>) {
		super(props)
	}

	componentDidMount() {
		if (this.props.onMount)
			this.saved = this.props.onMount(this)
	}

	componentWillUnmount() {
		if (this.props.onUnmount)
			this.saved = this.props.onUnmount(this, this.saved)
	}

	render() {
		const c = this.props.count()
		return <>
			{ c !== 0 ?
				<div className={ classnames(this.props.className, 'badge', style.badge) }>{ c >= 10 ? '9+' : c }</div> :
				null }
			{ this.props.children }
		</>
	}
}
