/* eslint-disable */
declare namespace NavigationLessNamespace {
  export interface INavigationLess {
    active: string;
    gradient: string;
    info: string;
    navigation: string;
    "navigation-container": string;
    "navigation-item": string;
    navigationContainer: string;
    navigationItem: string;
  }
}

declare const NavigationLessModule: NavigationLessNamespace.INavigationLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: NavigationLessNamespace.INavigationLess;
};

export = NavigationLessModule;
