import React, {ReactNode} from 'react'

import CONTACTS_ICON from '../../img/icons/supervisor_account-black-36dp.svg'
import CHAT_ICON from '../../img/icons/chat_bubble_outline-black-36dp.svg'
import SETTINGS_ICON from '../../img/icons/settings-black-36dp.svg'
import EDIT_ICON from '../../img/icons/create-black-36dp.svg'
import INFO_ICON from '../../img/icons/info-black-36dp.svg'

import ToolTip from '../tooltip/ToolTip'
import i18n from '../../i18n/i18n'
import UnreadCount from '../../lib/UnreadCount'
import Badge from '../badge/Badge'
import NavigationItem from './NavigationItem'
import NavGroup from './NavGroup'
import * as style from './navigation.less'


export default class ChatNavigation extends React.Component {
	private static buildItem(path: string, icon: ReactNode) {
		return <ToolTip text={ i18n.get(`navigation.${path}`) } wrapIt={true}>
			<NavigationItem to={path}>{ icon }</NavigationItem>
		</ToolTip>
	}

	render(): ReactNode {
		return <div className={ style.navigationContainer }>
			<NavGroup>
				{ ChatNavigation.buildItem('contacts', <CONTACTS_ICON />) }
				{ ChatNavigation.buildItem('chat', <Badge
					onMount={ self => {
						const listener = () => {
							self.forceUpdate()
						}

						UnreadCount.on('count-added', listener)
						return listener
					} }

					onUnmount={ (self, listener) => {
						if (listener)
							UnreadCount.removeListener('count-added', listener as any)
					} }
					count={ () => Object.values(UnreadCount.unreadCounts).reduce((a, b) => a + b, 0) }>
					<CHAT_ICON />
				</Badge>) }
				{ ChatNavigation.buildItem('settings', <SETTINGS_ICON />) }
				{ ChatNavigation.buildItem('edit', <EDIT_ICON />) }

				<NavigationItem to='info' className={ style.info }>{ <INFO_ICON /> }</NavigationItem>
			</NavGroup>
		</div>
	}
}
