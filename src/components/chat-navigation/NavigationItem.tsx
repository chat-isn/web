import React, { ReactNode } from 'react'
import { NavLink } from 'react-router-dom'
import classnames from 'classnames'
import * as style from './navigation.less'

interface INavigationItemProps {
	icon?: string
	to: string
	className?: string
	children: ReactNode
}

export default class NavigationItem extends React.Component<INavigationItemProps> {
	render(): ReactNode {
		return <li className={ classnames( style.navigationItem, this.props.className ) }>
			<NavLink to={ `/app/${this.props.to}` } activeClassName={ style.active }>
				{ this.props.children }
			</NavLink>
		</li>
	}
}
