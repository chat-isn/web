import React, { Component, ReactElement, ReactNode } from 'react'
import * as style from './navigation.less'
import NavigationItem from './NavigationItem'

interface INavGroupProps {
	children: ReactElement<NavigationItem>[]
}

export default class NavGroup extends Component<INavGroupProps> {
	render(): ReactNode {
		return <ul className={ style.navigation }>
			{ this.props.children }
		</ul>
	}
}
