import React from 'react'
import {downloadLog} from '../../../util/log'
import i18n from '../../../i18n/i18n'
import Button from '../../button/Button'

export default class DownloadLog extends React.Component<{
	className: string
}> {
	static friendlyName = i18n.get('app.logs.export')
	static id = 'logs'

	render() {
		return <div className={ this.props.className }>
			<Button style={{ margin: 'auto' }}
			        onClick={() => downloadLog()}>{ i18n.get('app.logs.export') }</Button>
		</div>
	}
}
