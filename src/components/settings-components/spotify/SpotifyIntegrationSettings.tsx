import React from 'react'
import i18n from '../../../i18n/i18n'
import spotifyIntegration from '../../../lib/SpotifyIntegration'
import Button from '../../button/Button'
import {IExtraSettingProps} from '../../../routes/settings-page/ExtraSettings'
import {ICancellablePromise} from '../../../util/make-cancelable'


export default class SpotifyIntegrationSettings extends React.Component<IExtraSettingProps, {
	loadingUserData: boolean
}> {
	static id = 'spotify'

	private req: ICancellablePromise<any> | null = null
	private userData: any | null = null

	constructor(props: IExtraSettingProps) {
		super(props)
		this.state = { loadingUserData: true }

		spotifyIntegration.getUserData().then(userData => {
			this.userData = userData
			this.setState({
				loadingUserData: false
			})
		})
	}

	static friendlyName = () => i18n.get('app.settings.spotify-integration')

	render() {
		return <div className={ this.props.className }>
			<h3>{ i18n.get('app.settings.spotify-integration') }</h3>
			{ spotifyIntegration.hasSpotifyAccess() ? this.renderSpotify() : (
				spotifyIntegration.hasRefreshToken() ? this.renderLoading() : this.renderNotConnected()
			) }
		</div>
	}

	componentWillUnmount() {
		if (this.req)
			this.req.cancel()
	}

	private renderSpotify() {
		if (this.state.loadingUserData || !this.userData)
			return this.renderLoading()

		return <>
			<p>{ i18n.format('app.settings.spotify-integration.connected', {
				name: this.userData.display_name
			}) }</p>
			{ this.userData.product === 'premium' ? null : <p>
				{ i18n.get('app.settings.spotify-integration.not-premium-warn') }
			</p>}
			<Button onClick={ () => {
				spotifyIntegration.disconnect()
				this.userData = null
			} }>
				{ i18n.get('app.settings.spotify-integration.log-off') }
			</Button>
		</>
	}

	private renderLoading() {
		return <p style={{ textAlign: 'center' }}>{ i18n.get('app.settings.spotify-integration.loading') }</p>
	}

	private renderNotConnected() {
		return <>
			<p>{ i18n.get('app.settings.spotify-integration.not-logged') }</p>
			<p>{ i18n.get('app.settings.spotify-integration.not-logged.details') }</p>
			<Button onClick={ () => spotifyIntegration.redirectToLogin() }>
				{ i18n.get('app.settings.spotify-integration.login') }
			</Button>
		</>
	}
}
