import classnames from 'classnames'
import React, { ReactNode } from 'react'
import {IFailedMessage, IMessage, ISendingMessage} from '../../lib/ChatAPI'
import renderMarkdown from '../../util/render-md'
import RelativeDate from '../../lib/RelativeDate'
import sanitize from '../../util/sanitize'
import * as style from './chat-message.less'


interface IChatMessageProps {
	message: ISendingMessage | IFailedMessage | IMessage
	compact: boolean
}

export default class ChatMessage extends React.Component<IChatMessageProps> {

	render(): ReactNode {
		const { message } = this.props

		return <div className={ classnames(style.message, {
			[style.fail]: message.sending === 'fail',
			[style.sending]: message.sending === true,
			[style.compact]: this.props.compact
		}) }>
			{this.props.compact ? null : <p className={style.user}>{message.sender.name}<span className={style.time}>
					{new RelativeDate(message.time).expressRelativeTime()}
				</span></p>
			}
			<p className={ style.content }
			   dangerouslySetInnerHTML={{ __html: renderMarkdown(sanitize( (message.content as string)
					   .replace(/(&lbrace;)/gi, '}')
					   .replace(/(&apos;)/gi, '\'')), false) }} />
		</div>
	}

}
