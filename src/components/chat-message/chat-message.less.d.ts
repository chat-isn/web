/* eslint-disable */
declare namespace ChatMessageLessNamespace {
  export interface IChatMessageLess {
    compact: string;
    content: string;
    fail: string;
    message: string;
    sending: string;
    time: string;
    user: string;
  }
}

declare const ChatMessageLessModule: ChatMessageLessNamespace.IChatMessageLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: ChatMessageLessNamespace.IChatMessageLess;
};

export = ChatMessageLessModule;
