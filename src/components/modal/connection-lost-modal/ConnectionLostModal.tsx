import React from 'react'
import Modal, {IModalProps} from '../Modal'
import Button from '../../button/Button'
import i18n from '../../../i18n/i18n'
import appInstance from '../../../AppInstance'

export default function ConnectionLostModal(props: IModalProps) {
	const hideModal = () => {
		appInstance.chatAPI.clearCache()
		appInstance.connection.reset()
		appInstance.connection.connect()
		appInstance.showModal(null)
	}

	return <Modal {...props} onModalClose={ hideModal }>
		<h3>{ i18n.get('connection-lost') }</h3>
		<p>{ i18n.get('connection-lost.details') }</p>
		<Button onClick={ hideModal }>
			{ i18n.get('connection-lost.restart') }
		</Button>
	</Modal>
}
