import React from 'react'
import Modal, {IModalProps} from '../Modal'
import Button from '../../button/Button'

interface IModalOption {
	id: string
	text: string
}

export default function buildButtonsModal(message: string, options: IModalOption[],
                                          callback: (self: Modal | null, id: string) => any, title?: string) {
	return function ButtonsModal(props: IModalProps) {
		const modalRef = React.createRef<Modal>()

		return <Modal {...props} ref={modalRef} disableModalExit={true} >
			{ title ? <h3>{ title }</h3> : null }

			<p>{ message }</p>

			{ options.map((option, i) =>
				<Button key={i} style={{
					float: 'right', display: 'inline-block'
				}} onClick={ () => callback(modalRef.current, option.id) }>{ option.text }</Button>) }
		</Modal>
	}
}
