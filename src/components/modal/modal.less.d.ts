/* eslint-disable */
declare namespace ModalLessNamespace {
  export interface IModalLess {
    modal: string;
    "modal-container": string;
    "modal-enter": string;
    "modal-enter-active": string;
    modalContainer: string;
    modalEnter: string;
    modalEnterActive: string;
  }
}

declare const ModalLessModule: ModalLessNamespace.IModalLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: ModalLessNamespace.IModalLess;
};

export = ModalLessModule;
