import { CSSTransition } from 'react-transition-group'
import React, { ReactNode } from 'react'
import * as style from './modal.less'

export interface IModalProps {
	children?: ReactNode
	onModalClose: (self: Modal) => any
	disableModalExit?: boolean
}

export default class Modal extends React.Component<IModalProps, { mounted: boolean }> {
	componentDidMount() {
		this.setState({ mounted: true })
	}

	render(): ReactNode {
		return <CSSTransition in={this.state ? this.state.mounted : false} timeout={200} classNames={{
			enter: style.modalEnter,
			enterActive: style.modalEnterActive
		}}>
			<div
				className={ style.modalContainer }
				onClick={ e => {
					if (e.currentTarget === e.target && !this.props.disableModalExit)
						this.hideModal()
				} }>
				<div className={ style.modal }>
					{ this.renderModalContent() }
				</div>
			</div>
		</CSSTransition>
	}

	renderModalContent(): ReactNode {
		if (this.props.children)
			return this.props.children

		return <p>Empty Modal</p>
	}

	hideModal() {
		this.props.onModalClose(this)
	}
}
