import React from 'react'
import Modal, {IModalProps} from '../Modal'
import Button from '../../button/Button'
import i18n from '../../../i18n/i18n'


export default function buildSimpleMessageModal(message: string, title?: string) {
	return function SimpleMessageModal(props: IModalProps) {
		const modalRef = React.createRef<Modal>()

		return <Modal {...props} ref={modalRef}>
			{ title ? <h3>{ title }</h3> : null }
			<p>{ message }</p>
			<Button onClick={() => modalRef.current ? modalRef.current.hideModal() : null }>
				{ i18n.get('generic.modal.ok') }
			</Button>
		</Modal>
	}
}
