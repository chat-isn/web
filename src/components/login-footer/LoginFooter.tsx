import React, { ReactNode } from 'react'
import appInstance from '../../AppInstance'
import i18n from '../../i18n/i18n'
import LanguageModal from '../language-modal/LanguageModal'
import * as style from './login-footer.less'


/**
 * Le footer du login
 */
export default class LoginFooter extends React.Component {
	render(): ReactNode {
		return <footer className={ style.footer }>
			<ul>
				{ /* shadow */ }
				<div />
				<li>{ i18n.get('project.name') }</li>
				<li>{ DEV ? i18n.get('project.dev-build') :
					<a target="_blank" rel="noopener noreferrer"
						href={`https://gitlab.com/chat-isn/web/-/commit/${COMMITHASH}`}>
						{ i18n.format('project.branch', {
							version: VERSION,
							branch: BRANCH
						}) }
					</a> }
				</li>
				<li><a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/chat-isn">Code source</a></li>
				<li>
					<a href="Hello you!"
						onClick={ (e): void => {
							e.preventDefault()
							appInstance.showModal(LanguageModal)
						} }>Change language</a>
				</li>
			</ul>
		</footer>
	}
}
