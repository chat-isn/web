/* eslint-disable */
declare namespace LoginFooterLessNamespace {
  export interface ILoginFooterLess {
    footer: string;
  }
}

declare const LoginFooterLessModule: LoginFooterLessNamespace.ILoginFooterLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: LoginFooterLessNamespace.ILoginFooterLess;
};

export = LoginFooterLessModule;
