/* eslint-disable */
declare namespace LoadingSpinnerLessNamespace {
  export interface ILoadingSpinnerLess {
    color: string;
    dash: string;
    "loading-spinner": string;
    loadingSpinner: string;
    rotate: string;
  }
}

declare const LoadingSpinnerLessModule: LoadingSpinnerLessNamespace.ILoadingSpinnerLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: LoadingSpinnerLessNamespace.ILoadingSpinnerLess;
};

export = LoadingSpinnerLessModule;
