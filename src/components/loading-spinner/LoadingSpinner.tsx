import classnames from 'classnames'
import React from 'react'
import * as style from './loading-spinner.less'


export default class LoadingSpinner
	extends React.Component< React.HTMLAttributes<HTMLDivElement> > {

	render() {
		return <div {...this.props}
		            className={ classnames(style.loadingSpinner, this.props.className) }>
			<svg viewBox="25 25 50 50">
				<circle cx="50" cy="50" r="20" fill="none" strokeWidth="2" strokeMiterlimit="10" />
			</svg>
		</div>
	}
}
