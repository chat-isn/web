/* eslint-disable */
declare namespace ChatInputLessNamespace {
  export interface IChatInputLess {
    "disable-on-mobile": string;
    disableOnMobile: string;
    gradient: string;
    input: string;
    "input-container": string;
    inputContainer: string;
  }
}

declare const ChatInputLessModule: ChatInputLessNamespace.IChatInputLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: ChatInputLessNamespace.IChatInputLess;
};

export = ChatInputLessModule;
