import React from 'react'
import {RouteComponentProps, withRouter} from 'react-router'

// import ATTACHMENT_ICON from '../../img/icons/attach_file-black-36dp.svg'
import INSERT_EMOJI from '../../img/icons/insert_emoticon-black-36dp.svg'
import PLAY_ICON from '../../img/icons/sports_esports-black-36dp.svg'

import i18n from '../../i18n/i18n'
import Button from '../button/Button'
import MarkdownInput from '../markdown-input/MarkdownInput'
import EmojiPicker from '../emoji-picker/EmojiPicker'
import appInstance from '../../AppInstance'
import insertAtCaretPosition from '../../util/insert-at-caret-pos'
import GameSelector from '../game-selector/GameSelector'
import * as style from './chat-input.less'


type IProps = RouteComponentProps<{
	chatId: string
}>

interface IState {
	isEmojiPanelOpened: boolean
	isGamePanelOpened: boolean
}

export class ChatInput extends React.Component<IProps, IState> {
	private inputRef = React.createRef<MarkdownInput>()

	constructor(props: IProps) {
		super(props)
		this.state = { isEmojiPanelOpened: false, isGamePanelOpened: false }
	}

	render() {
		return <div className={ style.inputContainer }>
			{ /* <Button type={'icon'} className={ style.disableOnMobile } onClick={() => {
				console.log('add attach')
			}}><ATTACHMENT_ICON /></Button> */ }

			<form onSubmit={ e => {
				e.preventDefault()
				this.send()
			}}>
				<MarkdownInput className={'input'} placeholder={ i18n.get('app.chat.type-a-msg') }
				               ref={ this.inputRef } onKeyDown={ e => {
				    if (e.key === 'Enter')
				    	if (e.ctrlKey)
				    		insertAtCaretPosition( e.currentTarget, '\n' )
						else if (!e.shiftKey) {
							e.preventDefault()
						    this.send()
					    }
				} } />
				<input type='submit' hidden />
			</form>

			<EmojiPicker show={this.state.isEmojiPanelOpened} onClose={ emoji => {
				this.setState({isEmojiPanelOpened: false})
				if (emoji && this.inputRef.current) {
					const textareaRef = this.inputRef.current.ref
					if (textareaRef.current)
						textareaRef.current.value += emoji
				}
			} } />
			<Button type={'icon'} className={ style.disableOnMobile } onClick={() => {
				this.setState({ isEmojiPanelOpened: !this.state.isEmojiPanelOpened })
			}}><INSERT_EMOJI /></Button>

			<GameSelector chatId={ parseInt(this.props.match.params.chatId, 10) }
				show={ this.state.isGamePanelOpened } onClose={ () => this.setState({ isGamePanelOpened: false}) }/>
			<Button type={'icon'} className={ style.disableOnMobile } onClick={() => {
				// here, we are sure that chatId is a number because else this component
				// wouldn't be rendered: see ChatPage.tsx
				this.setState({ isGamePanelOpened: true })
				// appInstance.openGameChat( parseInt(this.props.match.params.chatId, 10) )
			}}><PLAY_ICON /></Button>
		</div>
	}

	private send() {
		const { current } = this.inputRef
		if (current) {
			const { current: current1 } = current.ref
			if (current1) {
				let { value } = current1
				value = value.trim().replace(/}/gi, '&lbrace;')
					.replace(/'/gi, '&apos;')
				if (value.length > 0) {
					appInstance.chatAPI.sendMessageToChat(value, parseInt(this.props.match.params.chatId, 10)).then()
					current1.value = ''
					current.forceUpdate()
				}
			}
		}
	}
}

export default withRouter(ChatInput)
