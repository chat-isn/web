import React, { ReactNode } from 'react'
import i18n from '../../i18n/i18n'
import Modal from '../modal/Modal'


const availableLanguages = i18n.getLanguages()

export default class LanguageModal extends Modal {
	renderModalContent(): ReactNode {
		return <>
			<p>Change language</p>
			<ul>
				{ Object.keys(availableLanguages).map((langId, i) =>
					<li key={i} value={langId}
						onClick={(): void => {
							i18n.setLanguage(langId)
							this.hideModal()
						}}>
						{availableLanguages[langId]}
					</li>
				) }
			</ul>
		</>
	}
}

/*
export default class LanguageMenu extends React.Component<{ show: boolean }> {
	render(): ReactNode {
		return <Modal show={ this.props.show }>

		</Modal>
	}
}
*/
