import React, { ReactNode } from 'react'
import { RouteComponentProps, withRouter } from 'react-router'

import makeCancelable, { ICancellablePromise } from '../../util/make-cancelable'
import {IFailedMessage, IMessage, ISendingMessage} from '../../lib/ChatAPI'
import log from '../../util/log'
import appInstance from '../../AppInstance'
import LoadingSpinner from '../loading-spinner/LoadingSpinner'
import ChatMessage from '../chat-message/ChatMessage'
import applyNewSelfMessage from '../../util/apply-new-self-message'
import i18n from '../../i18n/i18n'
import ChatInput from '../chat-input/ChatInput'
import RichMessage, {RichMessageTypes} from '../../lib/RichMessage'
import * as style from './chat-messages.less'


const FETCH_STEP = 50 // _SISTER

interface IChatListState {
	loading: boolean
	isEnd: boolean
	content: 'NO_CHAT' | 'DOESNT_EXIST' | IMessage[]
}

export class ChatMessages extends React.Component<RouteComponentProps<{
	chatId: string
}>, IChatListState> {
	private request: ICancellablePromise<IMessage[] | false> | null = null
	private scrollableRef = React.createRef<HTMLDivElement>()
	private handleUserScroll = false

	constructor(props: RouteComponentProps<{ chatId: string }>) {
		super(props)
		this.state = { content : [], loading: false, isEnd: false }
	}

	componentDidMount(): void {
		this.makeInitialRequest()
		this.handleUserScroll = false
		appInstance.chatAPI.on('new-self-message', this.selfMessagesListener)
		appInstance.chatAPI.on('error-self-message', this.errorSelfMessageListener)
		appInstance.chatAPI.on(`message-${this.chatId}`, this.messagesListener)
	}

	componentDidUpdate(prevProps: Readonly<RouteComponentProps<{ chatId: string }>>): void {
		if (prevProps.match.params.chatId !== this.chatId) {
			this.componentWillUnmount(prevProps.match.params.chatId)
			this.componentDidMount()
		}
	}

	fetchMessages() {
		if (this.state.loading || this.state.isEnd)
			return

		let fetchedMessages = 0
		const chatId = parseInt(this.chatId, 10)

		if (Array.isArray(this.state.content))
			fetchedMessages = this.state.content.filter(m => m.sending !== 'fail').length

		if (fetchedMessages > 0 && !this.handleUserScroll)
			return

		this.setState({ loading: true })

		this.request = makeCancelable( appInstance.chatAPI.getDMMessages(
			chatId,
			fetchedMessages,
			FETCH_STEP
		) )

		let oldScrollValue = 0, div: HTMLDivElement | null = null
		if (this.scrollableRef.current) {
			div = this.scrollableRef.current
			oldScrollValue = div.scrollHeight - div.scrollTop
		}

		this.request.then(content => {
			if (!content)
				return this.setState({ content: 'DOESNT_EXIST' })

			let isEnd = false
			if (content.length !== FETCH_STEP)
				isEnd = true

			const oldContent = this.state.content
			const initialArray = Array.isArray( oldContent ) ? oldContent : []

			this.setState({ content: initialArray.concat(content), loading: false, isEnd }, () => {
				if (fetchedMessages === 0)
					this.scrollToBottom()
				else if (div)
					div.scrollTo({ top: div.scrollHeight - oldScrollValue })
			})
		})
	}

	makeInitialRequest() {
		if (this.request)
			this.request.cancel()

		if (this.chatId)
			try {
				this.setState({ content: [], loading: false, isEnd: false }, () => this.fetchMessages())
				return
			} catch (e) {
				console.log(e)
				log('Error', 'chatId not number...')
			}
		else
			this.setState({ content: 'NO_CHAT' })
	}

	componentWillUnmount(chatId: string = this.chatId): void {
		if (this.request)
			this.request.cancel()
		appInstance.chatAPI.removeListener('new-self-message', this.selfMessagesListener)
		appInstance.chatAPI.removeListener('error-self-message', this.errorSelfMessageListener)
		appInstance.chatAPI.removeListener(`message-${chatId}`, this.messagesListener)
	}

	render(): ReactNode {
		return <>
			<div className={ style.chatMessagesContainer } ref={ this.scrollableRef }
			     onScroll={ e => this.scrollEvent(e) }
			     onClick={ e => e.button === 1 ? this.handleUserScroll = false : null }
			     onWheel={ () => this.handleUserScroll = true } onKeyDown={ e => {
			     	if ([33, 34, 32, 38, 40].includes(e.which) || (e.ctrlKey && [36, 35].includes(e.which)))
			     		this.handleUserScroll = true
				} }>
					{ this.state.loading && (!this.state.isEnd && Array.isArray(this.state.content)) ?
						<LoadingSpinner className={ style.chatLoading }/> : null }
					{ this.state.content === 'NO_CHAT' ?
						this.noChatRender() : this.chatRender() }
			</div>
			{ Array.isArray(this.state.content) ? <ChatInput /> : null}
		</>
	}

	noChatRender(): ReactNode {
		return <p>???</p>
	}

	chatRender(): ReactNode {
		if (this.state.content === 'NO_CHAT')
			return <p>lol</p>

		if (this.state.content === 'DOESNT_EXIST')
			return <p style={{ textAlign: 'center' }}>{ i18n.get('app.chat.dm-doesnt-exists') }</p>

		if (this.state.content.length === 0 && !this.state.loading)
			return <div style={{ textAlign: 'right' }}>{ i18n.get('app.chat.empty-dm') }</div>

		const reversedMessages = [...this.state.content].reverse()

		return reversedMessages.map((message: IMessage, i: number, arr) => {
			let shouldBeCompact = false

			if (i > 0) {
				const nextMessage = arr[i - 1]
				shouldBeCompact = nextMessage.sender.id === message.sender.id
				if (nextMessage.content instanceof RichMessage)
					shouldBeCompact = false
			}

			if (message.content instanceof RichMessage) {
				if (i > 0) {
					const nextMessage = arr[i - 1]
					if (nextMessage && nextMessage.content instanceof RichMessage) {
						if (nextMessage.content.toJSON() === message.content.toJSON() &&
							shouldBeCompact) // skipping duplicates
							return
						const { content } = message
						if (nextMessage.content.type === content.type &&
							content.type === RichMessageTypes.GAME_NOTIFY &&
							content.data.gameName === nextMessage.content.data.gameName)
							return
					}
				}
				const Comp = message.content.getComponent()
				return <Comp key={i} rm={ message.content } compact={ shouldBeCompact } />
			}

			return <ChatMessage key={i} message={message} compact={ shouldBeCompact } />
		})
	}

	get chatId() {
		return this.props.match.params.chatId
	}

	private scrollEvent(e: React.UIEvent<HTMLDivElement>) {
		if (!this.handleUserScroll)
			return

		const div = e.currentTarget
		if (div.scrollTop < 100)
			this.fetchMessages()
	}

	private selfMessagesListener = (chatid: number, selfMessage: ISendingMessage) => {
		if (chatid !== parseInt(this.chatId, 10))
			return

		if (Array.isArray(this.state.content))
			this.setState({ content: [ selfMessage, ...this.state.content ] },
				() => this.scrollToBottom())
	}

	private errorSelfMessageListener = (chatid: number, selfMessage: IFailedMessage) =>
		chatid === parseInt(this.chatId, 10) ? this.messagesListener(selfMessage) : null

	private messagesListener = (newMessage: IMessage) => {
		if (newMessage.sender.id === appInstance.chatAPI.authedUser.id) {
			if (Array.isArray(this.state.content))
				this.setState({ content: applyNewSelfMessage(newMessage, this.state.content) })
			return
		}

		if (Array.isArray(this.state.content)) {
			this.setState({content: [newMessage, ...this.state.content]})
			const div = this.scrollableRef.current
			if (div && div.scrollHeight - div.scrollTop - div.clientHeight < 200)
				this.scrollToBottom()
		}
	}

	private scrollToBottom() {
		if (this.scrollableRef.current)
			this.scrollableRef.current.scrollTop = this.scrollableRef.current.scrollHeight
	}

}

export default withRouter(ChatMessages)
