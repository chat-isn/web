/* eslint-disable */
declare namespace ChatMessagesLessNamespace {
  export interface IChatMessagesLess {
    "chat-loading": string;
    "chat-messages-container": string;
    chatLoading: string;
    chatMessagesContainer: string;
  }
}

declare const ChatMessagesLessModule: ChatMessagesLessNamespace.IChatMessagesLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: ChatMessagesLessNamespace.IChatMessagesLess;
};

export = ChatMessagesLessModule;
