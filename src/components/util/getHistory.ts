import React from 'react'
import {RouteComponentProps, withRouter} from 'react-router'
import { History } from 'history'

let globalHistory: History | null = null

class HistoryExportComponent extends React.Component<RouteComponentProps> {
	constructor(props: RouteComponentProps) {
		super(props)
		globalHistory = this.props.history
	}

	componentDidUpdate() {
		globalHistory = this.props.history
	}

	render() { return null; }
}

export const HistoryExport = withRouter(HistoryExportComponent)

export default function getHistory() { return globalHistory }
