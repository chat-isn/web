/* eslint-disable */
declare namespace UserInputListLessNamespace {
  export interface IUserInputListLess {
    container: string;
    input: string;
    loading: string;
    users: string;
  }
}

declare const UserInputListLessModule: UserInputListLessNamespace.IUserInputListLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: UserInputListLessNamespace.IUserInputListLess;
};

export = UserInputListLessModule;
