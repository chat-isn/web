import React from 'react'
import {IUser} from '../../lib/ChatAPI'
import BeautifulInput, {InputError} from '../beautiful-input/BeautifulInput'
import i18n from '../../i18n/i18n'
import LoadingSpinner from '../loading-spinner/LoadingSpinner'
import ToolTip from '../tooltip/ToolTip'
import Button from '../button/Button'
import appInstance from '../../AppInstance'

import CLOSE_ICON from '../../img/icons/close-black-36dp.svg'

import * as style from './user-input-list.less'

interface IProps {
	userListUpdate(users: IUser[]): any
}

interface IState {
	loading: boolean
	users: IUser[]
	inputError: InputError
}

export default class UserInputList extends React.Component<IProps, IState> {
	private ref = React.createRef<BeautifulInput>()


	constructor(props: IProps) {
		super(props)
		this.state = { loading: false, users: [], inputError: false }
	}

	render() {
		return <div className={style.container}>
			<div className={style.input}>
				<BeautifulInput ref={ this.ref } placeholder={ i18n.get('app.chat.dm.id') }
			                autoComplete='off' autoCorrect='off' type='number'
			                error={ this.state.inputError } onEnter={() => this.add() } />
				<Button onClick={ () => this.add() }>
					{ i18n.get('generic.add') }
				</Button>
			</div>
			<div className={ style.users }>
				{ this.state.users.map((user, i) => <div key={i}>
					<ToolTip wrapIt={true} text={ i18n.get('generic.close') }>
						<CLOSE_ICON onClick={() => {
							const users = this.state.users.filter(u => u.id !== user.id)
							this.setState({ users })
							this.props.userListUpdate(users)
						}}/>
					</ToolTip>
					<p>{ user.name }</p>

				</div>)}
			</div>
			{ this.state.loading ? <LoadingSpinner className={style.loading} /> : null }
		</div>
	}

	private async addUser(userId: number) {
		this.setState({ loading: true })

		if (userId === appInstance.chatAPI.authedUser.id)
			return this.setState({ loading: false, inputError: i18n.get('app.chat.dm.yourself') })

		if (this.state.users.find(el => el.id === userId))
			return this.setState({ loading: false, inputError: i18n.get('app.chat.dm.duplicate') })

		const { chatAPI } = appInstance
		const queryResult = Object.values(await chatAPI.getUsers([userId]))

		if (queryResult.length === 0)
			return this.setState({ loading: false, inputError: i18n.get('app.chat.dm.user-not-found') })

		this.setState({
			loading: false,
			inputError: false,
			users: [...this.state.users, queryResult[0] ] })

		this.props.userListUpdate(this.state.users)
	}

	private add() {
		if (this.ref.current && this.ref.current.inputRef.current) {
	        const input = this.ref.current.inputRef.current,
			    userId = parseInt(input.value, 10)
		    if (!isNaN(userId)) {
			    this.addUser(userId).then()
			    input.value = ''
		    } else if (input.value !== '')
		        this.setState({ inputError: i18n.get('app.chat.dm.not-number') })
	    }
	}
}
