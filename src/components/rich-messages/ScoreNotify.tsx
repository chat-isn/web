import React from 'react'
import RichMessage from '../../lib/RichMessage'
import i18n from '../../i18n/i18n'
import log from '../../util/log'
import {GAMES} from '../game-selector/GameSelector'
import appInstance from '../../AppInstance'

import GAME_ICON from '../../img/icons/games-black-36dp.svg'
import * as style from './game-notify.less'

export default class ScoreNotify extends React.Component<{
	rm: RichMessage
	compact: boolean
}> {
	render() {
		const {gameName, score} = this.props.rm.data
		const {message} = this.props.rm

		if (!gameName || !message || !score) {
			log('RichMessage Component', 'Error: 0xACAB2')
			return <></>
		}

		const games = GAMES.filter(g => g.gameName === gameName)

		if (games.length !== 1) {
			log('RichMessage Component', 'Error: 0xACAB3')
			return <></>
		}

		const {sender} = message

		return <div className={style.gameNotify}>
			<GAME_ICON />
			<span>{i18n.format('app.chat.game.did-score', {
				name: sender.name, game: games[0].friendlyName, score
			})}</span>
			&nbsp;
			<span className={style.join} onClick={() => {
				appInstance.openGameChat(this.props.rm.data.chatId, gameName)
			}}>{i18n.get('app.chat.game.did-score.join')}</span>
		</div>
	}
}
