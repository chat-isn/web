/* eslint-disable */
declare namespace GameNotifyLessNamespace {
  export interface IGameNotifyLess {
    "game-notify": string;
    gameNotify: string;
    gradient: string;
    join: string;
  }
}

declare const GameNotifyLessModule: GameNotifyLessNamespace.IGameNotifyLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: GameNotifyLessNamespace.IGameNotifyLess;
};

export = GameNotifyLessModule;
