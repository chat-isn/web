/* eslint-disable */
declare namespace SpotifyListenLessNamespace {
  export interface ISpotifyListenLess {
    gradient: string;
    icon: string;
    join: string;
    "spotify-listen": string;
    spotifyListen: string;
  }
}

declare const SpotifyListenLessModule: SpotifyListenLessNamespace.ISpotifyListenLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: SpotifyListenLessNamespace.ISpotifyListenLess;
};

export = SpotifyListenLessModule;
