import React from 'react'
import RichMessage from '../../lib/RichMessage'
import i18n from '../../i18n/i18n'
import log from '../../util/log'
import spotifyIntegration from '../../lib/SpotifyIntegration'

import SPOTIFY_ICON from '../../img/icons/spotify-logo-without-text-36dp.svg'
import ToolTip from '../tooltip/ToolTip'
import * as style from './spotify-listen.less'

export default class SpotifyListen extends React.Component<{
	rm: RichMessage
	compact: boolean
}> {
	render() {
		if (!this.props.rm.message) {
			log('RichMessage Component', 'Error: 0xACAB2')
			return <></>
		}

		const { data } = this.props.rm,
			{ sender } = this.props.rm.message

		return <div className={ style.spotifyListen }>
			<div className={ style.icon }><SPOTIFY_ICON /></div>
			<p>{ i18n.format('app.chat.spotify.listen', {
				name: sender.name,
				song: data.name,
				author: data.artists,
				album: data.albumName
			}) }&nbsp;
			<span className={ style.join } onClick={ () => {
				spotifyIntegration.play( data.uri, data.albumUri ).then(ok => {
					if (!ok)
						window.open( data.href )
				})

			}}>{ i18n.get('app.chat.spotify.listen.button') }</span></p>
			<ToolTip text={ data.albumName } wrapIt={true}>
				<img src={ data.cover } alt={ data.name } onClick={ () => {
					spotifyIntegration.play( null, data.albumUri ).then() } } />
			</ToolTip>
		</div>
	}
}
