import React from 'react'
import {sha256} from 'js-sha256'

import ACCOUNT_ICON from '../../img/icons/account_circle-black-36dp.svg'
import INFO_ICON from '../../img/icons/info-black-36dp.svg'

import {email as emailTest} from '../../util/tests'
import i18n from '../../i18n/i18n'
import BeautifulInput, {InputError} from '../../components/beautiful-input/BeautifulInput'
import Button from '../../components/button/Button'
import appInstance from '../../AppInstance'
import SavingStateForm from '../../components/form/SavingStateForm'
import {generateFieldCheck} from '../../lib/FieldRules'
import {RegisterRules} from '../../lib/Authentication'
import ToolTip from '../../components/tooltip/ToolTip'
import buildButtonsModal from '../../components/modal/buttons-modal/ButtonsModal'
import LoadingSpinner from '../../components/loading-spinner/LoadingSpinner'
import * as style from './edit-page.less'

interface IState {
	loadingPassword: boolean
	loadingEmail: boolean
	emailError: InputError
	passwordError: InputError
	newPasswordError: InputError
	newPasswordConfirmError: InputError
}

export default class EditPage extends SavingStateForm<{}, IState> {
	constructor(props: {}) {
		super(props)
		this.state = {
			loadingPassword: false,
			loadingEmail: false,
			emailError: false,
			passwordError: false,
			newPasswordError: false,
			newPasswordConfirmError: false
		}
	}

	render(): React.ReactNode {
		const user = appInstance.chatAPI.authedUser

		return <div className={ style.container }>
			<div>
				<h2>{ i18n.get('app.edit-account') }</h2>

				<div className={ style.userMainDetails }>
					<div className={ style.icon }><ACCOUNT_ICON /></div>
					<div className={ style.username }>
						{ user.name } <span>({ user.id })</span>
						<ToolTip showMobile={true} text={ i18n.get('app.edit-account.username-not-changeable') }>
							<div><INFO_ICON /></div>
						</ToolTip>
					</div>
				</div>

				<h3>{ i18n.get('app.edit-account.email') }</h3>
				<form onSubmit={(e): void => {
						e.preventDefault()
						this.updateMail().then()
					}}>
					<BeautifulInput placeholder={i18n.get('app.edit-account.email.current')} type={'email'}
					                defaultValue={user.email} onChange={this._hdlInput}
					                checkError={val => emailTest(val) ? false : i18n.get('home.register.not-email')}
					                name={'email'} error={this.state.emailError} required spellCheck='false'/>
					<Button>
						{i18n.get('app.edit-account.password-change.confirm')}
					</Button>
					<input type='submit' hidden/>
					{ this.state.loadingEmail ? <LoadingSpinner className={ style.loading } /> : null }
				</form>

				<h3>{ i18n.get('app.edit-account.password-change') }</h3>
				<form onSubmit={(e): void => {
					e.preventDefault()
					this.updatePassword().then()
				}}>
					<input type='hidden' name='username' value={user.name} autoComplete={'username'} />
					<input type='hidden' name='email' value={user.email} autoComplete={'email'} />
					<BeautifulInput placeholder={i18n.get('app.edit-account.password-enter')}
					                name={'current-password'} onChange={this._hdlInput} type='password'
					                autoComplete='current-password'
					                error={this.state.passwordError} required spellCheck='false'/>
					<div className={style.newPassword}>
						<BeautifulInput placeholder={i18n.get('app.edit-account.new-password')}
						                name={'new-password'}
						                autoComplete='new-password'
						                onChange={this._hdlInput}
						                spellCheck='false'
						                type='password' error={this.state.newPasswordError} required/>
						<BeautifulInput placeholder={i18n.get('app.edit-account.new-password-confirm')}
						                name={'confirm-new-password'} onChange={this._hdlInput} type='password'
						                autoComplete='new-password'
						                error={this.state.newPasswordConfirmError} required
						                spellCheck='false'
						                checkError={val => val === this.formData['new-password'] ? false :
							                i18n.get('home.register.not-same-pw')}/>
					</div>

					<Button>
						{i18n.get('app.edit-account.password-change.confirm')}
					</Button>
					<input type='submit' hidden/>
					{ this.state.loadingPassword ? <LoadingSpinner className={ style.loading } /> : null }
				</form>

				<Button onClick={ () => this.disconnect() } style={ { float: 'right' } }>
					{ i18n.get('app.edit-account.disconnect') }
				</Button>
			</div>
		</div>
	}

	private async updateMail() {
		if (this.state.loadingEmail)
			return

		this.setState({ loadingEmail: true })
		const newMail = this.formData.email

		if (typeof newMail === 'string') {
			const ruleCheck = generateFieldCheck({ mail: newMail, username: '', password: '' }, RegisterRules, 'mail',
				i18n.get('home.register.mail-too-long'),
				i18n.get('home.register.mail-too-short'),
				i18n.get('home.register.field-should-not-empty'))

			if (ruleCheck)
				return this.setState({ loadingEmail: false, emailError: ruleCheck })

			const result = await appInstance.chatAPI.changeMail(newMail)


			if (!result)
				return this.setState({ loadingEmail: false, emailError: '' })
			else {
				const autoConnectData = JSON.parse(appInstance.auth.autoConnectPref.value)
				if (Array.isArray(autoConnectData) && autoConnectData.length === 2)
					appInstance.auth.autoConnectPref.value = JSON.stringify([newMail, autoConnectData[1]])

				return this.setState({loadingEmail: false})
			}
		}
		this.setState({ loadingEmail: false })
	}

	private async updatePassword() {
		this.setState({ loadingPassword: true })
		const currentPassword = this.formData['current-password'],
			newPassword = this.formData['new-password'],
			confirmNewPassword = this.formData['confirm-new-password']

		if (newPassword !== confirmNewPassword)
			return this.setState({ newPasswordConfirmError: i18n.get('home.register.not-same-pw') })

		const ruleCheck = generateFieldCheck({ mail: '', username: '', password: newPassword }, RegisterRules,
			'password', i18n.get('home.register.pw-too-long'),
			i18n.get('home.register.pw-too-short'),
			i18n.get('home.register.field-should-not-empty'))

		if (ruleCheck)
			return this.setState({ newPasswordError: ruleCheck })

		const result = await appInstance.chatAPI.changePassword(currentPassword as string, newPassword as string)
		if (result === 456)
			return this.setState({
				loadingPassword: false,
				passwordError: i18n.get('app.edit-account.wrong-old-password') })
		if (!result)
			return this.setState({ loadingPassword: false, passwordError: '' })

		appInstance.auth.autoConnectPref.value =
			JSON.stringify([appInstance.chatAPI.authedUser.email, sha256(newPassword as string) ])
		this.setState({ loadingPassword: false })
	}

	private disconnect() {
		appInstance.showModal( buildButtonsModal(i18n.get('app.edit-account.disconnect.confirm'),
			[
				{id: 'yes', text: i18n.get('generic.modal.yes') },
				{id: 'no', text: i18n.get('generic.modal.no')}
			],
			(modal, id) => {
				if (!modal)
					return

				switch (id) { // made the switch statement gang
					case 'yes':
						appInstance.auth.disconnect()
						appInstance.chatAPI.clearCache()
						appInstance.showModal(null)
						appInstance.connection.close()
						// reconnection state with <App />
						appInstance.reconnectStateWithComponent()
						appInstance.connection.connect()
						// appInstance.forceUpdate()
						break
					case 'no':
						modal.hideModal()
						break
				}
			}) )

	}
}
