/* eslint-disable */
declare namespace EditPageLessNamespace {
  export interface IEditPageLess {
    container: string;
    gradient: string;
    icon: string;
    loading: string;
    "new-password": string;
    newPassword: string;
    "user-main-details": string;
    userMainDetails: string;
    username: string;
  }
}

declare const EditPageLessModule: EditPageLessNamespace.IEditPageLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: EditPageLessNamespace.IEditPageLess;
};

export = EditPageLessModule;
