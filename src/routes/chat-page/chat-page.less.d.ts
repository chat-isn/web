/* eslint-disable */
declare namespace ChatPageLessNamespace {
  export interface IChatPageLess {
    "chat-content": string;
    chatContent: string;
    gradient: string;
    "left-panel": string;
    leftPanel: string;
    "no-chat": string;
    noChat: string;
  }
}

declare const ChatPageLessModule: ChatPageLessNamespace.IChatPageLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: ChatPageLessNamespace.IChatPageLess;
};

export = ChatPageLessModule;
