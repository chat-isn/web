import React, { ReactNode } from 'react'
import {RouteComponentProps, withRouter} from 'react-router'
import ChatMessages from '../../components/chat-messages/ChatMessages'
import ChatList from '../../components/chat-list/ChatList'
import i18n from '../../i18n/i18n'
import Button from '../../components/button/Button'
import * as style from './chat-page.less'

interface IState {
	canRenderSideContent: boolean
}

export class ChatPage extends React.Component<RouteComponentProps<{ chatId: string }>, IState> {

	constructor(props: RouteComponentProps<{ chatId: string }>) {
		super(props)
		this.state = {canRenderSideContent: false}
	}


	render(): ReactNode {
		return <div className={ style.chatContent }>
			<ChatList onDMFetched={() => this.setState({ canRenderSideContent: true }) } />
			<div className={ style.leftPanel }>
				{ this.state.canRenderSideContent ? this.renderContent() : null }
			</div>

		</div>
	}

	private renderContent() {
		const { chatId } = this.props.match.params
		const isChatIdProvided = !isNaN(parseInt(chatId, 10))

		if (isChatIdProvided)
			return <ChatMessages />
		return <div className={ style.noChat }>
			<h3>{ i18n.get('app.chat.no-chat') }</h3>
			<p>{ i18n.get('app.chat.select-dm') }</p>
			<p>{ i18n.get('app.chat.other-choices') }</p>
			<div>
				<Button onClick={() => {
					this.props.history.push('/app/contacts')
				}}>{ i18n.get('app.chat.create-dm') }</Button>
			</div>
		</div>
	}
}

export default withRouter(ChatPage)
