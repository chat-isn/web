import React from 'react'
import SpotifyIntegrationSettings from '../../components/settings-components/spotify/SpotifyIntegrationSettings'
import DownloadLog from '../../components/settings-components/download-log/DownloadLog'
import {ISearchResult} from '../../components/top-info-and-search-bar/SearchResults'
import normalize from '../../util/normalize'

export interface ISettingsComponent {
	friendlyName: string | (() => string)
	id: string
}

export interface IExtraSettingProps {
	className: string
}

const EXTRA_SETTINGS: (ISettingsComponent & React.ComponentClass<IExtraSettingProps>)[] = [
	SpotifyIntegrationSettings,
	DownloadLog]

export function searchInExtraSettings(q: string): ISearchResult[] {
	q = normalize(q.toLowerCase())
	return EXTRA_SETTINGS.filter( comp => {
		const id = comp.id.toLowerCase()

		if (id.includes(q))
			return true

		let friendlyName = comp.friendlyName
		if (typeof friendlyName === 'function')
			friendlyName = friendlyName()

		if (normalize(friendlyName.toLowerCase()).includes(q))
			return true
	}).map(comp => ({
		type: 'settings',
		friendlyName: typeof comp.friendlyName === 'string' ? comp.friendlyName : (
					typeof comp.friendlyName === 'function' ? comp.friendlyName() : 'null'),
		path: `app/settings/${comp.id}`
	}))
}

export { EXTRA_SETTINGS }
