/* eslint-disable */
declare namespace SettingsPageLessNamespace {
  export interface ISettingsPageLess {
    "fading-out-highlighting": string;
    fadingOutHighlighting: string;
    highlight: string;
    preferences: string;
    settings: string;
    "settings-container": string;
    settingsContainer: string;
  }
}

declare const SettingsPageLessModule: SettingsPageLessNamespace.ISettingsPageLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: SettingsPageLessNamespace.ISettingsPageLess;
};

export = SettingsPageLessModule;
