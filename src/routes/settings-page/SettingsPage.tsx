import React, { ReactNode } from 'react'
import {RouteComponentProps, withRouter} from 'react-router'
import classnames from 'classnames'

import i18n from '../../i18n/i18n'
import userPreferences, {UserPreferences} from '../../lib/UserPreferences'
import * as style from './settings-page.less'
import {EXTRA_SETTINGS} from './ExtraSettings'

export class SettingsPage extends React.Component<
	RouteComponentProps<{ settingId: string }>> {
	private timeout: NodeJS.Timeout | null = null

	componentDidMount() {
		this.timeout = setInterval(() => {
			this.forceUpdate()
		}, 500)
	}

	componentWillUnmount() {
		if (this.timeout)
			clearTimeout(this.timeout)
	}

	render(): ReactNode {
		return <div className={style.settingsContainer}>
			<div className={style.settings}>
				<h2>{ i18n.get('app.settings') }</h2>

				<div className={ style.preferences }>
					{ Object.entries(userPreferences.preferences)
						.filter(item => UserPreferences.isPreferencePublic(item[1]))
						.map((item, i) => {
							if (!item) return

							const [key, pref] = item

							if (!pref || !pref.choices || !pref.friendlyName)
								return

							const friendlyName = typeof pref.friendlyName === 'function' ?
								pref.friendlyName() : pref.friendlyName

							const choices = pref.choices()

							return <div key={i} className={ classnames({
								[ style.highlight ]: pref.id === this.props.match.params.settingId
							}) }>
								<p>{ friendlyName }</p>
								{ this.renderChoices(key, choices, userPreferences.get(pref), val => pref.value = val) }
							</div>
						})
					}
				</div>

				<h2>{ i18n.get('app.additional-settings') }</h2>
				{ EXTRA_SETTINGS.map((Element, i) =>
					<Element key={i} className={ classnames({
							[ style.highlight ]: Element.id === this.props.match.params.settingId
						}) } /> ) }
			</div>
		</div>
	}

	private renderChoices(key: string, choices: { [value: string]: string },
	                      defaultValue: string, callback: (val: string) => void) {
		return <select name={key} value={defaultValue} onChange={event => callback(event.target.value) }>
			{ Object.entries(choices).map((a, i) => {
				const [value, friendlyName] = a

				return <option key={i} value={value}>{ friendlyName }</option>
			}) }
		</select>
	}
}

export default withRouter(SettingsPage)
