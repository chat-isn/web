/* eslint-disable */
declare namespace AppPageLessNamespace {
  export interface IAppPageLess {
    "app-card": string;
    "app-card-container": string;
    "app-enter": string;
    "app-enter-active": string;
    "app-exit": string;
    "app-exit-active": string;
    appCard: string;
    appCardContainer: string;
    appEnter: string;
    appEnterActive: string;
    appExit: string;
    appExitActive: string;
    disableBorderOnMobile: string;
    "flex-fill": string;
    "flex-horizontal": string;
    "flex-vertical": string;
    flexFill: string;
    flexHorizontal: string;
    flexVertical: string;
    gradient: string;
  }
}

declare const AppPageLessModule: AppPageLessNamespace.IAppPageLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: AppPageLessNamespace.IAppPageLess;
};

export = AppPageLessModule;
