import classnames from 'classnames'
import React from 'react'
import { CSSTransition } from 'react-transition-group'
import {RouteComponentProps, Switch, Route, withRouter, Redirect} from 'react-router'
import ChatPage from '../chat-page/ChatPage'
import ChatNavigation from '../../components/chat-navigation/ChatNavigation'
import ChatSearch from '../../components/top-info-and-search-bar/TopInfoAndSearchBar'
import SettingsPage from '../settings-page/SettingsPage'
import ContactsPage from '../contacts-page/ContactsPage'
import EditPage from '../edit-page/EditPage'
import InfoPage from '../info-page/InfoPage'
import WelcomePage from '../welcome-page/WelcomePage'
import UserPreferences from '../../lib/UserPreferences'
import * as style from './app-page.less'


/**
 * Page de l'application!
 * Représente la "carte" de connexion
 */
class AppPage extends React.Component<RouteComponentProps, {
	isMounted: boolean
}> {
	private firstTime = UserPreferences.registerPreference({
		id: 'first-time',
		default: true,
		localStorage: true
	})

	private appearingTimeout: NodeJS.Timeout | null = null

	constructor(props: RouteComponentProps) {
		super(props)
		this.state = { isMounted: false }
	}

	componentDidMount() {
		this.appearingTimeout =
			setTimeout(() => this.setState({ isMounted: true }), 500)
	}

	componentWillUnmount() {
		if (this.appearingTimeout)
			clearTimeout(this.appearingTimeout)
	}

	render(): React.ReactNode {
		const { match } = this.props

		return <div>
			<CSSTransition in={this.state.isMounted} timeout={300}
				mountOnEnter
				unmountOnExit
				classNames={{
					enter: style.appEnter, enterActive: style.appEnterActive,
					exit: style.appExit, exitActive: style.appExitActive
				}} >
				<div className={ style.appCardContainer }>
					<div className={ classnames(style.appCard, style.flexHorizontal) }>
						<ChatNavigation />
						<div className={ classnames(style.flexVertical, style.disableBorderOnMobile) }
						     style={ { flex: 1 } }>
							<ChatSearch />
							<Switch>
								{ this.firstTime.value && this.firstTime.value.toString() === 'true' ?
									<WelcomePage pref={this.firstTime} parent={ this } /> : <>

										<Route path={`${match.path}/contacts`}
											component={ ContactsPage }/>

										<Route path={`${match.path}/chat/:chatId?`}
											component={ ChatPage }/>

										<Route path={`${match.path}/settings/:settingId?`}
											component={ SettingsPage } />

										<Route path={`${match.path}/edit`} component={ EditPage } />

										<Route path={`${match.path}/info`} component={ InfoPage } />

										<Redirect to={ `${match.path}/chat` } />
									</>
								}

							</Switch>
							{ /* content ? */ }
						</div>

					</div>
				</div>
			</CSSTransition>
		</div>
	}
}

export default withRouter(AppPage)
