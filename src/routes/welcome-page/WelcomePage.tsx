import React from 'react'
import {IUserPreference} from '../../lib/UserPreferences'
import i18n from '../../i18n/i18n'
import Button from '../../components/button/Button'
import appInstance from '../../AppInstance'

import * as style from './style.less'

export default class WelcomePage extends React.Component<{
	pref: IUserPreference
	parent: React.Component
}> {
	render() {
		return <div>
			<div className={ style.hello }>
				<h2>{ i18n.get('app.welcome.title') }</h2>
				<p>{ i18n.get('app.welcome.description') }</p>
				<p>{ i18n.format('app.welcome.description1', {
					id: appInstance.chatAPI.authedUser.id.toString(10)
				}) }</p>
				<p>{ i18n.get('app.welcome.description2') }</p>
				<p>{ i18n.get('app.welcome.description3') }</p>
				<Button onClick={() => {
					this.props.pref.value = 'false'
					this.props.parent.forceUpdate()
				}}>
					{ i18n.get('app.welcome.ok') }</Button>
			</div>
		</div>
	}
}
