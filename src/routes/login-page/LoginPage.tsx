import React from 'react'
import LoginCard from '../../components/cards/login-card/LoginCard'
import LoginFooter from '../../components/login-footer/LoginFooter'

/**
 * Vue/Page de connexion
 */
export default class LoginPage extends React.Component {
	render(): React.ReactNode {
		return <div>
			<LoginCard />
			<LoginFooter />
		</div>
	}
}
