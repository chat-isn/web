/* eslint-disable */
declare namespace InfoPageLessNamespace {
  export interface IInfoPageLess {
    "info-page": string;
    infoPage: string;
    presentation: string;
  }
}

declare const InfoPageLessModule: InfoPageLessNamespace.IInfoPageLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: InfoPageLessNamespace.IInfoPageLess;
};

export = InfoPageLessModule;
