import React from 'react'

import CHAT_ICON from '../../img/icons/chat_bubble_outline-black-36dp.svg'
import i18n from '../../i18n/i18n'
import * as style from './info-page.less'

export default class InfoPage extends React.Component {

	render(): React.ReactNode {
		return <div className={ style.infoPage }>
			<div className={ style.presentation }>
				<CHAT_ICON />

				<h2><a target="_blank" rel="noopener noreferrer" href='https://gitlab.com/chat-isn'>chat-isn</a></h2>
				<h4>{ DEV ? i18n.get('project.dev-build') :
					<a target="_blank" rel="noopener noreferrer"
						href={`https://gitlab.com/chat-isn/web/-/commit/${COMMITHASH}`}>
						{ i18n.format('project.branch', {
							version: VERSION,
							branch: BRANCH
						}) }
					</a> } </h4>
				<p>{ i18n.get('app.about.authors') }</p>
				<p>{ i18n.get('app.about.author1') }</p>
				<p>{ i18n.get('app.about.author2') }</p>
				<p>{ i18n.get('app.about.author3') }</p>
			</div>
		</div>
	}
}
