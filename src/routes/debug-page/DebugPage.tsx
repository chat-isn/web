import * as crypto from 'crypto'
import React from 'react'

/**
 * Ceci est une page de debug faite pour que la personne
 * de la partie serveur fasse son travail
 */
export default class DebugPage extends React.Component<{}, {
	dh: crypto.DiffieHellman | null
	computedSecret: string | false
}> {
	constructor(props: {}) {
		super(props)
		this.state = { dh: null, computedSecret: false }
	}

	render() {
		return <div style={ { background: 'white', userSelect: 'text' }}>
			<input type='text' id='lol' placeholder='group' defaultValue='modp14' />
			<button onClick={ () => {
				const d = crypto.getDiffieHellman(
					(document.querySelector('#lol') as HTMLInputElement).value
				)
				d.generateKeys()
				this.setState({ dh: d })
			}}>generate key</button>
			{ this.state.dh ? <>
					<p>public key as hex: { this.state.dh.getPublicKey('base64') }</p>
					<input type='text' id='sk' placeholder='server key as b64' />
					<button onClick={ () => {
						this.setState({
							computedSecret: (this.state.dh as crypto.DiffieHellman).computeSecret(
								Buffer.from( (document.querySelector('#sk') as HTMLInputElement).value, 'base64')
							).toString('base64')
						})
					}}>compute secret</button>
					{ this.state.computedSecret ?
						<p>computed secret as hex: { this.state.computedSecret }</p>
					: null }
				</> : null }
		</div>
	}
}
