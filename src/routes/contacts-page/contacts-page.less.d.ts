/* eslint-disable */
declare namespace ContactsPageLessNamespace {
  export interface IContactsPageLess {
    chats: string;
    container: string;
    error: string;
    loading: string;
    parent: string;
  }
}

declare const ContactsPageLessModule: ContactsPageLessNamespace.IContactsPageLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: ContactsPageLessNamespace.IContactsPageLess;
};

export = ContactsPageLessModule;
