import React, { ReactNode } from 'react'
import {NavLink, RouteComponentProps, withRouter} from 'react-router-dom'
import i18n from '../../i18n/i18n'
import UserInputList from '../../components/user-input-list/UserInputList'
import {IDMChatPreview, IUser} from '../../lib/ChatAPI'
import appInstance from '../../AppInstance'
import Button from '../../components/button/Button'
import makeCancelable, {ICancellablePromise} from '../../util/make-cancelable'
import LoadingSpinner from '../../components/loading-spinner/LoadingSpinner'

import CHAT_ICON from '../../img/icons/chat_bubble_outline-black-36dp.svg'
import * as style from './contacts-page.less'


interface IState {
	loading: boolean
	existingChats: IDMChatPreview[]
	error: string | false
}

export class ContactsPage extends React.Component<RouteComponentProps, IState> {
	private userInputList = React.createRef<UserInputList>()
	private request: ICancellablePromise<IDMChatPreview[]> | null = null
	private selectedUsers: IUser[] = []

	constructor(props: Readonly<RouteComponentProps>) {
		super(props)
		this.state = { loading: false, existingChats: [], error: false }
	}

	render(): ReactNode {
		return <div className={ style.parent }>
			<div className={ style.container }>
				<h2>{ i18n.get('app.chat.create-dm') }</h2>
				<h3>{ i18n.get('app.chat.dm.add-people') }</h3>
				{ this.state.error ? <p className={ style.error }>{ this.state.error }</p> : null }
				<UserInputList ref={this.userInputList} userListUpdate={ u => this.onUsersSelected(u) } />

				<div className={ style.chats }>
					{ this.state.existingChats.length > 0 ? <>
						<h3>{ i18n.get('app.chat.dm.existing-ones') }</h3>
						<p><i>{ i18n.get('app.chat.dm.existing-ones.help') }</i></p>
						{ this.state.existingChats.map((chat, i) =>
							<div key={i}>
								<NavLink to={`/app/chat/${chat.id}`}>
									<div>
										<CHAT_ICON />
									</div>

									<div>
										<p>{ chat.name }</p>
									</div>
								</NavLink>
							</div>
						) }
					</> : null }
				</div>

				<Button onClick={() => this.createChat() }>
					{ i18n.get('app.chat.dm.create') }
				</Button>
			</div>
			{ this.state.loading ? <LoadingSpinner className={ style.loading } /> : null }
		</div>
	}

	private async createChat() {
		if (this.selectedUsers.length === 0)
			return this.setState({ loading: false, error: i18n.get('app.chat.create-dm-alone') })

		this.setState({ loading: true })
		const response = await appInstance.chatAPI.createDM(this.selectedUsers.map(u => u.id), true)

		if (typeof response === 'number')
			this.props.history.push(`/app/chat/${response}`)
		else
			this.setState({ loading: false, error: i18n.get('app.chat.create-dm-failed') })
	}

	private onUsersSelected(users: IUser[]) {
		this.selectedUsers = [...users, appInstance.chatAPI.authedUser]

		if (this.request)
			this.request.cancel()

		if (users.length === 0)
			return this.setState({ existingChats: [] })

		this.request = makeCancelable( appInstance.chatAPI.getDMs() )
		this.request.then(dms => {
			const userIds = this.selectedUsers.map(u => u.id)
			this.setState({ existingChats:
				dms.filter(dm => {
					const userIdsChat = dm.users.map(u => u.id)

					if (userIdsChat.length === this.selectedUsers.length)
						return userIds.filter(id => !userIdsChat.includes(id)).length === 0
					return false
				})
			})
		})
	}
}

export default withRouter(ContactsPage)
