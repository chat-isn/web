const languages: {
	[x: string]: ILanguage
} = {}

interface ILanguage {
	[x: string]: string
}

/**
 * Fonction permettant de charger automatiquement
 * un fichier de langue dans le dossier ./languages/<langue>.json
 * @param lang
 */
const r = (lang: string): ILanguage => languages[lang] = require(`./languages/${lang}.json`) as ILanguage

r('fr')
r('en')

// Export des langues
export default languages
