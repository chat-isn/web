import appInstance from '../AppInstance'
import userPreferences, { IUserPreference } from '../lib/UserPreferences'
import log from '../util/log'
import Languages from './languages'

interface ILanguageNaturalList {
	[languageId: string]: string
}

/**
 * Classe se chargeant de l'internationalization
 * de l'application.
 * Doit être instancié avec #init()
 * @singleton
 */
export class I18n {
	private static DEFAULT_LANGUAGE = Object.keys( Languages )[0]
	private static instance: I18n = new I18n()

	langPref: IUserPreference<string>
	language: string = I18n.DEFAULT_LANGUAGE

	constructor() {
		// On charge la langue choisie par l'utilisateur OU celle du navigateur
		// Evidemment, #setLanguage fallback sur I18n.DEFAULT_LANGUAGE si la
		// langue n'existe pas
		this.langPref = userPreferences.registerPreference({
			id: 'lang',
			default: navigator.language.substring(0, 2),
			localStorage: true,
			friendlyName: 'Select language',
			choices: () => this.getLanguages(),
			onChange: (id: string) => this.setLanguage(id),
			enableSearchFromChoice: true
		})
		this.setLanguage( userPreferences.get(this.langPref) )
	}

	/**
	 * Initialisation de la classe
	 */
	static init(): I18n {
		return I18n.instance
	}

	getLanguages(): ILanguageNaturalList {
		const l: ILanguageNaturalList = {}
		Object.keys(Languages).forEach((langId: string) => {
			l[langId] = Languages[langId]['language.name']
		})
		return l
	}

	/**
	 * Returns whether the language has been loaded
	 * If the return is false, then, the returned value is false.
	 * @param lang
	 */
	setLanguage(lang: string): boolean {
		if (!Object.keys(Languages).includes(lang)) {
			this.setLanguage( I18n.DEFAULT_LANGUAGE )
			return false
		}

		this.language = lang
		document.documentElement.setAttribute('lang', lang)

		appInstance.forceUpdate()

		if (this.langPref.value !== lang)
			this.langPref.value = lang
		return true
	}

	/**
	 * Retourne si la clé est définie dans le fichier de langue actuel
	 * @param {boolean} key
	 */
	includes(key: string): boolean {
		return Object.keys(Languages[this.language]).includes(key)
	}

	/**
	 * Retourne la valeur de la clé dans le fichier actuel si elle existe
	 * Sinon, retourne la clé (à des fins de déboguage.)
	 * @param {boolean} key
	 */
	get(key: string): string {
		if (this.includes(key))
			return Languages[this.language][key]
		log('I18N', `${key} not found for lang ${this.language}`)
		return key
	}

	/**
	 * Remplace les "placeholders" (`{placeholder}`) des fichiers de langue
	 * par une valeur donnée
	 */
	format(i18nKey: string, obj: { [key: string]: string } ): string {
		let text = this.get(i18nKey)

		Object.keys(obj).forEach(key => {
			text = text.replace( new RegExp(`{${key}}`, 'gi'), obj[key] )
		})

		return text
	}
}

export default I18n.init()
