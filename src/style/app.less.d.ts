/* eslint-disable */
declare namespace AppLessNamespace {
  export interface IAppLess {
    gradient: string;
    "loading-spinner": string;
    loadingSpinner: string;
    "ws-loading": string;
    wsLoading: string;
  }
}

declare const AppLessModule: AppLessNamespace.IAppLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: AppLessNamespace.IAppLess;
};

export = AppLessModule;
