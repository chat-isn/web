/* eslint-disable */
declare namespace LyromasPurpleThemeLessNamespace {
  export interface ILyromasPurpleThemeLess {
    file: string;
    mappings: string;
    names: string;
    sources: string;
    sourcesContent: string;
    version: string;
  }
}

declare const LyromasPurpleThemeLessModule: LyromasPurpleThemeLessNamespace.ILyromasPurpleThemeLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: LyromasPurpleThemeLessNamespace.ILyromasPurpleThemeLess;
};

export = LyromasPurpleThemeLessModule;
