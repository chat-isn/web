/* eslint-disable */
declare namespace DeepBlueThemeLessNamespace {
  export interface IDeepBlueThemeLess {
    file: string;
    mappings: string;
    names: string;
    sources: string;
    sourcesContent: string;
    version: string;
  }
}

declare const DeepBlueThemeLessModule: DeepBlueThemeLessNamespace.IDeepBlueThemeLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: DeepBlueThemeLessNamespace.IDeepBlueThemeLess;
};

export = DeepBlueThemeLessModule;
