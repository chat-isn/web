/* eslint-disable */
declare namespace DarkThemeLessNamespace {
  export interface IDarkThemeLess {
    file: string;
    mappings: string;
    names: string;
    sources: string;
    sourcesContent: string;
    version: string;
  }
}

declare const DarkThemeLessModule: DarkThemeLessNamespace.IDarkThemeLess & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: DarkThemeLessNamespace.IDarkThemeLess;
};

export = DarkThemeLessModule;
