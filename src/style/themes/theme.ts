import { BACKGROUNDS } from '../../components/background/RandomBackground'
import i18n from '../../i18n/i18n'
import userPreferences from '../../lib/UserPreferences'
import appInstance from '../../AppInstance'

// To add a theme, create a file, import it under
// and add the "global" class inside `choices` (line 20)

import './dark-theme.less'
import './deep-blue-theme.less'
import './lyromas-purple-theme.less'

// Polyfill because TS :(
const fromEntries = (xs: [string|number|symbol, any][]) =>
  xs.reduce((acc, [key, value]) => ({...acc, [key]: value}), {})

const updateTheme = (id: string) => document.body.className = id

updateTheme( userPreferences.get(userPreferences.registerPreference({
	id: 'theme',
	default: 'normal',
	localStorage: true,
	friendlyName: () => i18n.get('app.theme'),
	choices: () => ({
		'normal': i18n.get('app.theme.normal'),
		'dark-theme': i18n.get('app.theme.dark'),
		'deep-blue-theme': i18n.get('app.theme.deep-blue'),
		'lyromas-purple-theme': i18n.get('app.theme.lyromas-purple')
	}),
	onChange: updateTheme,
	enableSearchFromChoice: true
})) )

userPreferences.get(userPreferences.registerPreference(({
	id: 'background',
	default: 'random',
	localStorage: true,
	friendlyName: () => i18n.get('app.background'),
	choices: () => ({
		...(fromEntries(BACKGROUNDS.map((b, i) => [i.toString(), i.toString()]))),
		random: i18n.get('app.theme.background.random')
	}),
	onChange: () => appInstance.forceUpdate(),
	enableSearchFromChoice: false
})))
