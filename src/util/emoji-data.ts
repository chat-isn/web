// a singleton wrapper !

import { EmojiData, Emoji as EmojiType, EmojiImage, sheetColumns, sheetRows } from 'emoji-data-ts'
import EmojisSorted_ from 'emoji.json/emoji-compact.json'

const instance = new EmojiData()

const EmojisSorted = EmojisSorted_.filter(emoji =>
	!instance.isSkinTone( instance.replaceEmojiToStr(emoji) ) )

const SheetData = {
	sheetSizeX: 100 * sheetColumns,
	sheetSizeY: 100 * sheetRows,
	multiplyX: 100 / (sheetColumns - 1),
	multiplyY: 100 / (sheetRows - 1)
}

export { EmojiType, EmojisSorted, EmojiImage, SheetData}
export default instance
