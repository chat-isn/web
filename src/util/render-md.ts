import Emoji from '../components/emoji-picker/Emoji'
import emojiData from './emoji-data'

const UNICODE_CHARS: {[x: string]: string } = {
	'\\_': '&#95;',
	'\\*': '&#42;',
	'\\`': '&#96;',
	'\\~': '&#126;'
}

const unicode = (str: string) => {
	for (const search in UNICODE_CHARS)
		if ({}.hasOwnProperty.call(UNICODE_CHARS, search))
			str = str.replace(new RegExp(search, 'gi'), UNICODE_CHARS[search])

	return str
}

const buildReplaceStr = (showDelimiter: boolean, delimiter: string, tag: string) => {
	const delimiterFriendly = showDelimiter ? `<span class='delimiter'>${unicode(delimiter)}</span>` : ''

	return (s: string) => `${delimiterFriendly}<${tag}>${ s.substr(delimiter.length,
			s.length - 2 * delimiter.length) }</${tag}>${delimiterFriendly}`
}

export default function renderMarkdown(md: string, showOriginalChars = true) {
	let lines = md.split('\n')

	const _ = buildReplaceStr.bind(null, showOriginalChars)

	lines = lines.map(line => {
		line = line.replace(/&#x2F;/gi, '/') // exempt sanitize for /
		line = line
			.replace(/(((mailto:|(osump|(ht|f)tp(s?)):\/\/)|www)+\S+)/gi, s => {
				let href = s
				if (!href.startsWith('http'))
					href = 'http://' + href
				return `<a href='${href}' target='_blank' rel='noopener noreferer'>${s}</a>`
			})

		// Bold
		line = line.replace(/([*]{2})([\wa-zA-Z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u024F][\d]*[\s]*)+\1/g,
			_('**', 'b'))
		// Underline
		line = line.replace(/([_]{2})([\wa-zA-Z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u024F][\d]*[\s]*)+\1/g,
			_('__', 'u'))
		// Emphasis
		line = line.replace(/([_|*])([\wa-zA-Z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u024F][\d]*[\s]*)+\1/g,
				s => _(s[0], 'i')(s))
		// Strikethrough
		line = line.replace(/([~]{2})([\wa-zA-Z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u024F][\d]*[\s]*)+\1/g,
			_('~~', 'del'))
		// Code
		line = line.replace(/([`])([\wa-zA-Z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u024F][\d]*[\s]*)+\1/g,
			_('`', 'code'))

		// Emoji
		line = line.replace( emojiData.emojiUnicodeRegex, m => {
			const emojiSearch = emojiData.searchEmoji(
				emojiData.replaceEmojiToStr(m).replace(/:/gi, ''),
				1)
			if (emojiSearch.length > 0) {
				const e = emojiSearch[0]
				return Emoji.toHtml(e)
			}

			return m
		})

		return line
	})

	return lines.join('<br>')
}
