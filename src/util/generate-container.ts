/**
 * Génère un conteneur pour React, utilisé dans index.tsx
 */
export default (): HTMLDivElement => {
	if (module.hot) {
		const old = document.querySelector('div#main')

		if (old)
			return old as HTMLDivElement
	}

	const element = document.createElement('div')
	element.id = 'main'

	document.body.appendChild(element)
	return element
}
