import downloadBlob from './download-blob'

const SHOW_LOG = DEV

interface ILogEntry {
	time: number
	group: string
	message: string
}

const LOGS: ILogEntry[] = []
/**
 * Pour log !
 * @param group Groupe du log
 * @param args Texte !
 */
const log = (group: string, ...args: any): void => {
	appendToLog.apply(null, [group, ...args])
	if (SHOW_LOG)
		console.log( `[%c${group}%c] ${args.join(';')}`, 'color: green; font-weight: bold;', '' )
}

const appendToLog = (group: string, ...args: any): void => {
	LOGS.push({
		time: (new Date()).getTime(),
		group,
		message: args.join(';')
	})
}

export function downloadLog() {
	downloadBlob(`log-${ new Date().getTime() }.txt`,
		new Blob([JSON.stringify(LOGS).replace(/},/g, '},\n')], {
		type: 'text/plain;charset=utf-8'
	}))
}

export default log
export { appendToLog }
