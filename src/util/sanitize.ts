type ValuesOf<T extends any[]>= T[number];

const sanitize = (str: string) => str.replace(/[&<>"'/{]/ig, (match: string) =>
	({'&': '&amp;', '{': '&#123;', '<': '&lt;', '>': '&gt;', '"': '&quot;', "'": '&#x27;', '/': '&#x2F;'})
		[match as (ValuesOf<['&', '<', '>', '"', '\'', '/', '{']>)]
)

export default sanitize
