import {IMessage, ISendingMessage} from '../lib/ChatAPI'

export default function applyNewSelfMessage(newMessage: IMessage, arr: (IMessage | ISendingMessage)[]) {
	let found = false
	arr.reverse()
	arr = arr.map(message => {
		if (message.content.toString() === newMessage.content.toString()
				&& message.sender.id === newMessage.sender.id) {
			found = true
			return newMessage
		} else return message
	})

	arr.reverse()

	if (!found)
		arr.unshift(newMessage)
	return arr
}
