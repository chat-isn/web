
export interface ICancellablePromise<T> extends Promise<T> {
  cancel: () => void
}

// Thanks to @istarkov https://github.com/facebook/react/issues/5465#issuecomment-157888325
// for this simple function !
export default function makeCancelable<T>(promise: Promise<T>): ICancellablePromise<T> & Promise<T> {
  let hasCanceled_ = false;

  const wrappedPromise = new Promise((resolve, reject) => {
    promise.then(
      val => hasCanceled_ ? /* reject({isCanceled: true}) */ null : resolve(val),
      error => hasCanceled_ ? /* reject({isCanceled: true}) */ null : reject(error)
    );
  });

  (wrappedPromise as ICancellablePromise<T>).cancel = () => hasCanceled_ = true

  return wrappedPromise as ICancellablePromise<T>

}
