export default function insertAtCaretPosition(element: HTMLTextAreaElement, text: string) {
	if (!text || text.length === 0)
		return

	if (element.selectionStart || element.selectionStart === 0) {
		const start = element.selectionStart,
			end = element.selectionEnd

		element.value = element.value.substring(0, start) +
			text +
			element.value.substring(end, element.value.length)

		element.selectionStart = start + text.length
		element.selectionEnd = end + text.length
	} else
		element.value += text
}
