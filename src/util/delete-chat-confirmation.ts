import buildButtonsModal from '../components/modal/buttons-modal/ButtonsModal'
import {IDMChatPreview} from '../lib/ChatAPI'
import i18n from '../i18n/i18n'
import appInstance from '../AppInstance'
import getHistory from '../components/util/getHistory'

export default function deleteChatConfirmation(dmchat: IDMChatPreview | null) {
	if (!dmchat)
		return

	appInstance.showModal(buildButtonsModal(
		i18n.format('app.chat.delete.confirmation', { name: dmchat.name }),
		[
			{id: 'yes', text: i18n.get('generic.modal.yes') },
			{id: 'no', text: i18n.get('generic.modal.no')}
		],
		(modal, id) => {
			if (!modal) return

			switch (id) {
				case 'yes':
					const history = getHistory()
					if (history)
						history.replace('/app/chat')
					appInstance.chatAPI.deleteChat(dmchat.id).then()
				case 'no':
				default:
					modal.hideModal()
					break
			}
		}
	))
}
