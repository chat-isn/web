import log, {appendToLog} from './log'

(function() {
	if (DEV)
		return

	/**
	 * Just some awful code to hook console !
	 */

	const oldConsole = window.console

	// lol :
	// noinspection
	/* eslint-disable @typescript-eslint/ban-ts-ignore */
	// @ts-ignore
	const consoleProps = Object.getOwnPropertyNames(oldConsole).filter(i => typeof oldConsole[i] === 'function')

	const newConsole: { [x: string]: (...args: any) => any } = {}
	consoleProps.forEach(prop => newConsole[prop] = (function(...args: any) {
		appendToLog(`console#${prop}-hook`, args.join(';'))
		// @ts-ignore
		return oldConsole[prop].call(oldConsole, ...args)
	}))

	if (newConsole)
		// @ts-ignore
		// noinspection JSConstantReassignment
		window.console = newConsole as unknown as Console
	else log('console-hook', 'Hooking failed.')

})()
