import {ComponentClass, FunctionComponent} from 'react'
import App from './App'
import buildSimpleMessageModal from './components/modal/simple-message-modal/buildSimpleMessageModal'
import Authentication from './lib/Authentication'
import Connection from './lib/Connection'
import log from './util/log'
import ChatAPI from './lib/ChatAPI'
import i18n from './i18n/i18n'
import {IModalProps} from './components/modal/Modal'
import RichMessage, {RichMessageTypes} from './lib/RichMessage'

const SERVER_WS = WEBSOCKET_SERVER

/**
 * Représente l'aspect "code" de l'interface.
 * Cette classe gère tout l'aspect connexion, serveur, ...
 * @singleton
 */
export class AppInstance {
	private static instance: AppInstance = new AppInstance()

	auth: Authentication = new Authentication(this)
	// auth: Authentication = new FakeAuthentication(this)
	connection: Connection = new Connection( SERVER_WS )
	chatAPI: ChatAPI = new ChatAPI( this.connection )
	// chatAPI: ChatAPI = new FakeChatAPI( this.connection )

	private root: App | null = null
	private gameWindows: { [chatId: number]: Window } = {}

	constructor() {
		setInterval( () => this.gamesHeartbeat(), 2000)
		this.auth.on('connected', () => {
			log('AppInstance', 'Logged! Refreshing...')
			this.forceUpdate()
		})
	}

	/**
	 * Initialisation de la classe
	 */
	static init(): AppInstance {
		return AppInstance.instance
	}

	setRoot(root: App): void {
		/* if (DEV && root)
			setTimeout(() => root.setState({ }), 250) */
		this.root = root
	}

	/**
	 * Retourne si l'utilisateur est connecté
	 */
	isAuthed(): boolean {
		return this.auth.isAuthed
	}

	showModal(modal: FunctionComponent<IModalProps> | ComponentClass<IModalProps> | null): void {
		if (this.root)
			if (modal === null || !this.root.state.modal)
				this.root.setState({ modal })
	}

	openGameChat(chatId: number, gameName: string) {
		if (!this.isAuthed())
			return

		if (Object.keys(this.gameWindows).includes(chatId.toString()))
			return this.showModal(
				buildSimpleMessageModal(i18n.get('app.chat.game.already-exists')))

		const w = window.open(`/game-interface/${gameName}/`)

		if (!w)
			return this.showModal(buildSimpleMessageModal(i18n.get('app.chat.game.window-blocked-by-browser')))

		this.gameWindows[chatId] = w

		if (!w.document)
			return this.showModal(buildSimpleMessageModal(i18n.get('app.chat.game.cannot-access-document')));

		(w as any).sendMessage = (message: string) =>
			this.chatAPI.sendMessageToChat(message, chatId, true);

		(w as any).heartbeat = Date.now();

		(w as any).notifyScore = (n: number) =>
			this.chatAPI.sendRichMessageToChat(new RichMessage(null, RichMessageTypes.SCORE_NOTIFY, {
				gameName, chatId, score: n.toString()
			}), chatId);

		(w as any).notifyJoin = () =>
			this.chatAPI.sendRichMessageToChat(new RichMessage(null, RichMessageTypes.GAME_NOTIFY, {
				gameName, chatId
			}), chatId).then()

		this.chatAPI.on(`game-message-${chatId}`, msg => {
			if ((w as any).onMessage)
				(w as any).onMessage(msg)
		})

		w.onbeforeunload = () => {
			this.chatAPI.removeAllListeners(`game-message-${chatId}`)
			delete this.gameWindows[chatId]
		}

		(async () => {
			const dmQuery = (await this.chatAPI.getDMs()).filter(dm => dm.id === chatId)
			if (dmQuery.length === 0)
				return
			w.document.title = i18n.format('app.chat.game-ext.title', {
				chatname: dmQuery[0].name
			})
		})()
	}

	forceUpdate(): void {
		if (this.root)
			this.root.forceUpdate()
	}

	reconnectStateWithComponent(): void {
		if (this.root)
			this.root.setState({ isWSConnected: false, connectionFailed: false })
	}

	private gamesHeartbeat() {
		for (const chatid in this.gameWindows)
			if (this.gameWindows.hasOwnProperty(chatid)) {
				const window_ = this.gameWindows[chatid];

				(window_ as any).heartbeat = Date.now()
			}
	}
}

export default AppInstance.init()
