import React from 'react'
import ReactDOM from 'react-dom'
import generateContainer from './util/generate-container'
import App from './App'
import appInstance from './AppInstance'
import WebSocketMessage from './lib/WebSocketMessage'

// Chargement des feuilles de style basiques
import './style/index.less'

// theme
import './style/themes/theme'

// console-hook
import './util/console-log-hook'

// notification
import './lib/NotificationHandler'

// unread msg counter
import './lib/UnreadCount'

// spotify integration
import spotifyIntegration from './lib/SpotifyIntegration'
spotifyIntegration.findCode()

// Génération d'un conteneur (div) pour React!
const CONTAINER = generateContainer()

const render = (): void => {
	ReactDOM.render(<App ref={(app: App): void => appInstance.setRoot(app)} />, CONTAINER)
}

// Mise en place d'hot-reload.
if (module.hot)
	module.hot.accept(() => {
		ReactDOM.unmountComponentAtNode(CONTAINER)
		WebSocketMessage.ID = 0
		render()
	})

render()
