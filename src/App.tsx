import React, {ComponentClass, FunctionComponent, ReactNode} from 'react'
import {HashRouter, Redirect, Route, Switch} from 'react-router-dom'
import LoginPage from './routes/login-page/LoginPage'
import appInstance from './AppInstance'
import ConnectionLostModal from './components/modal/connection-lost-modal/ConnectionLostModal'
import LoadingSpinner from './components/loading-spinner/LoadingSpinner'
import AppPage from './routes/app-page/AppPage'
import DebugPage from './routes/debug-page/DebugPage'
import RandomBackground from './components/background/RandomBackground'
import {IModalProps} from './components/modal/Modal'
import Button from './components/button/Button'
import {ConnectionState} from './lib/Connection'
import i18n from './i18n/i18n'
import getHistory, {HistoryExport} from './components/util/getHistory'
import * as style from './style/app.less'
import log from './util/log'
import Notifications from './components/notifications/Notifications'
import UnreadCount from './lib/UnreadCount'

interface IAppState {
	modal: FunctionComponent<IModalProps> | ComponentClass<IModalProps> | null
	isWSConnected: boolean
	connectionFailed: false | Error
}

/**
 * Classe principale de l'application
 */
export default class App extends React.Component<{}, IAppState> {
	private isItMounted = false

	constructor(props: {}) {
		super(props)
		this.state = {
			modal: null,
			isWSConnected: appInstance.connection.connectionState === ConnectionState.ESTABLISHED, // || true,
			connectionFailed: false
		}

		if (!this.state.isWSConnected) {
			appInstance.connection.connect()

			appInstance.connection.on('connected', this.connectedEventHandler)

			appInstance.connection.on('retry', this.retryEventHandler)
			appInstance.connection.on('connecting', this.connectingEventHandler)

			appInstance.connection.on('error', this.errorEventHandler)

			appInstance.connection.on('unexpected-close', this.unexpectedClose)
		} else
			this.autoConnect()
	}

	componentDidMount() {
		this.isItMounted = true
	}

	componentWillUnmount() {
		this.isItMounted = false
		appInstance.connection.removeListener('connected', this.connectedEventHandler)
		appInstance.connection.removeListener('retry', this.retryEventHandler)
		appInstance.connection.removeListener('connecting', this.connectingEventHandler)
		appInstance.connection.removeListener('error', this.errorEventHandler)
		appInstance.connection.removeListener('unexpected-close', this.unexpectedClose)
	}

	render(): React.ReactNode {
		return <div>
			{ this.renderModal() }

			<RandomBackground />

			{ this.state.isWSConnected ? this.renderContent() : this.renderLoading() }
		</div>
	}

	// event handlers
	private connectedEventHandler = () => this.autoConnect()
	private retryEventHandler = () => this.forceUpdate()
	private connectingEventHandler = () => this.setState({
			isWSConnected: false, connectionFailed: false
		})
	private errorEventHandler = (e: false | Error) => this.setState({ connectionFailed: e })
	private unexpectedClose = () => {
		console.error('Unexpected close')
		this.setState({modal: ConnectionLostModal})
	}

	private autoConnect() {
		if (appInstance.auth.canAutoConnect()) {
			log('App', 'Able to auto-connect...')
			appInstance.auth.autoConnect().then(isConnected => {
				if (!this.isItMounted)
					return

				if (isConnected)
					log('App', isConnected ? 'Auto-connected!' : 'Auto-connect failed...')
				this.setState({
					isWSConnected: true
				})
			})
		} else
			this.setState({
				isWSConnected: true
			})
	}

	private renderModal(): ReactNode {
		if (this.state.modal) {
			const _Modal = this.state.modal
			return <_Modal onModalClose={() => this.setState({ modal: null })} />
		}
		return null
	}

	private renderLoading(): ReactNode {
		return <div className={ style.wsLoading }>
			<div>
				{ this.state.connectionFailed ? <>
						<p>{ i18n.format('connecting.failed', {
							error: this.state.connectionFailed.name
						}) }</p>
						<Button onClick={ () => {
							appInstance.connection.connectionTries = 1
							appInstance.connection.connect()
							this.forceUpdate()
						}}>{ i18n.get('connecting.retry') }</Button>
					</> : <>
						<LoadingSpinner className={ style.loadingSpinner }/>
						<p>{ i18n.format('connecting', {
							try: appInstance.connection.connectionTries + '',
							max: appInstance.connection.connectionMax + ''
						}) }</p>
					</>
				}
			</div>
		</div>
	}

	private renderContent(): ReactNode {
		return <HashRouter>
			{ /* History dependant lib */ }
			<HistoryExport />
			<Route path='/' render={props => {
				if (props.history)
					UnreadCount.onPageChanged(props.history)
				return null
			}} />


			<Route path='/login' render={props => {
				const redirect = props.location.search.substring('?r='.length)
				// si on est déjà connecté, aller sur login redirigera vers l'app
				if (appInstance.isAuthed())
					return <Redirect to={`/app/${redirect}`} />
			}} />

			<Route path='/app' render={props => {
				let redirect = props.location.pathname.substring('/app/'.length)
				if (redirect.length > 1) redirect = '?r=' + redirect
				// si on est pas connecté, passer par le login
				if (!appInstance.isAuthed())
					return <Redirect to={`/login${redirect}`} />
			}} />

			<Route exact path='/' render={() => {
				const history = getHistory()
				if (history)
					history.replace(appInstance.isAuthed() ? '/app' : '/login')
				return <></>
			}} />

			<Switch>
				{ /* Routes */ }
				<Route path='/app' component={AppPage} />

				<Route path='/login' component={LoginPage} />

				<Route path='/debug' component={DebugPage} />

				<Redirect to='/' />
			</Switch>
			<Notifications />
		</HashRouter>
	}
}
