const path = require('path'),
	  HtmlWebpackPlugin = require('html-webpack-plugin'),
	  ImageminPlugin = require('imagemin-webpack-plugin').default,
	  imageminJpegoptim = require('imagemin-jpegoptim'),
	  TerserPlugin = require('terser-webpack-plugin'),
	  GitRevisionPlugin = require('git-revision-webpack-plugin'),
	  webpack = require('webpack')

const WEBSOCKET_SERVER = 'ws://164.132.6.33:80/',
	SPOTIFY_WEB_SERVICE_SERVER = 'http://164.132.6.33:8081/'

const APP_DIR     = path.resolve(__dirname, 'src'),
	  BUILD_DIR   = path.resolve(__dirname, 'dist'),
	  PORT = 8080,
	  gitRevisionPlugin = new GitRevisionPlugin()

module.exports = (env, argv) => {
	const TARGET = process.env.npm_lifecycle_event,
		  dev = argv.mode === 'development' || TARGET === 'dev' || TARGET === 'dev-ext'

	if (!TARGET && typeof argv.mode === 'undefined')
		console.warn('npm_lifecycyle_event and --mode not found, fallbacking to PROD env.')

	const config = {
		devtool: dev ? 'source-map': false,
		devServer: dev ? {
			hot: true,
			contentBase: BUILD_DIR,
			port: PORT,
			publicPath: '/',
			historyApiFallback: true,
			disableHostCheck: true,
			overlay: true
		} : { },

		mode: dev ? 'development': 'production',

		entry: [
			'@babel/polyfill',
			`${APP_DIR}/index.tsx`
		],
		output: {
			path: BUILD_DIR,
			publicPath: '/',
			filename: '[name].js'
		},

		resolve: {
			extensions: [ '.tsx', '.ts', '.js', '.json' ]
		},

		module: {
			rules: [
				{
					test: /\.tsx?$/,
					use: dev ? [ 'ts-loader?transpileOnly', 'eslint-loader' ] : 'ts-loader?transpileOnly',
					exclude: /node_modules/
				}, {
					test: /\.less$/,
					exclude: /node_modules/,
					use: [
						'style-loader',
						{
							loader: '@teamsupercell/typings-for-css-modules-loader',
							options: {
								banner: '/* eslint-disable */'
							}
						}, {
							loader: 'css-loader',
							options: {
								sourceMap: dev,
								modules: {
									localIdentName: dev ? '[name]__[local]' : '[hash:base64]'
								},
								importLoaders: 2,
								localsConvention: 'camelCase'
							}
						}, {
							loader: 'postcss-loader',
							options: {
								sourceMap: dev,
								plugins: [ require('autoprefixer') ]
							}
						},
						'resolve-url-loader?sourceMap=dev',
						{
							loader: 'less-loader',
							options: {
								sourceMap: dev
							}
						}
					]
				}, {
					test: /\.(jpe?g|png|gif)$/i,
					use: [
						'file-loader?hash=sha512&digest=hex&name=[hash].[ext]'
					]
				}, {
					test: /\.svg$/i,
					oneOf: [{
							resourceQuery: /file/,
							use: 'file-loader?hash=sha512&digest=hex&name=[hash].[ext]'
						}, {
						// resourceQuery: /inline/,
						use: [
							'babel-loader',
							{
								loader: '@svgr/webpack',
								options: {
									title: false
								}
							}
						]
						}
					]
				},
				{
					test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					use: 'url-loader?limit=10000&mimetype=application/font-woff'
				},
				{
					test: /\.(ttf|eot|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					use: 'file-loader'
				}
			]
		},

		plugins: [
			new HtmlWebpackPlugin({ title: 'Chat', meta: {
				viewport: 'width=device-width, initial-scale=1.0'
			}, template: './src/public/index.ejs', inject: false }), // inject handled by index.ejs with const src
			new webpack.DefinePlugin({
				DEV: dev,
				VERSION: JSON.stringify( gitRevisionPlugin.version() ),
				COMMITHASH: JSON.stringify( gitRevisionPlugin.commithash() ),
				BRANCH: JSON.stringify( gitRevisionPlugin.branch() ),
				SPOTIFY_WEB_SERVICE_SERVER: JSON.stringify(SPOTIFY_WEB_SERVICE_SERVER),
				WEBSOCKET_SERVER: JSON.stringify(WEBSOCKET_SERVER)
			})
		],

		optimization: {
			namedModules: true,
			splitChunks: {
				name: 'vendor',
				chunks: 'all'
			}
		}
	}

	if (dev) {
		config.plugins.push( new webpack.NamedModulesPlugin() )
		config.plugins.push( new webpack.WatchIgnorePlugin([ /css\.d\.ts$/ ]) )
		// config.plugins.push( new webpack.HotModuleReplacementPlugin() )

		config.entry.push( 'react-hot-loader/patch', 'webpack/hot/only-dev-server' )
	} else {
		config.plugins.push( new ImageminPlugin({
			optipng: null,
			jpegtran: null,
			gifsicle: null,
			pngquant: {
				speed: 2
			},
			svgo: null,
			plugins: [
				imageminJpegoptim({
					max: 85,
					progressive: true
				})
			]
		}))

		config.optimization.minimize = true
		config.optimization.minimizer = [ new TerserPlugin() ]
	}

	return config
}
