<h1 align="center">
	Projet ISN: Chat — Web
</h1>

<p align="center">
	<img src="https://forthebadge.com/images/badges/compatibility-betamax.svg">
	<img src="https://forthebadge.com/images/badges/contains-technical-debt.svg">
	<img src="https://forthebadge.com/images/badges/makes-people-smile.svg">
</p>

## Outils utilisés 

* [Typescript](http://www.typescriptlang.org/)
  * Pour écrire en JavaScript de manière plus "typée"
* [ReactJS](http://reactjs.org)
  * Simplifiant l'écriture d'une [SPA](https://fr.wikipedia.org/wiki/Application_web_monopage)
  * Pour son système de `states` par composants.
* [Less](http://lesscss.org/)
  * afin d'écrire du CSS, de manière plus organisée et simplifiée (@import, mixins, ...)
* [Autoprefixer](https://github.com/postcss/autoprefixer)
  * Permet d'automatiquement rajouter des préfixes à des règles CSS (`::-moz-placeholder` par exemple)
* [SVGR](https://github.com/gregberge/svgr)
  * Transforme les SVG en composants React, utilisé avec son loader @svgr/webpack
  * accompagné de [react-native-svg](https://github.com/react-native-community/react-native-svg)
* Tous les « loaders » des outils cités auparavants
* [css-loader](https://github.com/webpack-contrib/css-loader)
  * Permet d'ajouter le CSS à Webpack (sous forme de fichiers)
* [style-loader](https://github.com/webpack-contrib/style-loader)
  * Ajoute le CSS dans le code JavaScript directement et le rend utilisable comme module
* [typings-for-css-modules-loader](https://github.com/TeamSupercell/typings-for-css-modules-loader)
  * Permet de générer dynamiquement les définitions de fichiers pour les fichiers CSS
* [React Transition Group](https://github.com/reactjs/react-transition-group/tree/v1-stable)
  * Permet de gérer les animations en React simplement
* [classnames](https://github.com/JedWatson/classnames)
  * Un outil simple afin de génerer les `className`s de manière conditionnelle ou non
* [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin)
  * Plugin permettant de générer un fichier HTML (minimal) pour Webpack
* [git-revision-webpack-plugin](https://github.com/pirelenito/git-revision-webpack-plugin)
  * Permet d'afficher le hash du dernier commit dans le code (utilisé pour du debug)
* [imagemin-webpack-plugin](https://github.com/Klathmon/imagemin-webpack-plugin)
  * Permet d'automatiquement compresser & redimensionner les images de manière intelligente
* [terser-webpack-plugin](https://github.com/webpack-contrib/terser-webpack-plugin)
  * Compresse le code en le “minify”-ant 
* [react-hot-loader](https://github.com/gaearon/react-hot-loader)
  * Permet de mettre à jour les composants React en temps réel (sans rafraîchissement de page) pendant le développement
* [webpack](https://webpack.js.org/) & [webpack-dev-server](https://github.com/webpack/webpack-dev-server)
  * Afin de compiler la multitude de ressources dans un seul bundle de quatre-cinq fichiers
  * Utilisation du `HotModuleReplacementPlugin` permettant de mettre à jour en temps réel (sans rafraîchissement de page) le code!
  * Utilisation du `DefinePlugin` permettant de définir des constantes dans le code lors de la build
* [DefinitelyTyped](https://github.com/DefinitelyTyped/DefinitelyTyped)
  * Tous les fichiers de définitions pour ces bibliothèques lorsqu'elles n'ont pas déjà été faites (`@types/<bibliothèque>`)
* [ESLint](https://eslint.org/) avec [@typescript-eslint](https://github.com/typescript-eslint/typescript-eslint)
  * Se charge du "lint" (c-à-d vérifier que le code est bien écrit respectant un set de règles prédéfinies)
* [Yarn](https://yarnpkg.com/)
  * Un gestionnaire de paquets bien plus intelligent et rapide que [npm](https://www.npmjs.com/)
* [Todo-tree](https://github.com/Gruntfuggly/todo-tree)
  * Extension pour Visual Studio Code montrant les `TODO` et les liste
* Intellij IDEA
* [emoji-data-ts](https://github.com/nulab/emoji-data-ts) et [emoji.json](https://www.npmjs.com/package/emoji.json)
  * Deux bibliothèques qui sont souvent mises à jour

## Développer

* Clonez
```
git clone git@gitlab.com:chat-isn/web.git
cd web
```

* Téléchargez les dépendances
```bash
npm install
```
ou
```
yarn install --production=false
```

* Compilez
```
<package-man> run build
```

* ou hébergez localement un serveur de développement
```
<package-man> run dev
```

## Bugs connus

* En développement, avec le hot-reload, `<LoginCard />` a tendance à subir son `state` de remis à zéro
quand on essaye de changer la langue <1s après un refresh

## Crédits

### Fond d'écrans

* [*an alley like a background of anime*](https://www.flickr.com/photos/83823034@N06/8916614485) par [@eraplantonico](https://www.flickr.com/photos/83823034@N06/)
* Arseniy Chebynkin
* *Bliss*, Charlos O'Rear
* [*Under the strange horizon*](https://www.deviantart.com/joeyjazz/art/Under-the-strange-horizon-787762783) par [JoeyJazz](https://www.deviantart.com/joeyjazz)
* [VideoFromMoscow](https://www.youtube.com/channel/UCLzw70UBcZjaPTj7WdvyqcA)
* [*My heart laid bare*](https://www.deviantart.com/chasingartwork/art/My-heart-laid-bare-554857558) par [ChasingNetwork](https://www.deviantart.com/chasingartwork)
* Bannière de [Lockwould](https://www.youtube.com/channel/UCLhJ-5PJZJtugRtWMQOYbZA)

### Icônes

* [Material Icons](https://material.io/resources/icons) disponibles sous la [licence Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.html)